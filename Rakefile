require 'git'

task default: :specs

def system_or_exit(cmd, stdout = nil)
  puts "Executing #{cmd}"
  cmd += " >#{stdout}" if stdout
  system(cmd) || fail('******** Build failed ********')
end

desc 'Runs Unit Tests'
task :specs do
  system_or_exit("bundle install")
  system_or_exit("bundle exec fastlane specs")
end

desc 'Build App for Dev tests'
task :dev do
  system_or_exit("bundle install")
  system_or_exit("bundle exec fastlane build_dev")
end

desc 'Build App for Appstore release'
task :production do
  system_or_exit("bundle install")
  system_or_exit("bundle exec fastlane build_production")
end

desc 'Clean all local caches and nuke cocoapods'
task :nuke_cocoapods do
  system_or_exit("rm -rf ~/.cocoapods;")
  system_or_exit("rm -rf ~/Library/Caches/CocoaPods")
  system_or_exit("rm -rf Pods")
  system_or_exit("rm -rf ~/Library/Developer/Xcode/DerivedData/*")
  system_or_exit("xcodebuild clean")
  system_or_exit("pod deintegrate")
  system_or_exit("pod setup")
  system_or_exit("pod install")
end

desc 'Runs xcodebuild clean'
task :clean do
  system_or_exit("xcodebuild clean | xcpretty -c")
  system_or_exit("rm -rf build")
end

desc 'Runs swiftlint for files that changed in current HEAD compared to BASE'
task :swiftlint do
  repo = Git.open "."

  diff = repo.diff("develop", "HEAD")

  new_files = diff.select { |diff| diff.type == "new" }
  new_files = new_files.map { |e| e.path  }

  deleted_files = diff.select { |diff| diff.type == "deleted" }
  deleted_files = deleted_files.map { |e| e.path  }

  modified_files = diff.stats[:files].keys

  new_and_changed_files = new_files + modified_files - deleted_files
  swift_files = new_and_changed_files.select { |file| file.end_with?(".swift")}

  swiftlint_command = "swiftlint lint --quiet --reporter json"

  require 'json'
  result_json = swift_files.uniq.collect { |f|
    JSON.parse(`#{swiftlint_command} --path "#{f}"`.strip).flatten
  }.flatten

  # Convert to swiftlint results
  warnings = result_json.select do |results|
    results['severity'] == 'Warning'
  end

  errors = result_json.select do |results|
    results['severity'] == 'Error'
  end

  print_swiftlint_result("Warning", warnings)
  print_swiftlint_result("Error", errors)
end

def print_swiftlint_result (type, result)
  result.each { |r|
    filename = r['file'].split('/').last
    line = r['line']
    reason = r['reason']
    puts "#{type}: #{filename}:#{line}, #{reason}"
  }
end


