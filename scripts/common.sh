#!/usr/bin/env rvm-shell -e

set -x

function build_setup {
    JENKINS_RVM_DIR="$HOME/.rvm"
    RUBY_VERSION="2.3.0"
    RUBY_GEMSET="DS-finland-demo"

    export PATH="${JENKINS_RVM_DIR}/gems/ruby-${RUBY_VERSION}@${RUBY_GEMSET}/bin:${JENKINS_RVM_DIR}/gems/ruby-${RUBY_VERSION}@global/bin:${JENKINS_RVM_DIR}/rubies/ruby-${RUBY_VERSION}/bin:${JENKINS_RVM_DIR}/bin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin"

    export GEM_PATH="${JENKINS_RVM_DIR}/gems/ruby-${RUBY_VERSION}@${RUBY_GEMSET}:${JENKINS_RVM_DIR}/gems/${RUBY_VERSION}@global"
    export GEM_HOME="${JENKINS_RVM_DIR}/gems/ruby-${RUBY_VERSION}@${RUBY_GEMSET}"

    export LANG="en_US.UTF-8"
    export LANGUAGE="en_US.UTF-8"
    export LC_ALL="en_US.UTF-8"

    which bundle || gem install bundler
    bundle install
}
