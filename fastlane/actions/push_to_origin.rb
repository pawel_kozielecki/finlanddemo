require 'Git'

module Fastlane
  module Actions
    class PushToOriginAction < Action
      def self.run_one_command(cmd, log)
        Fastlane::Helper.log.info log
        system cmd
        if $? != 0
          raise "Failed to run '#{cmd}'"
        end
      end

      def self.run(_params)
      	safe_version_number = _params[:full_version_number].gsub!(':','_')
        branch_name = "version_bump/#{_params[:variant_name]}_#{safe_version_number}".downcase
        commit_msg = "Version bump after production build #{_params[:full_version_number]}"
        self.run_one_command("git fetch", "Fetch repository - you are on headless if running from TC")
        self.run_one_command("git checkout -b #{branch_name}", "Create branch")
        self.run_one_command("git commit -a -m \"#{commit_msg}\"", "Commit all changes to branch")
        self.run_one_command("git push --set-upstream origin #{branch_name}", "Push changes to origin")
        
        # remote_name = "https://bitbucket.org/lsydrones/luca-ios-client"
        # todo: change master to develop when gitflow is established
        # self.run_one_command("git request-pull -m \"#{commit_msg}\" -b master", "Create PR")
        # self.run_one_command("git request-pull #{branch_name} #{remote_name} master")
      end

      def self.description
        'CI-friendly push to origin action'
      end

      def self.available_options
        [
          ['variant_name', 'Variant number'],
          ['full_version_number', 'Full version number']
        ]
      end
      
      def self.is_supported?(platform)
        true
      end
    end
  end
end
