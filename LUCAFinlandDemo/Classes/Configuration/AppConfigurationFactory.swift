import Foundation

func currentConfiguration() -> AppConfiguration {
    return AppConfigurationFactory.sharedInstance.currentConfiguration
}

enum BundleName: String {
    case debugApp = "pl.lhsystems.ios.lucaFinlandDemo.LUCAFinlandDemo-Debug"
    case prodApp = "pl.lhsystems.ios.lucaFinlandDemo.LUCAFinlandDemo"
}

class AppConfigurationFactory {

    static let sharedInstance = AppConfigurationFactory()

    lazy fileprivate (set) var currentConfiguration: AppConfiguration = self.makeConfiguration()

    func makeConfiguration() -> AppConfiguration {
        var configuration: AppConfiguration!

        if let bundleName = BundleName(rawValue: self.configurationKey) {
            switch bundleName {
            case .debugApp:
                configuration = self.debugAppConfiguration()
            case .prodApp:
                configuration = self.releaseAppConfiguration()
            }
        } else {
            fatalError("Could not find app configuration for key \(self.configurationKey)")
        }

        return configuration
    }

    func debugAppConfiguration() -> AppConfiguration {
        var appConfiguration = AppConfiguration()

        appConfiguration.appSuffix = "Debug"
        appConfiguration.appSpectorKey = "ios_YTZiYTNkM2YtZjUyMy00N2I0LWJkN2QtMzg2MjY3ZGY5Mjg0"
        appConfiguration.baseURL = URL(string: "https://ans-finaland-demo-dev.drone-solutions.io/api")!

        return appConfiguration
    }

    func releaseAppConfiguration() -> AppConfiguration {
        var appConfiguration = AppConfiguration()

        appConfiguration.appSpectorKey = "ios_ZjM1ZThhY2ItMmFjZC00NmZlLWJjNzItYjYxMjE1MjAyMWI5"
        appConfiguration.baseURL = URL(string: "https://ans-finland-demo.drone-solutions.io/api")!

        return appConfiguration
    }

    var configurationKey: String = Bundle.main.bundleIdentifier! // swiftlint:disable:this force_cast
}

