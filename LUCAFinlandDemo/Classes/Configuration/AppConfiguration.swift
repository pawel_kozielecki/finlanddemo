import Foundation

struct AppConfiguration {

    var googleMapsApiKey: String = "AIzaSyCtoQ_wh5f7tlT-QPpXVJf_fooKhyJmk_8"
    var requestTimeout: Int = 30
    var appSuffix: String = ""
    var appSpectorKey: String = ""

    var baseURL: URL = URL(string: "https://ans-finland-demo.drone-solutions.io/api")!
}
