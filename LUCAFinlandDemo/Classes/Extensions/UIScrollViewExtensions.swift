import Foundation
import UIKit

extension UIScrollView {
    func scrollToTop(animated: Bool = true) {
        setContentOffset(CGPoint.zero, animated: animated)
    }

    func scrollBy(y: CGFloat) {
        let offset = CGPoint(x: contentOffset.x, y: contentOffset.y + y)
        setContentOffset(offset, animated: true)
    }
}
