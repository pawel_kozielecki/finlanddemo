import Foundation

extension String {

    // Convenience for getting localized strings
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }

    // Convenience for checking if localized strings exists
    var isLocalized: Bool {
        return NSLocalizedString(self, comment: "") != self
    }

    // Convenience for detecting empty strings
    var nullIfEmpty: String? {
        if !self.isEmpty {
            return self
        } else {
            return nil
        }
    }

    // Convenience for parsing
    func convertJsonToDictionary() -> [String: Any]? {
        var json: [String: Any]?
        if let data = self.data(using: String.Encoding.utf8) {
            do {
                json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any]
            } catch {
                logError("DefaultRequestBuilder:build:\(error.localizedDescription)".parseErrorLog)
            }
        }
        return json
    }

    static func randomString(length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0..<length).map{ _ in letters.randomElement()! })
    }
}
