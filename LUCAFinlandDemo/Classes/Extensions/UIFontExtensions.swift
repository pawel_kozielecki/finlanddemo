import Foundation
import UIKit

extension UIFont {

    // GENERAL:

    class var lucaHeader: UIFont {
        return UIFont.systemFont(ofSize: 34.0, weight: .medium)
    }

    // TEXTFIELDS:

    class var dsTextfieldPromptSmall: UIFont {
        return UIFont.systemFont(ofSize: 12.0, weight: .regular)
    }

    class var dsTextfieldText: UIFont {
        return UIFont.systemFont(ofSize: 16.0, weight: .regular)
    }

    class var lucaTextfieldPrompt: UIFont {
        return UIFont.systemFont(ofSize: 16.0, weight: .regular)
    }

    class var lucaTextfieldError: UIFont {
        return UIFont.systemFont(ofSize: 12.0, weight: .regular)
    }
}
