import Foundation
import UIKit
import GoogleMaps

extension GMSCircle {

    enum GMSCircleBoundsEdge: Double {
        case topLeft = -1
        case bottomRight = 1
    }

    func bounds() -> GMSCoordinateBounds {
        return GMSCoordinateBounds(
                coordinate: coordinates(ofEdge: .topLeft),
                coordinate: coordinates(ofEdge: .bottomRight))
    }

    func coordinates(ofEdge edge: GMSCircleBoundsEdge) -> CLLocationCoordinate2D {
        let radian = 180 / Double.pi
        let dx = edge.rawValue * self.radius / 6378000 * radian
        let lat = position.latitude + dx
        let lon = position.longitude + dx / cos(position.latitude * 1 / radian)
        return CLLocationCoordinate2D(latitude: lat, longitude: lon)
    }

    func update(atLocation coordinates: Coordinates, color: UIColor, radius: Double) {
        self.radius = radius
        self.position = coordinates.toCLCoordinates()
        self.fillColor = color.withAlphaComponent(0.2)
        self.strokeColor = color
    }

    static func makeSelectedAirspaceRegion(atLocation coordinates: Coordinates, color: UIColor, radius: Double) -> GMSCircle {
        let area = GMSCircle()
        area.strokeWidth = 1
        area.update(atLocation: coordinates, color: color, radius: radius)
        return area
    }
}
