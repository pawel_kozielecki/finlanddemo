import Foundation

extension DateComponentsFormatter {

    static func makeSimpleTimeFormatter() -> DateComponentsFormatter {
        let formatter = DateComponentsFormatter()
        formatter.unitsStyle = .positional
        formatter.allowedUnits = [.hour, .minute, .second]
        formatter.zeroFormattingBehavior = .pad

        return formatter
    }

    func string(fromSeconds seconds: Int) -> String {
        return "0\(self.string(from: TimeInterval(seconds))!)"
    }
}
