import Foundation

extension DateFormatter {

    static func makeSimpleDateFormatter() -> DateFormatter {
        let dateFormatter = makeGMTDateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd"
        return dateFormatter
    }

    static func makeDatePickerFormatter() -> DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd"
        return dateFormatter
    }

    static func makeDottedDateFormatter() -> DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        return dateFormatter
    }

    static func makeHourMinuteOnlyDateFormatter() -> DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        return dateFormatter
    }

    static func makeDayOfWeekOnlyDateFormatter() -> DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: Bundle.main.preferredLocalizations.first!)
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter
    }

    static func makeDateAndTimeFormatter() -> DateFormatter {
        let dateFormatter = makeGMTDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        return dateFormatter
    }

    static func makeDateAndTimeWithoutMillisecondsFormatter() -> DateFormatter {
        let dateFormatter = makeGMTDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        return dateFormatter
    }

    static func makeSimpleDateAndTimeFormatter() -> DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd HH:mm"
        dateFormatter.timeZone = TimeZone.current
        return dateFormatter
    }

    static func makeGMTDateAndTimeFormatter() -> DateFormatter {
        let dateFormatter = makeGMTDateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd HH:mm"
        return dateFormatter
    }

    static func makeDottedDateAndTimeFormatter() -> DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy HH:mm"
        return dateFormatter
    }

    static func convertTimestampToLocalSimpleDateAndTime(timestamp: String) -> String {
        let fromDateFormatter = DateFormatter.makeDateAndTimeFormatter()
        let fromDateFormatterAlternative = DateFormatter.makeDateAndTimeWithoutMillisecondsFormatter()
        let toDateFormatter = DateFormatter.makeSimpleDateAndTimeFormatter()
        toDateFormatter.timeZone = TimeZone.current

        if let date = fromDateFormatter.date(from: timestamp) {
            return toDateFormatter.string(from: date)
        } else if let date = fromDateFormatterAlternative.date(from: timestamp) {
            return toDateFormatter.string(from: date)
        } else {
            return timestamp
        }
    }

    static func convertTimestampToLocalSimpleDate(timestamp: String) -> String {
        let fromDateFormatter = DateFormatter.makeDateAndTimeFormatter()
        let fromDateFormatterAlternative = DateFormatter.makeDateAndTimeWithoutMillisecondsFormatter()
        let toDateFormatter = DateFormatter.makeSimpleDateFormatter()
        toDateFormatter.timeZone = TimeZone.current

        if let date = fromDateFormatter.date(from: timestamp) {
            return toDateFormatter.string(from: date)
        } else if let date = fromDateFormatterAlternative.date(from: timestamp) {
            return toDateFormatter.string(from: date)
        } else {
            return timestamp
        }
    }

    static func convertTimestampToLocalSimpleTime(timestamp: String) -> String {
        let fromDateFormatter = DateFormatter.makeDateAndTimeFormatter()
        let fromDateFormatterAlternative = DateFormatter.makeDateAndTimeWithoutMillisecondsFormatter()
        let toDateFormatter = DateFormatter.makeHourMinuteOnlyDateFormatter()
        toDateFormatter.timeZone = TimeZone.current

        if let date = fromDateFormatter.date(from: timestamp) {
            return toDateFormatter.string(from: date)
        } else if let date = fromDateFormatterAlternative.date(from: timestamp) {
            return toDateFormatter.string(from: date)
        } else {
            return timestamp
        }
    }

    static func calculateDuration(startTimestamp: String, endTimestamp: String) -> Int {
        let dateFormatter = DateFormatter.makeDateAndTimeFormatter()
        let alternativeDateFormatter = DateFormatter.makeDateAndTimeWithoutMillisecondsFormatter()

        let startDate = dateFormatter.date(from: startTimestamp) ?? alternativeDateFormatter.date(from: startTimestamp)
        let endDate = dateFormatter.date(from: endTimestamp) ?? alternativeDateFormatter.date(from: endTimestamp)
        if let startDate = startDate, let endDate = endDate {
            let differenceDateComponents = Calendar.current.dateComponents([.minute, .second], from: startDate, to: endDate)
            return differenceDateComponents.second! != 0 ? (differenceDateComponents.minute! + 1) : differenceDateComponents.minute!
        } else {
            return 0
        }
    }

    private static func makeGMTDateFormatter() -> DateFormatter {
        //  Because timestamps on backed are stored stripped of timezone, we have to send and parse received dates in GMT
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        return dateFormatter
    }

}
