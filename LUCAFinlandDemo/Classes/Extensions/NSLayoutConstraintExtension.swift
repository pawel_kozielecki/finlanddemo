import Foundation
import UIKit

enum ConstraintAnimationDuration: Double {
    case medium = 0.3
    case delayed = 0.5
}

extension NSLayoutConstraint {


    func animate(toConstant value: CGFloat, viewToUpdate view: UIView,
                 duration: ConstraintAnimationDuration = .medium, completion: (() -> Void)? = nil) {
        UIView.animate(withDuration: duration.rawValue) {
            self.constant = value
            view.layoutIfNeeded()
            completion?()
        }
    }
}
