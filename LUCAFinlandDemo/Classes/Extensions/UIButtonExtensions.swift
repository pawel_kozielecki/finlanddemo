import Foundation
import UIKit

extension UIButton {
    
    @IBInspectable
    var localizationKey: String? {
        get {
            return nil
        }
        set(key) {
            setTitle(key?.localized, for: .normal)
        }
    }

    static let loadingIndicatorTag: Int = 999999

    func replaceAttributedTitleText(_ text: String, for state: UIControl.State, color: UIColor? = nil) {
        if let currentTitle = attributedTitle(for: state) {
            let newTitle = NSMutableAttributedString(attributedString: currentTitle)
            let range = NSRange(location: 0, length: currentTitle.string.count)
            newTitle.replaceCharacters(in: range, with: text)
            if let color = color {
                let newTitleRange = NSRange(location: 0, length: newTitle.string.count)
                newTitle.addAttribute(.foregroundColor, value: color, range: newTitleRange)
            }
            setAttributedTitle(newTitle, for: state)
        }
    }
}
