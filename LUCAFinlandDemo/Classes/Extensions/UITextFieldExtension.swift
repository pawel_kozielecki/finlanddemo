import Foundation
import UIKit

extension UITextField {
    
    @IBInspectable
    var borderColor: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                self.layer.borderColor = color.cgColor
                self.layer.borderWidth = 1.0
            } else {
                self.layer.borderColor = nil
                self.layer.borderWidth = 1.0
            }
        }
    }
}

