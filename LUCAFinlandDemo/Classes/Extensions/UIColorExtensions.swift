import Foundation
import UIKit

extension UIColor {

    class var dsWarmGrey: UIColor {
        return UIColor(hex: "#6F7179")
    }

    class var dsDarkGrey: UIColor {
        return UIColor(hex: "#101520")
    }

    class var dsViolet: UIColor {
        //return UIColor(hex: "#e80071")
        return .dsLightBlue
    }
    
    class var dsLightViolet: UIColor {
        return UIColor(hex: "#e93372")
    }

    class var dsBlue: UIColor {
        return UIColor(hex: "#172d36")
    }

    class var dsLightBlue: UIColor {
        return UIColor(hex: "#345c6d")
    }

    class var dsEnabled: UIColor {
        return UIColor(hex: "#345c6d")
    }

    class var dsDisabled: UIColor {
        return UIColor(hex: "#797979")
    }

    convenience init(hex: String, alpha: CGFloat = 1.0) {
        let hex = hex.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }

    func lighter(by value: CGFloat = 0.05) -> UIColor {
        return adjust(by: abs(value))
    }

    func darker(by value: CGFloat = 0.05) -> UIColor {
        return adjust(by: -1 * abs(value))
    }
}

private extension UIColor {

    func adjust(by value: CGFloat = 0.05) -> UIColor {
        var r: CGFloat = 0, g: CGFloat = 0, b: CGFloat = 0, a: CGFloat = 0;
        if (self.getRed(&r, green: &g, blue: &b, alpha: &a)) {
            return UIColor(
                    red: min(r + value, 1.0),
                    green: min(g + value, 1.0),
                    blue: min(b + value, 1.0),
                    alpha: a)
        } else {
            return self
        }
    }
}
