import Foundation

extension Double {
    func toRoundedString(precision: Int = 1) -> String {
        if precision <= 0 {
            return String(Int(self))
        } else {
            let format = "%.\(precision)f"
            return String(format: format, self)
        }
    }

    static func fromString(stringValue: String) -> Double? {
        return (stringValue as NSString).doubleValue
    }
}

extension Float {
    static func fromString(stringValue: String) -> Float? {
        return (stringValue as NSString).floatValue
    }
}
