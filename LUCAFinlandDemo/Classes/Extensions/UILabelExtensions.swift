import Foundation
import UIKit

extension UILabel {

    func replaceAttributedText(_ replacementText: String) {
        if let currentText = self.attributedText {
            let newText = NSMutableAttributedString(attributedString: currentText)
            let range = NSRange(location: 0, length: currentText.string.count)
            newText.replaceCharacters(in: range, with: replacementText)
            self.attributedText = newText
        }
    }
    
    @IBInspectable
    var localizationKey: String? {
        get {
            return nil
        }
        set(key) {
            text = key?.localized
        }
    }
}
