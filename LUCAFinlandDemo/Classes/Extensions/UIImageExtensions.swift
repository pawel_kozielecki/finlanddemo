import Foundation
import UIKit

extension UIImage {

    class func image(withColor color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) -> UIImage? {
        UIGraphicsBeginImageContext(size)

        let context = UIGraphicsGetCurrentContext()
        context?.setFillColor(color.cgColor)
        context?.fill(CGRect(origin: CGPoint.zero, size: size))
        let image = UIGraphicsGetImageFromCurrentImageContext()

        UIGraphicsEndImageContext()
        return image
    }

    class func circularImage(withColor color: UIColor, diameter: CGFloat) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(CGSize(width: diameter, height: diameter), false, 0)

        let rect = CGRect(x: 0, y: 0, width: diameter, height: diameter)
        let context = UIGraphicsGetCurrentContext()
        context?.saveGState()
        context?.setFillColor(color.cgColor)
        context?.fillEllipse(in: rect)
        context?.restoreGState()
        let image = UIGraphicsGetImageFromCurrentImageContext()

        UIGraphicsEndImageContext()
        return image
    }

    func tintedImage(color: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, UIScreen.main.scale)
        guard let context = UIGraphicsGetCurrentContext() else {
            return self
        }

        context.scaleBy(x: 1.0, y: -1.0)    // flipping the image
        context.translateBy(x: 0.0, y: -self.size.height)
        context.setBlendMode(.multiply)

        let rect = CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height)
        context.clip(to: rect, mask: self.cgImage!)
        color.setFill()
        context.fill(rect)

        guard let newImage = UIGraphicsGetImageFromCurrentImageContext() else {
            return self
        }
        UIGraphicsEndImageContext()

        return newImage
    }

    func resizedImage(newSize: CGSize) -> UIImage {
        guard self.size != newSize else {
            return self
        }

        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0);
        draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))

        guard let newImage = UIGraphicsGetImageFromCurrentImageContext() else {
            return self
        }
        UIGraphicsEndImageContext()

        return newImage
    }

    func resizedImage(shorterEdgeWidth: CGFloat) -> UIImage {
        let widthFactor = size.width / shorterEdgeWidth
        let heightFactor = size.height / shorterEdgeWidth

        var resizeFactor = widthFactor
        if size.height < size.width {
            resizeFactor = heightFactor
        }

        let newSize = CGSize(width: size.width / resizeFactor, height: size.height / resizeFactor)
        return resizedImage(newSize: newSize)
    }
}
