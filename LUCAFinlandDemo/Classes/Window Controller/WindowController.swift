import UIKit

protocol VisibleViewControllerProvider: class {
    func visibleViewController() -> UIViewController
    func visibleView() -> UIView
}

class WindowController {

    let window: UIWindow
    let rootViewController: RootViewController
    unowned let dependencyProvider: DependencyProvider

    init(dependencyProvider: DependencyProvider) {
        self.dependencyProvider = dependencyProvider
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.rootViewController = RootViewController(dependencyProvider: dependencyProvider)
    }

    func makeAndPresentInitialViewController() {
        window.rootViewController = rootViewController
        window.makeKeyAndVisible()
    }
}

extension WindowController: VisibleViewControllerProvider {

    func visibleViewController() -> UIViewController {
        if let topViewController = topViewController(controller: rootViewController) {
            return topViewController
        }

        return self.rootViewController
    }

    func visibleView() -> UIView {
        return visibleViewController().view
    }
}

private extension WindowController {

    func topViewController(controller: UIViewController?) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        if let childViewController = controller?.children.first {
            return topViewController(controller: childViewController)
        }
        return controller
    }
}

extension UIViewController: VisibleViewControllerProvider {

    func visibleViewController() -> UIViewController {
        return self
    }

    func visibleView() -> UIView {
        return view
    }
}
