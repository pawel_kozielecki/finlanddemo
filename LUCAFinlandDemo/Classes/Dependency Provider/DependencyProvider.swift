import Foundation
import UserNotifications
import UIKit
import GoogleMaps
import FirebaseCore
import IQKeyboardManagerSwift
//import AppSpectorSDK

protocol DependencyProvider: class {

    var visibleViewControllerProvider: VisibleViewControllerProvider { get }
    var appConfiguration: AppConfiguration { get }
    var loginManager: LoginManager { get }
    var locationManager: LocationManager { get }
    var remoteNotificationsManager: RemoteNotificationsManager { get }
    var userDataManager: UserDataManager { get }
    var airspaceReservationStorage: AirspaceReservationStorage { get }
    var airspaceReservationManager: AirspaceReservationManager { get }


    func setup(windowController: WindowController)
    func makeDefaultNetworkModule() -> NetworkModule
}

class DefaultDependencyProvider {

    let appConfiguration: AppConfiguration
    let accessTokenManager: AccessTokenManager
    let loginManager: LoginManager
    let locationManager: LocationManager
    let remoteNotificationsManager: RemoteNotificationsManager
    let userDataManager: UserDataManager
    let airspaceReservationManager: AirspaceReservationManager
    var airspaceReservationStorage: AirspaceReservationStorage

    var windowController: WindowController!

    init(appConfiguration: AppConfiguration) {
        GMSServices.provideAPIKey(appConfiguration.googleMapsApiKey)

        self.appConfiguration = appConfiguration
        self.accessTokenManager = DefaultAccessTokenManager()
        let networkModule = DefaultNetworkModule.makeGenericNetworkModule(
                configuration: appConfiguration,
                accessTokenProvider: accessTokenManager)

        self.userDataManager = DefaultUserDataManager()
        self.airspaceReservationStorage = DefaultAirspaceReservationStorage()

        let registrationManager = DefaultRemoteNotificationsRegistrationManager()
        let notificationsCenterWrapper = DefaultNotificationsCenterWrapper(notificationCenter: UNUserNotificationCenter.current())
        self.remoteNotificationsManager = DefaultRemoteNotificationsManager(
                registrationManager: registrationManager,
                notificationCenterWrapper: notificationsCenterWrapper)


        self.loginManager = DefaultLoginManager(
                accessTokenManager: accessTokenManager,
                userDataManager: userDataManager,
                remoteNotificationsManager: remoteNotificationsManager,
                networkController: LoginNetworkController(networkModule: networkModule))
        self.locationManager = DefaultLocationManager()
        let networkController = AirspaceReservationNetworkController(networkModule: networkModule)
        self.airspaceReservationManager = DefaultAirspaceReservationManager(
                networkController: networkController,
                storage: airspaceReservationStorage)

    }

    func setup(windowController: WindowController) {
        self.windowController = windowController


//        setupAppSpectorIfNeeded()
        setupFirebase()
        setupKeyboardManager()
    }
}

extension DefaultDependencyProvider: DependencyProvider {

    var visibleViewControllerProvider: VisibleViewControllerProvider {
        return windowController
    }

    var locationPermissionsHandler: LocationPermissionsHandler {
        return locationManager
    }

    func makeDefaultNetworkModule() -> NetworkModule {
        return DefaultNetworkModule.makeGenericNetworkModule(
                configuration: appConfiguration,
                accessTokenProvider: accessTokenManager)
    }
}

private extension DefaultDependencyProvider {

    func setupFirebase() {
        let bundleSuffix = appConfiguration.appSuffix
        let filePath = Bundle.main.path(forResource: "GoogleService-Info\(bundleSuffix)", ofType: "plist")
        if let options = FirebaseOptions(contentsOfFile: filePath!) {
            FirebaseApp.configure(options: options)
        }
    }

    func setupKeyboardManager() {
        let manager = IQKeyboardManager.shared
        manager.enable = true
        manager.toolbarDoneBarButtonItemText = "DONE".localized.capitalized
        manager.toolbarBarTintColor = .dsLightBlue
        manager.toolbarTintColor = .white
        manager.placeholderColor = .white
        manager.shouldResignOnTouchOutside = true
    }

//    func setupAppSpectorIfNeeded() {
//        let config = AppSpectorConfig(
//                apiKey: currentConfiguration().appSpectorKey,
//                monitorIDs: [.http, .environment, .userdefaults])
//        AppSpector.run(with: config)
//    }
}
