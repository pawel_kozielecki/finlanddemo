import Foundation
import UIKit

protocol RootFlowCoordinatorProvider {
    var currentRootFlowCoordinator: RootFlowCoordinator? { get }
}

protocol RootFlowCoordinatorStarter: RootFlowCoordinatorProvider {
    func start(rootFlowCoordinator coordinator: RootFlowCoordinator, animationType: ViewControllerTransitionAnimation)
}

class RootViewController: UIViewController {

    unowned let dependencyProvider: DependencyProvider

    var rootViewControllerFactory: RootFlowCoordinatorFactory
    var currentRootFlowCoordinator: RootFlowCoordinator?
    var manualTransitionCoordinator: ViewControllersTransitionsCoordinator?

    init(dependencyProvider: DependencyProvider) {
        self.dependencyProvider = dependencyProvider
        self.rootViewControllerFactory = DefaultRootFlowCoordinatorFactory(dependencyProvider: dependencyProvider)

        super.init(nibName: nil, bundle: nil)

        modalPresentationCapturesStatusBarAppearance = true
        self.manualTransitionCoordinator = ViewControllersTransitionsCoordinator(containerViewController: self)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("RootViewController: init(coder) not implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        reloadCurrentRootFlow(animated: false)
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

extension RootViewController: RootFlowCoordinatorDelegate {

    func rootFlowCoordinatorDidFinish(_ flowCoordinator: RootFlowCoordinator) {
        reloadCurrentRootFlow()
    }
}

extension RootViewController: RootFlowCoordinatorStarter {

    func start(rootFlowCoordinator coordinator: RootFlowCoordinator, animationType: ViewControllerTransitionAnimation) {
        self.currentRootFlowCoordinator = coordinator
        coordinator.start()
        coordinator.rootFlowCoordinatorDelegate = self
        manualTransitionCoordinator?.transition(toViewController: coordinator.rootViewController, animationType: animationType)
    }
}

private extension RootViewController {

    func reloadCurrentRootFlow(animated: Bool = true) {
        let currentRootFlowCoordinator = rootViewControllerFactory.makeNextRootFlowCoordinator()
        let animation: ViewControllerTransitionAnimation = animated ? .forward : .none
        start(rootFlowCoordinator: currentRootFlowCoordinator, animationType: animation)
    }
}
