import Foundation
import UIKit

enum RootFlowCoordinatorType {
    case authentication
    case loggedIn
}

protocol RootFlowCoordinatorDelegate: class {

    func rootFlowCoordinatorDidFinish(_ flowCoordinator: RootFlowCoordinator)
}

protocol RootFlowCoordinator: class {

    var rootFlowCoordinatorDelegate: RootFlowCoordinatorDelegate? { get set }
    var rootViewController: UIViewController { get }
    var type: RootFlowCoordinatorType { get }

    func start()
}
