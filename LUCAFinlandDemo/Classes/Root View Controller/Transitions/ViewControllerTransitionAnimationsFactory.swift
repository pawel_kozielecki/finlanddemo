import Foundation
import UIKit

enum ViewControllerTransitionAnimation {
    case fadeIn
    case forward
    case backward
    case none
}

protocol ViewControllerTransitionAnimationsFactory {

    func makePreTransitionAnimationBlock(animationType: ViewControllerTransitionAnimation, fromView: UIView?, toView: UIView?) -> (() -> Void)?
    func makePostTransitionAnimationBlock(animationType: ViewControllerTransitionAnimation, fromView: UIView?, toView: UIView?) -> (() -> Void)?
}

class DefaultViewControllerTransitionAnimationsFactory: ViewControllerTransitionAnimationsFactory {

    func makePreTransitionAnimationBlock(animationType: ViewControllerTransitionAnimation, fromView: UIView?, toView: UIView?) -> (() -> Void)? {
        switch animationType {
        case .fadeIn:
            return makeFadeInAnimationPreBlock(fromView: fromView, toView: toView)
        case .forward:
            return makeSlideAnimationPreBlock(fromView: fromView, toView: toView, isForward: true)
        case .backward:
            return makeSlideAnimationPreBlock(fromView: fromView, toView: toView, isForward: false)
        case .none:
            return nil
        }
    }

    func makePostTransitionAnimationBlock(animationType: ViewControllerTransitionAnimation, fromView: UIView?, toView: UIView?) -> (() -> Void)? {
        switch animationType {
        case .fadeIn:
            return makeFadeInAnimationPostBlock(fromView: fromView, toView: toView)
        case .forward:
            return makeSlideAnimationPostBlock(fromView: fromView, toView: toView, isForward: true)
        case .backward:
            return makeSlideAnimationPostBlock(fromView: fromView, toView: toView, isForward: false)
        case .none:
            return nil
        }
    }
}

private extension DefaultViewControllerTransitionAnimationsFactory {

    func makeFadeInAnimationPreBlock(fromView: UIView?, toView: UIView?) -> (() -> Void) {
        return {
            toView?.alpha = 0
        }
    }

    func makeFadeInAnimationPostBlock(fromView: UIView?, toView: UIView?) -> (() -> Void) {
        return {
            fromView?.alpha = 0
            toView?.alpha = 1
        }
    }

    func makeSlideAnimationPreBlock(fromView: UIView?, toView: UIView?, isForward: Bool) -> (() -> Void) {
        let direction: CGFloat = isForward ? 1 : -1
        return {
            if let toView = toView {
                let width: CGFloat = fromView?.frame.size.width ?? toView.frame.size.width
                let origin = CGPoint(x: direction * width, y: 0)
                let size = toView.frame.size
                toView.frame = CGRect(origin: origin, size: size)
            }
        }
    }

    func makeSlideAnimationPostBlock(fromView: UIView?, toView: UIView?, isForward: Bool) -> (() -> Void) {
        let direction: CGFloat = isForward ? -1 : 1
        return {
            if let toView = toView {
                let width: CGFloat = fromView?.frame.size.width ?? toView.frame.size.width
                let origin = CGPoint(x: direction * width, y: 0)
                let size = toView.frame.size
                toView.frame = CGRect(origin: CGPoint.zero, size: size)
                fromView?.frame = CGRect(origin: origin, size: size)
            }
        }
    }
}
