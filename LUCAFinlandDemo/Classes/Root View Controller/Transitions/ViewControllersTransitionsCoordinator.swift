import Foundation
import UIKit

class ViewControllersTransitionsCoordinator {

    var animationBlocksFactory: ViewControllerTransitionAnimationsFactory = DefaultViewControllerTransitionAnimationsFactory()

    weak private (set) var containerViewController: UIViewController?
    weak private (set) var currentViewController: UIViewController?
    private var currentViewControllerConstraints: [NSLayoutConstraint]?

    init(containerViewController: UIViewController) {
        self.containerViewController = containerViewController
    }

    func transition(toViewController viewController: UIViewController, animationType: ViewControllerTransitionAnimation) {
        guard let containerViewController = containerViewController else {
            fatalError("Attempted to run transition without a container view controller.")
        }

        let animated = animationType != .none
        let fromViewController = currentViewController
        let fromView = currentViewController?.view
        let fromViewConstraints = currentViewControllerConstraints
        let containerView: UIView = containerViewController.view
        let toView: UIView = viewController.view
        let preTransitionAnimationBlock = animationBlocksFactory.makePreTransitionAnimationBlock(animationType: animationType, fromView: fromView, toView: toView)
        let postTransitionAnimationBlock = animationBlocksFactory.makePostTransitionAnimationBlock(animationType: animationType, fromView: fromView, toView: toView)

        fromViewController?.willMove(toParent: nil)
        fromViewController?.beginAppearanceTransition(false, animated: animated)
        viewController.beginAppearanceTransition(true, animated: animated)

        containerViewController.addChild(viewController)
        containerViewController.view.addSubview(toView)

        if animated {
            preTransitionAnimationBlock?()
        }
        toView.bounds = containerView.bounds
        currentViewControllerConstraints = [
            toView.topAnchor.constraint(equalTo: containerView.topAnchor),
            toView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor),
            toView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            toView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor)
        ]

        containerView.layoutIfNeeded()

        let completion: ((Bool) -> Void) = {
            completed in

            if let fromViewConstraints = fromViewConstraints {
                NSLayoutConstraint.deactivate(fromViewConstraints)
            }

            fromView?.removeFromSuperview()
            fromViewController?.endAppearanceTransition()
            fromViewController?.removeFromParent()

            viewController.didMove(toParent: self.containerViewController)
            viewController.endAppearanceTransition()

            self.currentViewController = viewController
        }

        if animated {
            UIView.animate(withDuration: 0.3,
                    delay: 0,
                    options: [],
                    animations: {
                        postTransitionAnimationBlock?()
                    },
                    completion: completion)
        } else {
            completion(true)
        }
    }
}
