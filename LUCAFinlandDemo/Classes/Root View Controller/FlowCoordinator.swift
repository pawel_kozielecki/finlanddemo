import Foundation
import UIKit

protocol FlowCoordinatorDelegate: class {

    func flowCoordinatorDidFinish(_ flowCoordinator: FlowCoordinator)

}

/*
 * Simple Flow coordinator
 * responsible for laying out views on provided navigation controller and notifying delegate when it is done
 */
protocol FlowCoordinator: class {

    var flowCoordinatorDelegate: FlowCoordinatorDelegate? { get set }
    var presentingNavigationController: UINavigationController? { get }

    func present(animated: Bool)
}
