import Foundation
import UIKit

protocol RootFlowCoordinatorFactory {

    func makeNextRootFlowCoordinator() -> RootFlowCoordinator
}

class DefaultRootFlowCoordinatorFactory: RootFlowCoordinatorFactory {

    unowned let dependencyProvider: DependencyProvider

    init(dependencyProvider: DependencyProvider) {
        self.dependencyProvider = dependencyProvider
    }

    func makeNextRootFlowCoordinator() -> RootFlowCoordinator {
        let loginManager = dependencyProvider.loginManager
        if loginManager.isLoggedIn {
            return MainAppRootFlowCoordinator(dependencyProvider: dependencyProvider)
        } else {
            return AuthenticationRootFlowCoordinator(dependencyProvider: dependencyProvider)
        }
    }
}
