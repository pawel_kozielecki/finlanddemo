import Foundation

protocol GeospatialProvider: class {
    func getAirspaces() -> [GeoAirspace]?
    func getAirspaces(upperLimit meters: Float) -> [GeoAirspace]?
    func getHeliports() -> [GeoHeliport]?
}

protocol GeospatialStorage: class {
}

protocol GeospatialManager: GeospatialProvider, GeospatialStorage {
}

class DefaultGeospatialManager: GeospatialManager {
    func getAirspaces() -> [GeoAirspace]? {
        let airspacesData = readDataAsset(assetName: "geo-airspaces")

        guard let airspaces = try? JSONDecoder().decode([GeoAirspace].self, from: airspacesData) else {
            return nil
        }

        return airspaces
    }

    func getAirspaces(upperLimit meters: Float) -> [GeoAirspace]? {
        guard let airspaces = getAirspaces() else {
            return nil
        }

        return airspaces.filter { $0.properties.upperLimit.valInMeters <= meters }
    }

    func getHeliports() -> [GeoHeliport]? {
        let heliportsData = readDataAsset(assetName: "geo-heliports")

        guard let heliports = try? JSONDecoder().decode([GeoHeliport].self, from: heliportsData) else {
            return nil
        }

        return heliports
    }
}
