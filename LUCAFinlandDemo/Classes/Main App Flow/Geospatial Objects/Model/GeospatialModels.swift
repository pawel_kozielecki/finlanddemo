import Foundation
import GoogleMaps

struct GeoAirspace: Codable {
    let type: String
    let geometry: AirspaceGeometry
    let properties: AirspaceProperties
}

struct AirspaceGeometry: Codable {
    let coordinates: [[[Double]]]
    let type: String

    func makePath() -> GMSMutablePath {
        let path = GMSMutablePath()

        if let coordinates = coordinates.first {
            for coordinate in coordinates {
                if let latitude = coordinate.last, let longitude = coordinate.first {
                    path.add(CLLocationCoordinate2D(latitude: latitude, longitude: longitude))
                }
            }
        }

        return path
    }
}

struct AirspaceProperties: Codable {
    let name: String
    let designator: String
    let isDesignatorICAO: Bool
    let id: String
    let type: String
    let lowerLimit: AirspacePropertyLimit
    let upperLimit: AirspacePropertyLimit

    var color: UIColor {
        return upperLimit.valInMeters <= 4000 ? .yellow : .orange
    }
}

struct AirspacePropertyLimit: Codable {
    let unit: String
    let val: String
    let valInMeters: Float
}

struct GeoHeliport: Codable {
    let geometry: HeliportGeometry
    let properties: HeliportProperties
    let type: String
}

struct HeliportGeometry: Codable {
    let coordinates: [Double]
    let type: String

    var latitude: Double? {
        return coordinates.last
    }

    var longitude: Double? {
        return coordinates.first
    }
}

struct HeliportProperties: Codable {
    let designator: String
    let name: String
    let type: String

    var color: UIColor {
        return .red
    }
}


