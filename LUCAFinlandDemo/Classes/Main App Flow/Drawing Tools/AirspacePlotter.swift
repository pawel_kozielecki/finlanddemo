import Foundation
import UIKit
import GoogleMaps

protocol AirspacePlotterDelegate: class {
    func airspacePlotter(_ plotter: AirspacePlotter, didRequestZoomingToBounds bounds: CoordinateBounds)
}

protocol AirspacePlotter: class {
    var mapView: GMSMapView? { get }
    var delegate: AirspacePlotterDelegate? { get set }

    func plotSelectedAirspace(atSelectedLocation selectedLocation: Coordinates, radius: Double, color: UIColor)
    func getSelectedAirspaceBounds() -> CoordinateBounds?
    func clear()
}

class DefaultAirspacePlotter: AirspacePlotter {

    weak var delegate: AirspacePlotterDelegate?
    var operationsExecutor: AsynchronousOperationsExecutor = MainQueueOperationsExecutor()

    private (set) weak var mapView: GMSMapView?
    private (set) var selectedAirspaceRadius: Double?
    private (set) var selectedLocation: Coordinates?
    private (set) var selectedAirspaceShape: GMSCircle?

    init(mapView: GMSMapView) {
        self.mapView = mapView
    }

    func plotSelectedAirspace(atSelectedLocation selectedLocation: Coordinates, radius: Double, color: UIColor) {
        self.selectedLocation = selectedLocation
        self.selectedAirspaceRadius = radius
        plotSelectedAirspaceShape(color: color)
    }

    func clear() {
        selectedAirspaceShape?.map = nil
    }

    func getSelectedAirspaceBounds() -> CoordinateBounds? {
        return selectedAirspaceShape?.bounds().toCoordinateBounds()
    }
}

private extension DefaultAirspacePlotter {

    func plotSelectedAirspaceShape(color: UIColor) {
        guard let location = selectedLocation,
              let radius = selectedAirspaceRadius else {
            return
        }

        if let region = selectedAirspaceShape {
            region.update(atLocation: location, color: color, radius: radius)
        } else {
            selectedAirspaceShape = GMSCircle.makeSelectedAirspaceRegion(atLocation: location, color: color, radius: radius)
        }
        selectedAirspaceShape?.map = mapView

        let bounds = selectedAirspaceShape!.bounds().toCoordinateBounds()
        delegate?.airspacePlotter(self, didRequestZoomingToBounds: bounds)
    }
}
