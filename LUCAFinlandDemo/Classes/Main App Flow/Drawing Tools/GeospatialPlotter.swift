import Foundation
import UIKit
import GoogleMaps

protocol GeospatialPlotter: class {
    func plot(airspaces: [GeoAirspace], color: UIColor)
    func plot(heliports: [GeoHeliport], color: UIColor)
}

class DefaultGeospatialPlotter: GeospatialPlotter {

    private (set) weak var mapView: GMSMapView?
    private (set) var airspaces: [GeoAirspace] = []
    private (set) var heliports: [GeoHeliport] = []
    private (set) var color = UIColor.dsViolet

    init(mapView: GMSMapView) {
        self.mapView = mapView
    }

    func plot(airspaces: [GeoAirspace], color: UIColor) {
        self.airspaces = airspaces
        self.color = color
        plotAirspaces()
    }

    func plot(heliports: [GeoHeliport], color: UIColor) {
        self.heliports = heliports
        self.color = color
        plotHeliports()
    }
}

private extension DefaultGeospatialPlotter {

    func plotAirspaces() {
        for airspace in airspaces {
            let path = airspace.geometry.makePath()

            let polygon = GMSPolygon(path: path)
            polygon.strokeColor = airspace.properties.color
            polygon.fillColor = polygon.strokeColor?.withAlphaComponent(0.25)

            polygon.map = mapView
        }
    }

    func plotHeliports() {
        for heliport in heliports {
            let latitude = heliport.geometry.latitude!
            let longitude = heliport.geometry.longitude!

            let circleCenter = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            let circle = GMSCircle(position: circleCenter, radius: 1000)
            circle.strokeColor = heliport.properties.color
            circle.fillColor = circle.strokeColor?.withAlphaComponent(0.50)

            circle.map = mapView
        }
    }
}
