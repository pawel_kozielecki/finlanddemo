import Foundation
import UIKit
import GoogleMaps

protocol AirspacesPlotter: class {
    var mapView: GMSMapView? { get }

    func plot(airspaces: [AirspaceLocation], color: UIColor)
    func zoom(toLocation location: AirspaceLocation)
    func clear()
}

class DefaultAirspacesPlotter: AirspacesPlotter {

    var operationsExecutor: AsynchronousOperationsExecutor = MainQueueOperationsExecutor()

    private (set) weak var mapView: GMSMapView?
    private (set) var airspaces: [AirspaceLocation] = []
    private (set) var airspaceShapes: [GMSCircle] = []
    private (set) var color = UIColor.dsViolet

    init(mapView: GMSMapView) {
        self.mapView = mapView
    }

    func plot(airspaces: [AirspaceLocation], color: UIColor) {
        self.airspaces = airspaces
        self.color = color
        plotAirspaces()
    }

    func zoom(toLocation location: AirspaceLocation) {
        let shape = makeAirspaceShape(airspace: location)
        let bounds = shape.bounds()
        mapView?.zoom(toBounds: bounds.toCoordinateBounds())
    }

    func clear() {
        for shape in airspaceShapes {
            shape.map = nil
        }
        airspaceShapes = []
    }
}

private extension DefaultAirspacesPlotter {

    func plotAirspaces() {
        clear()
        for airspace in airspaces {
            let shape = makeAirspaceShape(airspace: airspace)
            shape.map = mapView
            airspaceShapes.append(shape)
        }
    }

    func makeAirspaceShape(airspace: AirspaceLocation) -> GMSCircle {
        return GMSCircle.makeSelectedAirspaceRegion(atLocation: airspace.location, color: color, radius: Double(airspace.radius))
    }
}

extension GMSMapView {

    enum GMSMapViewAnimationDuration: Double {
        case long = 1.5
        case medium = 1.0
        case short = 0.5
    }

    func zoom(toBounds bounds: CoordinateBounds, animated: Bool = true, duration: GMSMapViewAnimationDuration = .short) {
        let camera = GMSCameraUpdate.fit(bounds.toGMSCoordinateBounds(), with: UIEdgeInsets(top: 50, left: 30, bottom: 100, right: 30))
        if animated {
            CATransaction.begin()
            CATransaction.setAnimationDuration(duration.rawValue)
            animate(with: camera)
            CATransaction.commit()
        } else {
            moveCamera(camera)
        }
    }

    func zoom(toLocation location: Coordinates, animated: Bool = true, duration: GMSMapViewAnimationDuration = .short) {
        let camera = GMSCameraPosition.camera(withLatitude: location.latitude, longitude: location.longitude, zoom: ZoomLevel.high.rawValue)
        if animated {
            CATransaction.begin()
            CATransaction.setAnimationDuration(duration.rawValue)
            animate(to: camera)
            CATransaction.commit()
        } else {
            self.camera = camera
        }
    }
}
