import Foundation
import UIKit
import GoogleMaps

protocol PlaneRoutePlotterDelegate: class {
    func planeRoutePlotter(_ plotter: PlaneRoutePlotter, didRequestZoomingToBounds bounds: CoordinateBounds)
}

protocol PlaneRoutePlotter: class {
    var mapView: GMSMapView? { get }
    var delegate: PlaneRoutePlotterDelegate? { get set }

    func plot(selectedRoute route: PlaneRoute, color: UIColor)
    func getSelectedRouteBounds() -> CoordinateBounds?
    func clear()
}

class DefaultPlaneRoutePlotter: PlaneRoutePlotter {

    weak var delegate: PlaneRoutePlotterDelegate?
    var operationsExecutor: AsynchronousOperationsExecutor = MainQueueOperationsExecutor()

    private (set) weak var mapView: GMSMapView?
    private (set) var selectedRoute: GMSPolyline?
    private (set) var selectedRouteMarkers = [GMSMarker]()

    init(mapView: GMSMapView) {
        self.mapView = mapView
    }

    func plot(selectedRoute route: PlaneRoute, color: UIColor) {
        self.selectedRoute = route.toPolyline()
        self.selectedRouteMarkers = route.toMarkers()
        selectedRoute?.strokeColor = color
        selectedRoute?.strokeWidth = 3
        plotSelectedRoute(color: color)
    }

    func getSelectedRouteBounds() -> CoordinateBounds? {
        if let path = selectedRoute?.path {
            return GMSCoordinateBounds(path: path).toCoordinateBounds()
        }
        return nil
    }

    func clear() {
        selectedRoute?.map = nil
        for marker in selectedRouteMarkers {
            marker.map = nil
        }
    }
}

private extension DefaultPlaneRoutePlotter {

    func plotSelectedRoute(color: UIColor) {
        if let route = selectedRoute {
            route.map = nil
        }
        selectedRoute?.map = mapView

        for marker in selectedRouteMarkers {
            marker.iconView?.tintColor = .dsViolet
            marker.map = mapView
        }

        if let bounds = getSelectedRouteBounds() {
            delegate?.planeRoutePlotter(self, didRequestZoomingToBounds: bounds)
        }
    }
}
