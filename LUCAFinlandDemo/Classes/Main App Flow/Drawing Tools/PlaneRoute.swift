import Foundation
import GoogleMaps

struct PlaneRoute: Codable, Equatable {

    let wayPoints: [Coordinates]
    let margin: Int

    private enum CodingKeys: String, CodingKey {
        case wayPoints = "way_points"
        case margin
    }
}

extension PlaneRoute {

    func routeByAdding(wayPoint point: Coordinates) -> PlaneRoute {
        var points = wayPoints
        points.append(point)
        return PlaneRoute(wayPoints: points, margin: margin)
    }

    func toPolyline() -> GMSPolyline {
        let path = GMSMutablePath()
        for point in self.wayPoints {
            path.add(point.toCLCoordinates())
        }
        let polyline = GMSPolyline(path: path)
        polyline.zIndex = 998
        return polyline
    }

    func toMarkers() -> [GMSMarker] {
        var markers = [GMSMarker]()
        for point in self.wayPoints {
            let marker = GMSMarker(position: point.toCLCoordinates())
            marker.zIndex = 999
            markers.append(marker)
        }
        return markers
    }
}
