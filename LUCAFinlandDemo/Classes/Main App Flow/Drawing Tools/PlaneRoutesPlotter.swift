import Foundation
import UIKit
import GoogleMaps

protocol PlaneRoutesPlotter: class {
    var mapView: GMSMapView? { get }

    func plot(routes: [PlaneRoute], color: UIColor)
    func zoom(toRoute route: PlaneRoute)
    func clear()
}

class DefaultPlaneRoutesPlotter: PlaneRoutesPlotter {

    var operationsExecutor: AsynchronousOperationsExecutor = MainQueueOperationsExecutor()

    private (set) weak var mapView: GMSMapView?
    private (set) var routes: [PlaneRoute] = []
    private (set) var polylines: [GMSPolyline] = []
    private (set) var markers: [GMSMarker] = []
    private (set) var color: UIColor = .dsViolet

    init(mapView: GMSMapView) {
        self.mapView = mapView
    }

    func plot(routes: [PlaneRoute], color: UIColor) {
        self.routes = routes
        self.color = color
        plotRoutes()
    }

    func zoom(toRoute route: PlaneRoute) {
        let shape = route.toPolyline()
        if let path = shape.path {
            let bounds = GMSCoordinateBounds(path: path)
            mapView?.zoom(toBounds: bounds.toCoordinateBounds(), animated: false)
        }
    }

    func clear() {
        for polylines in polylines {
            polylines.map = nil
        }
        for marker in markers {
            marker.map = nil
        }
        markers = []
        polylines = []
    }
}

private extension DefaultPlaneRoutesPlotter {

    func plotRoutes() {
        clear()
        for route in routes {
            let polyline = route.toPolyline()
            polyline.strokeColor = color
            polyline.strokeWidth = 3
            polyline.map = mapView
            self.polylines.append(polyline)
            self.markers = route.toMarkers()
            for marker in markers {
                marker.iconView?.tintColor = color
                marker.map = mapView
            }
        }
    }
}
