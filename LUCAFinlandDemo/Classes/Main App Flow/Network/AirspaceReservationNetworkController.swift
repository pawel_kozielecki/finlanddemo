import Foundation

class AirspaceReservationNetworkController {

    let networkModule: NetworkModule

    required init(networkModule: NetworkModule) {
        self.networkModule = networkModule
    }

    func reserve(planeRoute: PlaneRouteDetails, completion: ((ReservationModel?, NSError?) -> Void)?) {
        let request = PlaneRouteReservationRequest(planeRouteDetails: planeRoute)
        let decoder = JSONDecoder.makeDateAwareDecoder()
        networkModule.execute(request: request) {
            response, error in
            if let error = error {
                completion?(nil, error)
            } else if let resultData = response.data {
                do {
                    let reservationResult = try decoder.decode(ReservationModel.self, from: resultData)
                    completion?(reservationResult, nil)
                } catch {
                    completion?(nil, error as NSError)
                }
            }
        }
    }

    func reserve(circularAirspace: CircularAirspaceDetails, completion: ((ReservationModel?, NSError?) -> Void)?) {
        let request = CircularAirspaceReservationRequest(airspaceDetails: circularAirspace)
        let decoder = JSONDecoder.makeDateAwareDecoder()
        networkModule.execute(request: request) {
            response, error in
            if let error = error {
                completion?(nil, error)
            } else if let resultData = response.data {
                do {
                    let reservationResult = try decoder.decode(ReservationModel.self, from: resultData)
                    completion?(reservationResult, nil)
                } catch {
                    completion?(nil, error as NSError)
                }
            }
        }
    }

    func reserve(customAirspace: CustomAirspaceDetails, completion: ((ReservationModel?, NSError?) -> Void)?) {
        let request = CustomAirspaceReservationRequest(airspaceDetails: customAirspace)
        let decoder = JSONDecoder.makeDateAwareDecoder()
        networkModule.execute(request: request) {
            response, error in
            if let error = error {
                completion?(nil, error)
            } else if let resultData = response.data {
                do {
                    let reservationResult = try decoder.decode(ReservationModel.self, from: resultData)
                    completion?(reservationResult, nil)
                } catch {
                    completion?(nil, error as NSError)
                }
            }
        }
    }

    func getReservations(completion: (([ReservationModel]?, NSError?) -> Void)?) {
        let request = GetReservationsRequest()
        let decoder = JSONDecoder.makeDateAwareDecoder()
        networkModule.execute(request: request) {
            response, error in
            if let error = error {
                completion?(nil, error)
            } else if let resultData = response.data {
                do {
                    let userReservationsResult = try decoder.decode(UserReservations.self, from: resultData)
                    completion?(userReservationsResult.reservations ?? [], nil)
                } catch {
                    completion?(nil, error as NSError)
                }
            }
        }
    }

    func cancel(reservation: ReservationModel, completion: ((NSError?) -> Void)?) {
        let request = CancelReservationRequest(reservationId: reservation.id)
        networkModule.execute(request: request) {
            response, error in
            completion?(error)
        }
    }
}

struct CircularAirspaceDetails: Codable {
    let location: AirspaceLocation
    let startDate: Date
    let endDate: Date
    let height: Int
    let flightType: FlightType
    let reservationType: ReservationType
}

struct CustomAirspaceDetails: Codable {
    let points: [Coordinates]
    let startDate: Date
    let endDate: Date
    let height: Int
    let flightType: FlightType
    let reservationType: ReservationType
}

struct PlaneRouteDetails: Codable {
    let route: PlaneRoute
    let startDate: Date
    let endDate: Date
    let height: Int
    let flightType: FlightType
}

struct UserReservations: Codable {
    let reservations: [ReservationModel]?
}

extension JSONDecoder {

    static func makeDateAwareDecoder() -> JSONDecoder {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(DateFormatter.makeDateAndTimeFormatter())
        return decoder
    }
}
