import Foundation

struct GetReservationsRequest: NetworkRequest {
    let method = RequestMethod.get
    let path = "/reservations"
    let includeAuthenticationToken = true
    let params: [String: Any]? = nil
    let isAuthenticationRequest: Bool = false
    let body: [String: Any]? = nil
}
