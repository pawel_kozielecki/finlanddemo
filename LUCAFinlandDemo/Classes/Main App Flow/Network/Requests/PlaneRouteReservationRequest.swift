import Foundation

struct PlaneRouteReservationRequest: NetworkRequest {
    let method = RequestMethod.post
    let path = "/reservations/plane_route"
    let includeAuthenticationToken = true
    let params: [String: Any]? = nil
    let isAuthenticationRequest: Bool = false

    let body: [String: Any]?

    init(planeRouteDetails: PlaneRouteDetails) {
        let route = planeRouteDetails.route
        let dateFormatter = DateFormatter.makeDateAndTimeFormatter()
        body = [
            "way_points": route.wayPoints.map({ $0.toJSON() }),
            "flight_type": planeRouteDetails.flightType.rawValue,
            "margin": route.margin,
            "start_date": dateFormatter.string(from: planeRouteDetails.startDate),
            "end_date": dateFormatter.string(from: planeRouteDetails.endDate),
            "height": planeRouteDetails.height
        ]
    }
}
