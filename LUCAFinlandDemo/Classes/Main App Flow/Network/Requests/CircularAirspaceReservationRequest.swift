import Foundation

struct CircularAirspaceReservationRequest: NetworkRequest {
    let method = RequestMethod.post
    let path = "/reservations/circular_airspace"
    let includeAuthenticationToken = true
    let params: [String: Any]? = nil
    let isAuthenticationRequest: Bool = false

    let body: [String: Any]?

    init(airspaceDetails: CircularAirspaceDetails) {
        let dateFormatter = DateFormatter.makeDateAndTimeFormatter()
        body = [
            "location": airspaceDetails.location.toJSON(),
            "flight_type": airspaceDetails.flightType.rawValue,
            "reservation_type": airspaceDetails.reservationType.rawValue,
            "start_date": dateFormatter.string(from: airspaceDetails.startDate),
            "end_date": dateFormatter.string(from: airspaceDetails.endDate),
            "height": airspaceDetails.height
        ]
    }
}
