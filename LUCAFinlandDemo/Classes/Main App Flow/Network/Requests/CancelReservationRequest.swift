import Foundation

struct CancelReservationRequest: NetworkRequest {
    let method = RequestMethod.delete
    let includeAuthenticationToken = true
    let params: [String: Any]? = nil
    let isAuthenticationRequest: Bool = false
    let body: [String: Any]? = nil

    let path: String

    init(reservationId: String) {
        path = "/reservations/\(reservationId)"
    }
}
