import Foundation

struct CustomAirspaceReservationRequest: NetworkRequest {
    let method = RequestMethod.post
    let path = "/reservations/custom_airspace"
    let includeAuthenticationToken = true
    let params: [String: Any]? = nil
    let isAuthenticationRequest: Bool = false

    let body: [String: Any]?

    init(airspaceDetails: CustomAirspaceDetails) {
        let dateFormatter = DateFormatter.makeDateAndTimeFormatter()
        body = [
            "way_points": airspaceDetails.points.map({ $0.toJSON() }),
            "flight_type": airspaceDetails.flightType.rawValue,
            "reservation_type": airspaceDetails.reservationType.rawValue,
            "start_date": dateFormatter.string(from: airspaceDetails.startDate),
            "end_date": dateFormatter.string(from: airspaceDetails.endDate),
            "height": airspaceDetails.height
        ]
    }
}
