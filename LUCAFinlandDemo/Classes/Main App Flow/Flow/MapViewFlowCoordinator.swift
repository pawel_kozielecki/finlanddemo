import Foundation
import UIKit

class MapViewFlowCoordinator: FlowCoordinator {

    let dependencyProvider: DependencyProvider

    weak var flowCoordinatorDelegate: FlowCoordinatorDelegate?
    weak var presentingNavigationController: UINavigationController?

    init(dependencyProvider: DependencyProvider, presentingNavigationController: UINavigationController) {
        self.dependencyProvider = dependencyProvider
        self.presentingNavigationController = presentingNavigationController
    }

    func present(animated: Bool) {
        let rootViewController = MapViewController(
                locationManager: dependencyProvider.locationManager,
                loginManager: dependencyProvider.loginManager,
                reservationManager: dependencyProvider.airspaceReservationManager
        )
        rootViewController.delegate = self
        presentingNavigationController?.pushViewController(rootViewController, animated: animated)
    }
}

extension MapViewFlowCoordinator: MapViewControllerDelegate {

    func mapViewControllerDidFinish(_ viewController: MapViewController) {
        flowCoordinatorDelegate?.flowCoordinatorDidFinish(self)
    }

    func mapViewController(_ viewController: MapViewController, didRequestStartingReservation reservationType: ReservationType) {
        reservationManager.set(reservationType: reservationType)
        if reservationType == .stationary {
            reservationManager.set(flightType: .commercial)
            let controller = DateSelectionViewController()
            controller.delegate = self
            presentingNavigationController?.pushViewController(controller, animated: true)
        } else {
//            let roleViewController = FlightFormViewController()
            let roleViewController = SelectRoleViewController(reservationType: reservationType)
            roleViewController.delegate = self
            presentingNavigationController?.pushViewController(roleViewController, animated: true)
        }
    }

    func mapViewControllerDidRequestReservationsList(_ viewController: MapViewController) {
        let controller = ReservationsListViewController(reservationManager: reservationManager)
        controller.delegate = self
        presentingNavigationController?.present(controller, animated: true)
    }
}

extension MapViewFlowCoordinator: ReservationsListViewControllerDelegate {
    func reservationsListViewControllerDidCancel(_ viewController: ReservationsListViewController) {
        presentingNavigationController?.dismiss(animated: true)
    }

    func reservationsListViewController(_ viewController: ReservationsListViewController, didSelectReservation reservation: ReservationModel) {
        mapViewController.show(reservation: reservation)
        presentingNavigationController?.dismiss(animated: true)
    }
}

extension MapViewFlowCoordinator: SelectRoleViewControllerDelegate {

    func selectRoleViewControllerDidCancel(_ viewController: SelectRoleViewController) {
        presentingNavigationController?.popViewController(animated: true)
    }

    func selectRoleViewController(_ viewController: SelectRoleViewController, didSelectFlightType flightType: FlightType) {
        reservationManager.set(flightType: flightType)
        let controller = DateSelectionViewController()
        controller.delegate = self
        presentingNavigationController?.pushViewController(controller, animated: true)
    }
}

extension MapViewFlowCoordinator: DateSelectionViewControllerDelegate {

    func dateSelectionViewControllerDidFinish(_ viewController: DateSelectionViewController) {
        presentingNavigationController?.popViewController(animated: true)
    }

    func dateSelectionViewController(_ viewController: DateSelectionViewController, didSelectStartDate startDate: Date, andFinishDate finishDate: Date) {
        reservationManager.set(startDate: startDate, endDate: finishDate)
        if reservationManager.reservationType == .plane {
            let controller = RouteSelectionViewController(locationManager: dependencyProvider.locationManager)
            controller.delegate = self
            presentingNavigationController?.pushViewController(controller, animated: true)
        } else {
            let controller = AirspaceSelectionViewController(locationManager: dependencyProvider.locationManager)
            controller.delegate = self
            presentingNavigationController?.pushViewController(controller, animated: true)
        }
    }
}

extension MapViewFlowCoordinator: AirspaceSelectionViewControllerDelegate {

    func airspaceSelectionViewControllerDidCancel(_ viewController: AirspaceSelectionViewController) {
        presentingNavigationController?.popViewController(animated: true)
    }

    func airspaceSelectionViewController(_ viewController: AirspaceSelectionViewController, didFinishWithCustomAirspace airspacePoints: [Coordinates]) {
        reservationManager.set(customAirspaceOfPoints: airspacePoints)
        showHeightSelectionScreen()
    }

    func airspaceSelectionViewController(_ viewController: AirspaceSelectionViewController, didFinishWithStandardAirspace airspaceLocation: AirspaceLocation) {
        reservationManager.set(airspaceLocation: airspaceLocation)
        showHeightSelectionScreen()
    }
}

extension MapViewFlowCoordinator: RouteSelectionViewControllerDelegate {

    func routeSelectionViewControllerDidCancel(_ viewController: RouteSelectionViewController) {
        presentingNavigationController?.popViewController(animated: true)
    }

    func routeSelectionViewController(_ viewController: RouteSelectionViewController, didFinishWithRoute route: PlaneRoute) {
        reservationManager.set(planeRoute: route)
        showFlightFormScreen()
    }
}

extension MapViewFlowCoordinator: HeightSelectionViewControllerDelegate {

    func heightSelectionViewControllerDidCancel(_ viewController: HeightSelectionViewController) {
        presentingNavigationController?.popViewController(animated: true)
    }

    func heightSelectionViewController(_ viewController: HeightSelectionViewController, didSelectHeight height: Int) {
        reservationManager.set(height: height)
        showWeatherScreen()
    }
}

extension MapViewFlowCoordinator: FlightFormViewControllerDelegate {

    func flightFormViewControllerDidCancel(_ viewController: FlightFormViewController) {
        presentingNavigationController?.popViewController(animated: true)
    }

    func flightFormViewController(_ viewController: FlightFormViewController, didSelectHeight height: Int) {
        reservationManager.set(height: height)
        showWeatherScreen()
    }
}

extension MapViewFlowCoordinator: WeatherViewControllerDelegate {
    
    func weatherViewControllerDidBack(_ viewController: WeatherViewController) {
        presentingNavigationController?.popViewController(animated: true)
    }
    
    func weatherViewControllerDidNext(_ viewController: WeatherViewController) {
        showSummaryScreen()
    }
}

extension MapViewFlowCoordinator: SummaryViewControllerDelegate {

    func summaryViewControllerDidCancel(_ viewController: SummaryViewController) {
        presentingNavigationController?.popViewController(animated: true)
    }

    func summaryViewControllerDidFinish(_ viewController: SummaryViewController) {
        presentingNavigationController?.popToRootViewController(animated: true)
    }
}

private extension MapViewFlowCoordinator {

    var mapViewController: MapViewController {
        return presentingNavigationController?.viewControllers.first as! MapViewController
    }

    var reservationManager: AirspaceReservationManager {
        return dependencyProvider.airspaceReservationManager
    }

    func showHeightSelectionScreen() {
        let controller = HeightSelectionViewController()
        controller.delegate = self
        presentingNavigationController?.pushViewController(controller, animated: true)
    }

    func showFlightFormScreen() {
        let controller = FlightFormViewController()
        controller.delegate = self
        presentingNavigationController?.pushViewController(controller, animated: true)
    }

    func showWeatherScreen() {
        let controller = WeatherViewController()
        controller.delegate = self
        presentingNavigationController?.pushViewController(controller, animated: true)
    }

    func showSummaryScreen() {
        let controller = SummaryViewController(reservationManager: reservationManager)
        controller.delegate = self
        presentingNavigationController?.pushViewController(controller, animated: true)
    }
}
