import Foundation
import UIKit

class MainAppRootFlowCoordinator: RootFlowCoordinator {

    let type: RootFlowCoordinatorType = .loggedIn
    let dependencyProvider: DependencyProvider

    var navigationController: UINavigationController
    var rootFlowCoordinatorDelegate: RootFlowCoordinatorDelegate?
    var mapFlowCoordinator: MapViewFlowCoordinator
    var rootViewController: UIViewController {
        return navigationController
    }

    init(dependencyProvider: DependencyProvider) {
        self.dependencyProvider = dependencyProvider
        self.navigationController = MainAppRootFlowCoordinator.makeNavigationController()
        self.mapFlowCoordinator = MapViewFlowCoordinator(dependencyProvider: dependencyProvider, presentingNavigationController: navigationController)
        self.mapFlowCoordinator.flowCoordinatorDelegate = self
    }

    func start() {
        mapFlowCoordinator.present(animated: true)
    }
}

extension MainAppRootFlowCoordinator: FlowCoordinatorDelegate {

    func flowCoordinatorDidFinish(_ flowCoordinator: FlowCoordinator) {
        rootFlowCoordinatorDelegate?.rootFlowCoordinatorDidFinish(self)
    }
}

private extension MainAppRootFlowCoordinator {

    class func makeNavigationController() -> UINavigationController {
        let navigationController = UINavigationController()
        navigationController.isNavigationBarHidden = true
        return navigationController
    }
}
