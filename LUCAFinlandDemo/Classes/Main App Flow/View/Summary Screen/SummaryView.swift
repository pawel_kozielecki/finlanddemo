import Foundation
import UIKit

protocol SummaryViewDelegate: class {
    func summaryViewDidCancel(_ view: SummaryView)
    func summaryViewDidFinish(_ view: SummaryView)
}

class SummaryView: UIView {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var labelRole: UILabel!
    @IBOutlet weak var labelFlightType: UILabel!
    @IBOutlet weak var labelDates: UILabel!
    @IBOutlet weak var labelHeight: UILabel!
    @IBOutlet weak var buttonNext: DSRoundedButton!

    weak var delegate: SummaryViewDelegate?

    @IBAction func didTapNextButton(_ sender: UIButton) {
        delegate?.summaryViewDidFinish(self)
    }

    @IBAction func didTapCancelButton(_ sender: UIButton) {
        delegate?.summaryViewDidCancel(self)
    }

    func setup(role: ReservationType, flightType: FlightType, startDate: Date, stopDate: Date, height: Int) {
        arrangeView(role: role, flightType: flightType, startDate: startDate, stopDate: stopDate, height: height)
    }
}

private extension SummaryView {

    func arrangeView(role: ReservationType, flightType: FlightType, startDate: Date, stopDate: Date, height: Int) {
        labelTitle.text = "SUMMARY_SCREEN_TITLE".localized
        labelDescription.text = "SUMMARY_SCREEN_DESCRIPTION".localized

        labelRole.text = "\("SUMMARY_SCREEN_ROLE".localized):\n     \(role.localizedDescription)"
        labelFlightType.text = "\("SUMMARY_SCREEN_FLIGHT_TYPE".localized):\n     \(flightType.localizedDescription)"
        labelHeight.text = "\("SUMMARY_SCREEN_HEIGHT".localized):\n     \(height) m"

        let dateFormatter = DateFormatter.makeSimpleDateAndTimeFormatter()
        let dateFrom = dateFormatter.string(from: startDate)
        let dateTo = dateFormatter.string(from: stopDate)
        var dateString = "SUMMARY_SCREEN_DATES".localized
        dateString = dateString.replacingOccurrences(of: "%from%", with: dateFrom)
        dateString = dateString.replacingOccurrences(of: "%to%", with: dateTo)

        labelDates.text = dateString

        layoutIfNeeded()
    }
}
