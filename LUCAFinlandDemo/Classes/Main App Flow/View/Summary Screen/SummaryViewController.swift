import Foundation
import UIKit

protocol SummaryViewControllerDelegate: class {
    func summaryViewControllerDidCancel(_ viewController: SummaryViewController)
    func summaryViewControllerDidFinish(_ viewController: SummaryViewController)
}

class SummaryViewController: UIViewController {

    let reservationManager: AirspaceReservationManager

    weak var delegate: SummaryViewControllerDelegate?
    var progressHud: ProgressHud?
    var alert: InfoAlert?

    init(reservationManager: AirspaceReservationManager) {
        self.reservationManager = reservationManager

        super.init(nibName: nil, bundle: nil)

        progressHud = DefaultProgressHud(view: view)
        alert = SimpleInfoAlert(presentingViewController: self)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("Init(coder) not implemented")
    }

    override func loadView() {
        let view: SummaryView = Bundle.loadView(fromNib: "SummaryView")
        view.delegate = self
        if let role = reservationManager.reservationType,
           let flightType = reservationManager.flightType,
           let startDate = reservationManager.startDate,
           let endDate = reservationManager.endDate,
           let height = reservationManager.height {
            view.setup(role: role, flightType: flightType, startDate: startDate, stopDate: endDate, height: height)
        }
        self.view = view
    }
}

extension SummaryViewController: SummaryViewDelegate {

    func summaryViewDidCancel(_ view: SummaryView) {
        delegate?.summaryViewControllerDidCancel(self)
    }

    func summaryViewDidFinish(_ view: SummaryView) {
        bookAirspace()
    }
}

private extension SummaryViewController {

    func bookAirspace() {
        progressHud?.show()
        reservationManager.makeReservation() {
            [weak self] reservedAirspace, error in
            guard let strongSelf = self else {
                return
            }

            if let error = error {
                let title = "RESERVATION_ERROR_TITLE".localized
                let message = "RESERVATION_ERROR_MESSAGE".localized
                strongSelf.alert?.show(title: title, message: message, completion: nil)
            } else {
                strongSelf.reservationManager.clear()
                strongSelf.delegate?.summaryViewControllerDidFinish(strongSelf)
            }
            strongSelf.progressHud?.hide()
        }
    }
}
