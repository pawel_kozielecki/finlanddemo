import Foundation
import UIKit

protocol RouteSelectionViewControllerDelegate: class {

    func routeSelectionViewControllerDidCancel(_ viewController: RouteSelectionViewController)
    func routeSelectionViewController(_ viewController: RouteSelectionViewController, didFinishWithRoute route: PlaneRoute)
}

class RouteSelectionViewController: UIViewController {

    let locationManager: LocationManager
    weak var delegate: RouteSelectionViewControllerDelegate?

    init(locationManager: LocationManager) {
        self.locationManager = locationManager
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("Init(coder) not implemented")
    }

    override func loadView() {
        let view: RouteSelectionView = Bundle.loadView(fromNib: "RouteSelectionView")
        view.delegate = self
        self.view = view
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        locationManager.delegate = self
        mainView.userLocation = locationManager.currentLocation
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        locationManager.delegate = nil
    }
}

extension RouteSelectionViewController: LocationProviderDelegate {

    func locationProvider(_ provider: LocationProvider, didUpdateLocation location: Coordinates, updatesCount: Int) {
        mainView.userLocation = location
    }
}

extension RouteSelectionViewController: RouteSelectionViewDelegate {

    func routeSelectionViewDidCancel(_ view: RouteSelectionView) {
        delegate?.routeSelectionViewControllerDidCancel(self)
    }

    func airspaceSelectionView(_ view: RouteSelectionView, didFinishWithRoute route: PlaneRoute) {
        delegate?.routeSelectionViewController(self, didFinishWithRoute: route)
    }
}

private extension RouteSelectionViewController {

    var mainView: RouteSelectionView {
        return view as! RouteSelectionView
    }
}
