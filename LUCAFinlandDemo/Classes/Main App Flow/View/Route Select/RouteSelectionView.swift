import Foundation
import UIKit
import GoogleMaps

protocol RouteSelectionViewDelegate: class {
    func routeSelectionViewDidCancel(_ view: RouteSelectionView)
    func airspaceSelectionView(_ view: RouteSelectionView, didFinishWithRoute route: PlaneRoute)
}

class RouteSelectionView: UIView {

    @IBOutlet weak var buttonNext: DSRoundedButton!
    @IBOutlet weak var mapContainer: UIView!
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var buttonRemoveMarker: DSRoundedButton!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var labelMargin: UILabel!


    weak var delegate: RouteSelectionViewDelegate?

    var selectedRoutePlotter: PlaneRoutePlotter?
    private (set) var points = [Coordinates]()

    private var gmsMapView: GMSMapView!
    private var isMapInitialized = false

    var userLocation: Coordinates? {
        didSet {
            zoomToUserLocation()
        }
    }

    @IBAction func didTapCancelButton(_ sender: Any) {
        delegate?.routeSelectionViewDidCancel(self)
    }

    @IBAction func didTapNextButton(_ sender: Any) {
        delegate?.airspaceSelectionView(self, didFinishWithRoute: currentRoute)
    }

    @IBAction func didTapRemoveMarker(_ sender: Any) {
        points.removeLast()
        manageSubviewsAvailability()
        redrawRoute()
    }

    @IBAction func didSetMargin(_ sender: UISlider) {
        let margin = Int(slider.value)
        labelMargin.text = "\(margin) m"
        redrawRoute()
    }

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        arrangeView()
        manageSubviewsAvailability()
        selectedRoutePlotter = DefaultPlaneRoutePlotter(mapView: gmsMapView)
    }
}

extension RouteSelectionView: GMSMapViewDelegate {

    public func mapView(_ mapView: GMSMapView, didLongPressAt coordinate: CLLocationCoordinate2D) {
        points.append(coordinate.toCoordinates())
        manageSubviewsAvailability()
        redrawRoute()
    }
}

private extension RouteSelectionView {

    func zoomToAirspace(bounds: CoordinateBounds) {
        let camera = GMSCameraUpdate.fit(bounds.toGMSCoordinateBounds(), with: UIEdgeInsets(top: 50, left: 30, bottom: 100, right: 30))
        gmsMapView.moveCamera(camera)
    }

    func manageSubviewsAvailability() {
        buttonRemoveMarker.isEnabled = !points.isEmpty
        buttonNext.isEnabled = points.count > 1
        slider.isHidden = points.count < 2
        labelMargin.isHidden = points.count < 2
    }

    func redrawRoute() {
        selectedRoutePlotter?.clear()
        selectedRoutePlotter?.plot(selectedRoute: currentRoute, color: .dsViolet)
    }

    func initGoogleMapView() {
        let location = userLocation ?? Coordinates.defaultLocation
        let camera = GMSCameraPosition.camera(withLatitude: location.latitude, longitude: location.longitude, zoom: ZoomLevel.high.rawValue)

        gmsMapView = GMSMapView.map(withFrame: mapContainer.bounds, camera: camera)
        mapContainer.addSubview(self.gmsMapView)
        gmsMapView.delegate = self
        gmsMapView.isMyLocationEnabled = true
        if let styleURL = Bundle.main.url(forResource: "mapDarkStyle", withExtension: "json") {
            gmsMapView.mapStyle = try? GMSMapStyle(contentsOfFileURL: styleURL)
        }

        gmsMapView.snp.prepareConstraints {
            make in
            make.margins.equalToSuperview()
        }

        isMapInitialized = true
    }

    func zoomToUserLocation() {
        if let location = userLocation, isMapInitialized {
            let camera = GMSCameraPosition.camera(withLatitude: location.latitude, longitude: location.longitude, zoom: ZoomLevel.high.rawValue)
            gmsMapView.camera = camera
        }
    }

    func arrangeView() {
        initGoogleMapView()

        buttonNext.isEnabled = false
        buttonRemoveMarker.setTitle("PLANE_ROUTE_SELECT_SCREEN_BUTTON_REMOVE_MARKER".localized, for: .normal)
        labelDescription.text = "PLANE_ROUTE_SELECT_SCREEN_DESCRIPTION".localized

        layoutIfNeeded()
    }

    var currentRoute: PlaneRoute {
        return PlaneRoute(wayPoints: points, margin: Int(slider.value))
    }
}
