import Foundation
import UIKit

protocol DateSelectionViewControllerDelegate: class {

    func dateSelectionViewControllerDidFinish(_ viewController: DateSelectionViewController)
    func dateSelectionViewController(_ viewController: DateSelectionViewController, didSelectStartDate startDate: Date, andFinishDate finishDate: Date)
}

class DateSelectionViewController: UIViewController {

    weak var delegate: DateSelectionViewControllerDelegate?

    init() {
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("Init(coder) not implemented")
    }

    override func loadView() {
        let view: DateSelectionView = Bundle.loadView(fromNib: "DateSelectionView")
        view.setup()
        view.delegate = self
        self.view = view
    }
}

extension DateSelectionViewController: DateSelectionViewDelegate {
    func dateSelectionViewDidFinish(_ view: DateSelectionView) {
        delegate?.dateSelectionViewControllerDidFinish(self)
    }

    func dateSelectionView(_ view: DateSelectionView, didSelectStartDate startDate: Date, andFinishDate finishDate: Date) {
        delegate?.dateSelectionViewController(self, didSelectStartDate: startDate, andFinishDate: finishDate)
    }
}
