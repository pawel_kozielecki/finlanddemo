import Foundation
import UIKit

protocol DateSelectionViewDelegate: class {

    func dateSelectionViewDidFinish(_ view: DateSelectionView)
    func dateSelectionView(_ view: DateSelectionView, didSelectStartDate startDate: Date, andFinishDate finishDate: Date)
}

class DateSelectionView: UIView {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var buttonNext: DSRoundedButton!
    @IBOutlet weak var inputDateFrom: DSTextfield!
    @IBOutlet weak var inputDateTo: DSTextfield!

    weak var delegate: DateSelectionViewDelegate?

    private var startDate: Date?
    private var finishDate: Date?

    func setup() {
        arrangeView()
        updateButtonAvailability()
    }

    @IBAction func didTapGoBackButton(_ sender: UIButton) {
        delegate?.dateSelectionViewDidFinish(self)
    }

    @IBAction func didTapNextButton(_ sender: UIButton) {
        guard let startDate = startDate, let finishDate = finishDate else {
            return
        }

        delegate?.dateSelectionView(self, didSelectStartDate: startDate, andFinishDate: finishDate)
    }
}

extension DateSelectionView: DSTextFieldDelegate {

    func textField(_ field: DSTextfield, didChangeValue value: String?) {
        let dateFormatter = DateFormatter.makeSimpleDateAndTimeFormatter()
        let value = field.value ?? ""
        if field === inputDateFrom {
            startDate = dateFormatter.date(from: value)
            inputDateTo.isUserInteractionEnabled = true
            (inputDateTo.textField.inputView as? UIDatePicker)?.minimumDate = startDate?.addingTimeInterval(60)
        } else {
            finishDate = dateFormatter.date(from: value)
            (inputDateFrom.textField.inputView as? UIDatePicker)?.maximumDate = finishDate?.addingTimeInterval(-60)
        }
        updateButtonAvailability()
    }

    func textFieldDidReturn(_ field: DSTextfield) {
    }
}

private extension DateSelectionView {

    func updateButtonAvailability() {
        buttonNext.isEnabled = inputDateTo.value?.isEmpty == false && inputDateFrom.value?.isEmpty == false
    }

    func arrangeView() {
        labelTitle.text = "DATE_SELECT_SCREEN_TITLE".localized
        labelDescription.text = "DATE_SELECT_SCREEN_MESSAGE".localized
        inputDateFrom.delegate = self
        inputDateTo.delegate = self
        inputDateTo.isUserInteractionEnabled = false
        buttonNext.isEnabled = false
        layoutIfNeeded()
    }
}
