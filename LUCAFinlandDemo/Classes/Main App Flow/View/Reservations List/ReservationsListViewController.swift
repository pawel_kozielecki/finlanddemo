import Foundation
import UIKit

protocol ReservationsListViewControllerDelegate: class {
    func reservationsListViewControllerDidCancel(_ viewController: ReservationsListViewController)
    func reservationsListViewController(_ viewController: ReservationsListViewController, didSelectReservation reservation: ReservationModel)
}

class ReservationsListViewController: UIViewController {

    let reservationManager: AirspaceReservationManager

    var progressHud: ProgressHud?
    var alert: InfoAlert?
    var acceptanceAlert: AcceptanceAlert?
    var dateProvider: DateProvider = SimpleDateProvider()
    weak var delegate: ReservationsListViewControllerDelegate?

    private var reservationsModel: [ReservationModel] = []
    private var stationaryReservationsModel: [ReservationTableViewCellModel] = []
    private var droneAreaReservationsModel: [ReservationTableViewCellModel] = []
    private var planeRouteReservationsModel: [ReservationTableViewCellModel] = []

    init(reservationManager: AirspaceReservationManager) {
        self.reservationManager = reservationManager

        super.init(nibName: nil, bundle: nil)

        self.progressHud = DefaultProgressHud(visibleViewControllerProvider: self)
        self.alert = SimpleInfoAlert(presentingViewController: self)
        self.acceptanceAlert = SimpleAcceptanceAlert(presentingViewController: self)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("Init(coder) not implemented")
    }

    override func loadView() {
        let view: ReservationsListView = Bundle.loadView(fromNib: "ReservationsListView")
        view.delegate = self
        self.view = view

        arrangeTableView()
        tableView.delegate = self
        tableView.dataSource = self
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadLocalData()
        loadRemoteData()
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

extension ReservationsListViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .destructive, title: "CANCEL".localized) {
            [weak self] action, indexPath in
            if let data = self?.data(forIndexPath: indexPath) {
                self?.showAcceptanceAlert(reservationToCancel: data)
            }
        }
        return [delete]
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) {
            cell.backgroundColor = UIColor.dsDarkGrey.lighter(by: 0.05)
        }
        let reservation = self.data(forIndexPath: indexPath)
        delegate?.reservationsListViewController(self, didSelectReservation: reservation)
    }

    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) {
            cell.backgroundColor = .dsDarkGrey
        }
    }
}

extension ReservationsListViewController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model(forSection: section).count
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell: ReservationTableViewSectionHeader = tableView.dequeueReusableHeaderFooterView()
        let title = self.title(forSection: section)
        cell.set(title: title)
        return cell
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = self.model(forSection: indexPath.section)
        let cellModel = model[indexPath.row]

        let cell: ReservationTableViewCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.set(model: cellModel)
        cell.backgroundColor = .clear

        return cell
    }
}

extension ReservationsListViewController: ReservationsListViewDelegate {

    func reservationsListViewDidCancel(_ view: ReservationsListView) {
        delegate?.reservationsListViewControllerDidCancel(self)
    }
}

private extension ReservationsListViewController {

    func loadLocalData() {
        self.reservationsModel = reservationManager.getLocalReservations()
        fillTableViewModels()
        tableView.reloadData()
    }

    func loadRemoteData() {
        reservationManager.getReservations() {
            [weak self] models, error in
            self?.reservationsModel = models
        }
    }

    func fillTableViewModels() {
        droneAreaReservationsModel = []
        stationaryReservationsModel = []
        planeRouteReservationsModel = []
        for reservation in reservationsModel {
            guard reservation.isValid(date: dateProvider.currentDate()) else {
                continue
            }

            switch reservation.reservationType {
            case .drone:
                droneAreaReservationsModel.append(reservation.toCellModel())
            case .stationary:
                stationaryReservationsModel.append(reservation.toCellModel())
            case .plane:
                planeRouteReservationsModel.append(reservation.toCellModel())
            }
        }
    }

    func arrangeTableView() {
        initTableViewHeader()
        tableView.register(ReservationTableViewCell.self)
        tableView.register(ReservationTableViewSectionHeader.self)
        tableView.rowHeight = 98
        tableView.separatorColor = .dsViolet
        tableView.tableFooterView = UIView(frame: CGRect.zero)
    }

    func cancel(reservation: ReservationModel) {
        progressHud?.show()
        reservationManager.cancel(reservation: reservation) {
            [weak self] error in

            self?.progressHud?.hide()
            self?.loadLocalData()
        }
    }

    func showAcceptanceAlert(reservationToCancel: ReservationModel) {
        let title = "RESERVATIONS_LIST_SCREEN_ALERT_TITLE".localized
        let message = "RESERVATIONS_LIST_SCREEN_ALERT_MESSAGE".localized

        acceptanceAlert?.show(title: title, message: message) {
            [weak self] answer in

            if answer.toBool() == true {
                self?.cancel(reservation: reservationToCancel)
            }
        }
    }

    func model(forSection section: Int) -> [ReservationTableViewCellModel] {
        if section == 0 {
            return stationaryReservationsModel
        } else if section == 1 {
            return droneAreaReservationsModel
        } else if section == 2 {
            return planeRouteReservationsModel
        }
        return []
    }

    func title(forSection section: Int) -> String {
        if section == 0 {
            return "RESERVATIONS_LIST_SCREEN_STATIONARY_SECTION_TITLE".localized
        } else if section == 1 {
            return "RESERVATIONS_LIST_SCREEN_DRONE_AREA_SECTION_TITLE".localized
        } else {
            return "RESERVATIONS_LIST_SCREEN_PLANE_ROUTE_SECTION_TITLE".localized
        }
    }

    func data(forIndexPath indexPath: IndexPath) -> ReservationModel {
        let model = self.model(forSection: indexPath.section)
        let cellModel = model[indexPath.row]
        return cellModel.data
    }

    func initTableViewHeader() {
        let tableHeaderView: ReservationTableViewHeader = Bundle.loadView(fromNib: "ReservationTableViewHeader")
        tableView.tableHeaderView = tableHeaderView
    }

    var mainView: ReservationsListView {
        return self.view as! ReservationsListView
    }

    var tableView: UITableView {
        return mainView.tableView
    }
}
