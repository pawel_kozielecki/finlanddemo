import Foundation
import UIKit

class ReservationTableViewHeader: UIView {

    @IBOutlet weak var labelTitle: UILabel!
    
    override func willMove(toWindow newWindow: UIWindow?) {
        super.willMove(toWindow: newWindow)
        arrangeView()
    }
}

private extension ReservationTableViewHeader {

    func arrangeView() {
        labelTitle.text = "RESERVATIONS_LIST_SCREEN_TITLE".localized
        layoutIfNeeded()
    }
}
