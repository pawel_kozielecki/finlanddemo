import Foundation
import UIKit

struct ReservationTableViewCellModel {

    let dates: String
    let height: String
    let type: String
    let duration: String
    let data: ReservationModel
}

class ReservationTableViewCell: UITableViewCell, ReusableView, NibLoadableView {

    @IBOutlet weak var labelDates: UILabel!
    @IBOutlet weak var labelFlightType: UILabel!
    @IBOutlet weak var labelHeight: UILabel!
    @IBOutlet weak var labelDuration: UILabel!

    func set(model: ReservationTableViewCellModel) {
        labelDates.text = model.dates
        labelFlightType.text = model.type
        labelDuration.text = model.duration
        labelHeight.text = model.height
    }
}

extension ReservationModel {

    func toCellModel() -> ReservationTableViewCellModel {
        let dateFormatter = DateFormatter.makeSimpleDateAndTimeFormatter()
        let dates = "\("FROM".localized): \(dateFormatter.string(from: startDate))\n\("TO".localized): \(dateFormatter.string(from: endDate))"
        let durationInSeconds = endDate.timeIntervalSince1970 - startDate.timeIntervalSince1970
        let duration = "\(Int(durationInSeconds / 60)) min"
        let flightType = self.flightType == .commercial ? "COMMERCIAL".localized : "RECREATIONAL".localized
        return ReservationTableViewCellModel(
                dates: dates,
                height: "\(self.height)m",
                type: "\(flightType) (Confirmed)", //todo: make dynamic
                duration: duration,
                data: self
        )
    }
}
