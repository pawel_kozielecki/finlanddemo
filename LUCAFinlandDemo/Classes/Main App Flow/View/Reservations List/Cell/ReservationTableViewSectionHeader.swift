import Foundation
import UIKit

class ReservationTableViewSectionHeader: UITableViewHeaderFooterView, ReusableView, NibLoadableView {

    @IBOutlet weak var titleLabel: UILabel!

    func set(title: String) {
        titleLabel.text = title
    }
}
