import Foundation
import UIKit

protocol ReservationsListViewDelegate: class {
    func reservationsListViewDidCancel(_ view: ReservationsListView)
}

class ReservationsListView: UIView {

    weak var delegate: ReservationsListViewDelegate?

    @IBOutlet weak var buttonClose: UIButton!
    @IBOutlet weak var tableView: UITableView!

    @IBAction func didTapCloseButton(_ sender: Any) {
        delegate?.reservationsListViewDidCancel(self)
    }
}
