import Foundation
import UIKit

protocol AirspaceSelectionViewControllerDelegate: class {

    func airspaceSelectionViewControllerDidCancel(_ viewController: AirspaceSelectionViewController)
    func airspaceSelectionViewController(_ viewController: AirspaceSelectionViewController, didFinishWithCustomAirspace airspacePoints: [Coordinates])
    func airspaceSelectionViewController(_ viewController: AirspaceSelectionViewController, didFinishWithStandardAirspace airspaceLocation: AirspaceLocation)
}

class AirspaceSelectionViewController: UIViewController {

    let locationManager: LocationManager
    weak var delegate: AirspaceSelectionViewControllerDelegate?

    init(locationManager: LocationManager) {
        self.locationManager = locationManager
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("Init(coder) not implemented")
    }

    override func loadView() {
        let view: AirspaceSelectionView = Bundle.loadView(fromNib: "AirspaceSelectionView")
        view.delegate = self
        self.view = view
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        locationManager.delegate = self
        mainView.userLocation = locationManager.currentLocation
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        locationManager.delegate = nil
    }
}

extension AirspaceSelectionViewController: LocationProviderDelegate {

    func locationProvider(_ provider: LocationProvider, didUpdateLocation location: Coordinates, updatesCount: Int) {
        mainView.userLocation = location
    }
}

extension AirspaceSelectionViewController: AirspaceSelectionViewDelegate {

    func airspaceSelectionViewDidCancel(_ view: AirspaceSelectionView) {
        delegate?.airspaceSelectionViewControllerDidCancel(self)
    }

    func airspaceSelectionView(_ view: AirspaceSelectionView, didFinishWithCustomAirspace airspacePoints: [Coordinates]) {
        delegate?.airspaceSelectionViewController(self, didFinishWithCustomAirspace: airspacePoints)
    }

    func airspaceSelectionView(_ view: AirspaceSelectionView, didFinishWithStandardAirspace airspaceLocation: AirspaceLocation) {
        delegate?.airspaceSelectionViewController(self, didFinishWithStandardAirspace: airspaceLocation)
    }
}

private extension AirspaceSelectionViewController {

    var mainView: AirspaceSelectionView {
        return view as! AirspaceSelectionView
    }
}
