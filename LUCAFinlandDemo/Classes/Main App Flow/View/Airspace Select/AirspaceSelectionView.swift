import Foundation
import UIKit
import GoogleMaps

protocol AirspaceSelectionViewDelegate: class {
    func airspaceSelectionViewDidCancel(_ view: AirspaceSelectionView)
    func airspaceSelectionView(_ view: AirspaceSelectionView, didFinishWithCustomAirspace airspacePoints: [Coordinates])
    func airspaceSelectionView(_ view: AirspaceSelectionView, didFinishWithStandardAirspace airspaceLocation: AirspaceLocation)
}

class AirspaceSelectionView: UIView {

    @IBOutlet weak var buttonNext: DSRoundedButton!
    @IBOutlet weak var mapContainer: UIView!
    @IBOutlet weak var switchWorkMode: UISegmentedControl!

    @IBOutlet weak var panelCustomAirspace: UIView!
    @IBOutlet weak var buttonFinishShape: DSRoundedButton!
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var buttonRemoveMarker: DSRoundedButton!

    @IBOutlet weak var panelCircularAirspace: UIView!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var labelRadius: UILabel!

    weak var delegate: AirspaceSelectionViewDelegate?

    var selectedAirspacePlotter: AirspacePlotter?
    private (set) var selectedLocation: AirspaceLocation?

    var selectedRoutePlotter: PlaneRoutePlotter?
    private (set) var points = [Coordinates]()
    private (set) var isAirspaceFinished = false

    private var gmsMapView: GMSMapView!
    private var isMapInitialized = false
    private var workMode = AirspaceSelectionViewWorkMode.points

    var userLocation: Coordinates? {
        didSet {
            zoomToUserLocation()
        }
    }

    @IBAction func didTapCancelButton(_ sender: Any) {
        delegate?.airspaceSelectionViewDidCancel(self)
    }

    @IBAction func didTapNextButton(_ sender: Any) {
        if workMode == .points {
            delegate?.airspaceSelectionView(self, didFinishWithCustomAirspace: points)
        } else if let selectedLocation = selectedLocation {
            delegate?.airspaceSelectionView(self, didFinishWithStandardAirspace: selectedLocation)
        }
    }

    @IBAction func didTapFinishCustomAirspace(_ sender: Any) {
        if let point = points.first {
            isAirspaceFinished = true
            points.append(point)
            redrawRoute()
            manageButtonsAvailability()
        }
    }

    @IBAction func didTapRemoveMarker(_ sender: Any) {
        points.removeLast()
        isAirspaceFinished = false
        redrawRoute()
    }

    @IBAction func didChangeWorkMode(_ sender: UISegmentedControl) {
        isAirspaceFinished = false
        points = []
        selectedLocation = nil
        selectedRoutePlotter?.clear()
        selectedAirspacePlotter?.clear()
        if workMode == .points {
            workMode = .area
            panelCircularAirspace.isHidden = false
            panelCustomAirspace.isHidden = true
            labelDescription.text = "AIRSPACE_SELECT_SCREEN_CIRCULAR_AIRSPACE_DESCRIPTION".localized
        } else {
            workMode = .points
            panelCircularAirspace.isHidden = true
            panelCustomAirspace.isHidden = false
            labelDescription.text = "AIRSPACE_SELECT_SCREEN_CUSTOM_AIRSPACE_DESCRIPTION".localized
        }
        manageButtonsAvailability()
    }

    @IBAction func didSetRadius(_ sender: UISlider) {
        let radius = Int(slider.value)
        labelRadius.text = "\(radius) m"
        selectedLocation = selectedLocation?.updating(radius: radius)
        redrawAirspace()
    }

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        arrangeView()
        manageButtonsAvailability()
        selectedAirspacePlotter = DefaultAirspacePlotter(mapView: gmsMapView)
        selectedAirspacePlotter?.delegate = self
        selectedRoutePlotter = DefaultPlaneRoutePlotter(mapView: gmsMapView)
        selectedRoutePlotter?.delegate = self
    }
}

extension AirspaceSelectionView: AirspacePlotterDelegate {
    func airspacePlotter(_ plotter: AirspacePlotter, didRequestZoomingToBounds bounds: CoordinateBounds) {
        manageButtonsAvailability()
        gmsMapView.zoom(toBounds: bounds)
    }
}

extension AirspaceSelectionView: PlaneRoutePlotterDelegate {
    func planeRoutePlotter(_ plotter: PlaneRoutePlotter, didRequestZoomingToBounds bounds: CoordinateBounds) {
        manageButtonsAvailability()
        if isAirspaceFinished {
            gmsMapView.zoom(toBounds: bounds)
        }
    }
}

extension AirspaceSelectionView: GMSMapViewDelegate {

    public func mapView(_ mapView: GMSMapView, didLongPressAt coordinate: CLLocationCoordinate2D) {
        if workMode == .area {
            let radius = slider.value
            selectedLocation = AirspaceLocation(location: coordinate.toCoordinates(), radius: Int(radius))
            redrawAirspace()
            manageButtonsAvailability()
        } else {
            points.append(coordinate.toCoordinates())
            redrawRoute()
        }
    }
}

private extension AirspaceSelectionView {

    func manageButtonsAvailability() {
        if workMode == .points {
            buttonFinishShape.isEnabled = points.count > 2 && isAirspaceFinished == false
            buttonNext.isEnabled = isAirspaceFinished
            buttonRemoveMarker.isEnabled = !points.isEmpty
        } else {
            slider.isHidden = selectedLocation == nil
            labelRadius.isHidden = selectedLocation == nil
            buttonNext.isEnabled = selectedLocation != nil
        }
    }

    func redrawRoute() {
        let route = PlaneRoute(wayPoints: points, margin: 0)
        selectedRoutePlotter?.clear()
        selectedRoutePlotter?.plot(selectedRoute: route, color: .dsViolet)
    }

    func redrawAirspace() {
        if let location = selectedLocation {
            selectedAirspacePlotter?.clear()
            selectedAirspacePlotter?.plotSelectedAirspace(atSelectedLocation: location.location, radius: Double(location.radius), color: .dsViolet)
        }
    }

    func initGoogleMapView() {
        let location = userLocation ?? Coordinates.defaultLocation
        let camera = GMSCameraPosition.camera(withLatitude: location.latitude, longitude: location.longitude, zoom: ZoomLevel.high.rawValue)

        gmsMapView = GMSMapView.map(withFrame: mapContainer.bounds, camera: camera)
        mapContainer.addSubview(self.gmsMapView)
        gmsMapView.delegate = self
        gmsMapView.isMyLocationEnabled = true
        if let styleURL = Bundle.main.url(forResource: "mapDarkStyle", withExtension: "json") {
            gmsMapView.mapStyle = try? GMSMapStyle(contentsOfFileURL: styleURL)
        }

        gmsMapView.snp.prepareConstraints {
            make in
            make.margins.equalToSuperview()
        }

        isMapInitialized = true
    }

    func zoomToUserLocation() {
        if let location = userLocation, isMapInitialized {
            gmsMapView.zoom(toLocation: location)
        }
    }

    func arrangeView() {
        initGoogleMapView()

        panelCircularAirspace.isHidden = true

        buttonNext.isEnabled = false
        buttonRemoveMarker.setTitle("AIRSPACE_SELECT_SCREEN_BUTTON_REMOVE_MARKER".localized, for: .normal)
        buttonFinishShape.setTitle("AIRSPACE_SELECT_SCREEN_BUTTON_FINISH_SHAPE".localized, for: .normal)
        switchWorkMode.setTitle("AIRSPACE_SELECT_SCREEN_WORK_MODE_POINTS".localized, forSegmentAt: 0)
        switchWorkMode.setTitle("AIRSPACE_SELECT_SCREEN_WORK_MODE_AIRSPACE".localized, forSegmentAt: 1)
        labelDescription.text = "AIRSPACE_SELECT_SCREEN_CUSTOM_AIRSPACE_DESCRIPTION".localized

        layoutIfNeeded()
    }
}

enum AirspaceSelectionViewWorkMode {
    case area
    case points
}
