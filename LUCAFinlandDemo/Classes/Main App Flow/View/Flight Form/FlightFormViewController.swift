import Foundation
import UIKit

protocol FlightFormViewControllerDelegate: class {
    func flightFormViewControllerDidCancel(_ viewController: FlightFormViewController)
    func flightFormViewController(_ viewController: FlightFormViewController, didSelectHeight height: Int)
}

class FlightFormViewController: UIViewController {

    weak var delegate: FlightFormViewControllerDelegate?

    private var alert: InfoAlert?

    init() {
        super.init(nibName: nil, bundle: nil)
        self.alert = SimpleInfoAlert(presentingViewController: self)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("Init(coder) not implemented")
    }

    override func loadView() {
        let view: FlightFormView = Bundle.loadView(fromNib: "FlightFormView")
        view.delegate = self
        self.view = view
    }
}

extension FlightFormViewController: FlightFormViewDelegate {
    func flightFormViewDidBack(_ view: FlightFormView) {
        delegate?.flightFormViewControllerDidCancel(self)
    }

    func flightFormView(_ view: FlightFormView, didSelectHeight height: Int) {
        delegate?.flightFormViewController(self, didSelectHeight: height)
    }

    func flightFormView(_ view: FlightFormView, didShowAlert message: String) {
        alert?.show(title: "FLIGHT_PLAN_FORM_ALERT_TITLE".localized, message: message, completion: nil)
    }
}
