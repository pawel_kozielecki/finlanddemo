import Foundation
import UIKit

//https://www.ais.fi/en/pre-flight-information-service-and-flight-plans/fpl-form

protocol FlightFormViewDelegate: class {
    func flightFormViewDidBack(_ view: FlightFormView)
    func flightFormView(_ view: FlightFormView, didSelectHeight height: Int)
    func flightFormView(_ view: FlightFormView, didShowAlert message: String)
}

class FlightFormView: UIView {

    weak var delegate: FlightFormViewDelegate?

    override func willMove(toWindow newWindow: UIWindow?) {
        super.willMove(toWindow: newWindow)
        arrangeView()
    }

    @IBOutlet weak var containerView: UIView!

    @IBOutlet weak var labelTitle: UILabel!

    @IBOutlet weak var addresseeTextField: UITextField!
    @IBOutlet weak var aircraftIdentificationTextField: UITextField!
    @IBOutlet weak var flightRulesDropDown: FormDropDown!
    @IBOutlet weak var typeFlightDropDown: FormDropDown!
    @IBOutlet weak var numberTextField: UITextField!
    @IBOutlet weak var typeOfAircraftTextField: UITextField!
    @IBOutlet weak var wakeTurbulenceDropDown: FormDropDown!
    @IBOutlet weak var equipment1TextField: UITextField!
    @IBOutlet weak var equipment2TextField: UITextField!
    @IBOutlet weak var departureAerodromeTextField: UITextField!
    @IBOutlet weak var timeTextField: UITextField!
    @IBOutlet weak var cruisingSpeedDropDown: FormDropDown!
    @IBOutlet weak var cruisingSpeedTextField: UITextField!
    @IBOutlet weak var levelDropDown: FormDropDown!
    @IBOutlet weak var levelTextField: UITextField!
    @IBOutlet weak var routeTextView: UITextView!
    @IBOutlet weak var destinationAerodromeTextField: UITextField!
    @IBOutlet weak var totalEetTextField: UITextField!
    @IBOutlet weak var alternateAerodromeTextField: UITextField!
    @IBOutlet weak var secondAlternateAerodromeTextField: UITextField!
    @IBOutlet weak var otherInformationTextView: UITextView!
    @IBOutlet weak var enduranceTextField: UITextField!
    @IBOutlet weak var personsOnBoardTextField: UITextField!
    @IBOutlet weak var uhfCheckBox: FormCheckBox!
    @IBOutlet weak var vhfCheckBox: FormCheckBox!
    @IBOutlet weak var elbaCheckBox: FormCheckBox!
    @IBOutlet weak var equipmentPolarCheckBox: FormCheckBox!
    @IBOutlet weak var equipmentDesertCheckBox: FormCheckBox!
    @IBOutlet weak var equipmentMaritimeCheckBox: FormCheckBox!
    @IBOutlet weak var equipmentJungleCheckBox: FormCheckBox!
    @IBOutlet weak var equipmentLightCheckBox: FormCheckBox!
    @IBOutlet weak var equipmentFluoresceinCheckBox: FormCheckBox!
    @IBOutlet weak var equipmentUHFCheckBox: FormCheckBox!
    @IBOutlet weak var equipmentVHFCheckBox: FormCheckBox!
    @IBOutlet weak var dinghiesCheckBox: FormCheckBox!
    @IBOutlet weak var dinghiesNumberTextField: UITextField!
    @IBOutlet weak var dinghiesCapacityTextField: UITextField!
    @IBOutlet weak var coverCheckBox: FormCheckBox!
    @IBOutlet weak var coverColourTextField: UITextField!
    @IBOutlet weak var aircraftColourAndMarkingTextField: UITextField!
    @IBOutlet weak var remarksTextField: UITextField!
    @IBOutlet weak var pilotsInCommandTextField: UITextField!
    @IBOutlet weak var filledByTextField: UITextField!
    @IBOutlet weak var telephoneTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var faxTextField: UITextField!
    @IBOutlet weak var sendButton: DSRoundedButton!
    
    @IBAction func didTapBackButton(_ sender: Any) {
        delegate?.flightFormViewDidBack(self)
    }

    @IBAction func didTapSendButton(_ sender: Any) {
        if validate() {
            if let height = levelTextField.text, let fakeHeight = Int(height) {
                delegate?.flightFormView(self, didSelectHeight: fakeHeight)
            } else {
                let fakeHeight = 50
                delegate?.flightFormView(self, didSelectHeight: fakeHeight)
            }
        }
    }

    @IBAction func didTapFillButton(_ sender: Any) {
        fillWithFakeData()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        arrangeView()
    }
}

private extension FlightFormView {

    func arrangeView() {
        arrangeLabels()
        arrangeTextFields()
        arrangeDropDowns()
        arrangeTextViews()
        arrangeCheckBoxes()
        
        containerView.backgroundColor = .clear
        labelTitle.textColor = .white

        flightRulesDropDown.optionArray = ["IFR", "VFR", "IFR than VFR", "VFR than IFR"]
        typeFlightDropDown.optionArray = ["Scheduled", "Non-Scheduled", "General", "Military", "Other"]
        wakeTurbulenceDropDown.optionArray = ["J", "H", "M", "L"]
        cruisingSpeedDropDown.optionArray = ["N", "M", "K"]
        levelDropDown.optionArray = ["F", "A", "S", "M", "VFR"]

//        addresseeTextField.addTarget(self, action: #selector(FlightFormView.editingDidEnd(_:)), for: UIControl.Event.editingDidEnd)

        layoutIfNeeded()
    }

    func arrangeLabels() {
        for label in containerView.subviews.filter({ $0 is UILabel }) {
            (label as! UILabel).textColor = .white
        }
    }

    func arrangeTextFields() {
        for textField in containerView.subviews.filter({ $0 is UITextField }) {
            (textField as! UITextField).borderColor = .dsBlue
            (textField as! UITextField).backgroundColor = .dsBlue
            (textField as! UITextField).textColor = .white
            (textField as! UITextField).tintColor = .white
        }
    }
    
    func arrangeTextViews() {
        for textView in containerView.subviews.filter({ $0 is UITextView }) {
            (textView as! UITextView).borderColor = .dsBlue
            (textView as! UITextView).backgroundColor = .dsBlue
            (textView as! UITextView).textColor = .white
            (textView as! UITextView).tintColor = .white
        }
    }
    
    func arrangeDropDowns() {
        for dropDown in containerView.subviews.filter({ $0 is FormDropDown }) {
            (dropDown as! FormDropDown).borderColor = .dsBlue
            (dropDown as! FormDropDown).arrowColor = .dsLightBlue
            (dropDown as! FormDropDown).selectedRowColor = .dsLightBlue
            (dropDown as! FormDropDown).selectedTextColor = .white
        }
    }

    func arrangeCheckBoxes() {
        for checkbox in containerView.subviews.filter({ $0 is FormCheckBox }) {
            (checkbox as! FormCheckBox).borderColor = .dsLightBlue
            (checkbox as! FormCheckBox).backgroundColor = .clear
            (checkbox as! FormCheckBox).selectedBackground = .dsLightBlue
        }
    }

    func validate() -> Bool {
        for subview in containerView.subviews {
            if let formTextField = subview as? FormTextField, formTextField.isObligatoryAndEmpty {
                formTextField.becomeFirstResponder()
                
                let message = formTextField.message?.localized ?? "Field"
                delegate?.flightFormView(self, didShowAlert: "\(message) cannot be empty!")
                
                return false
            }
        }

        return true
    }

    @objc func editingDidEnd(_ textField: UITextField) {
        fillWithFakeData()
        sendButton.becomeFirstResponder()
    }

    func fillWithFakeData() {
        addresseeTextField.text = "Helsinki, Finland"
        aircraftIdentificationTextField.text = "AC-123"
        flightRulesDropDown.text = "VFR"
        flightRulesDropDown.selectedIndex = 1
        typeFlightDropDown.text = "General"
        typeFlightDropDown.selectedIndex = 2
        numberTextField.text = "123-456-789"
        typeOfAircraftTextField.text = "airplane"
        wakeTurbulenceDropDown.text = "H"
        wakeTurbulenceDropDown.selectedIndex = 1
        equipment1TextField.text = "altimeter"
        equipment2TextField.text = "12-34/56"
        departureAerodromeTextField.text = "HEL"
        timeTextField.text = "12:53:00"
        cruisingSpeedDropDown.text = "M"
        cruisingSpeedDropDown.selectedIndex = 1
        cruisingSpeedTextField.text = "90"
        levelDropDown.text = "VFR"
        levelDropDown.selectedIndex = 4
        levelTextField.text = "123"
        routeTextView.text = "HEL-TKU-POR"
        destinationAerodromeTextField.text = "POR"
        totalEetTextField.text = "123"
        alternateAerodromeTextField.text = "TMP"
        secondAlternateAerodromeTextField.text = "VAA"
        otherInformationTextView.text = "no other information"
        enduranceTextField.text = "3"
        personsOnBoardTextField.text = "2"
//        uhfCheckBox: FormCheckBox!
        vhfCheckBox.isSelected = true
//        elbaCheckBox: FormCheckBox!
//        equipmentPolarCheckBox: FormCheckBox!
        equipmentDesertCheckBox.isSelected = true
//        equipmentMaritimeCheckBox: FormCheckBox!
//        equipmentJungleCheckBox: FormCheckBox!
//        equipmentLightCheckBox: FormCheckBox!
//        equipmentFluoresceinCheckBox: FormCheckBox!
//        equipmentUHFCheckBox: FormCheckBox!
        equipmentVHFCheckBox.isSelected = true
        dinghiesCheckBox.isSelected = true
        dinghiesNumberTextField.text = "A234"
        dinghiesCapacityTextField.text = "567"
//        coverCheckBox: FormCheckBox!
//        coverColourTextField: UITextField!
        aircraftColourAndMarkingTextField.text = "black-orange"
        remarksTextField.text = "no remarks"
        pilotsInCommandTextField.text = "Kris"
        filledByTextField.text = "Kris"
        telephoneTextField.text = "+358 600-123-456"
        emailTextField.text = "abc@gmail.com"
        faxTextField.text = "+358 600-123-457"
    }
}
