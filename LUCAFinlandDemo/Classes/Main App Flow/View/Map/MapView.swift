import Foundation
import UIKit
import GoogleMaps
import SnapKit

protocol MapViewDelegate: class {
    func mapViewDidRequestLogout(_ view: MapView)
    func mapViewDidRequestStartingReservation(_ view: MapView)
    func mapViewDidRequestReservationsList(_ view: MapView)
}

class MapView: UIView {

    @IBOutlet weak var mapContainer: UIView!
    @IBOutlet weak var buttonStartReservation: UIButton!
    @IBOutlet weak var buttonReservations: UIButton!
    
    var airspacesPlotter: AirspacesPlotter?
    var planeRoutesPlotter: PlaneRoutesPlotter?

    var geospatialPlotter: GeospatialPlotter?

    weak var delegate: MapViewDelegate?

    private (set) var gmsMapView: GMSMapView!
    private var isMapInitialized = false

    var userLocation: Coordinates? {
        didSet {
            zoomToUserLocation()
        }
    }

    var currentReservations: [ReservationModel] = [] {
        didSet {
            plotReservationsIfNeeded()
            manageButtonsVisibility()
        }
    }

    var geoAirspaces: [GeoAirspace]?
    var geoHeliports: [GeoHeliport]?


    override func draw(_ rect: CGRect) {
        super.draw(rect)
        arrangeView()

        airspacesPlotter = DefaultAirspacesPlotter(mapView: gmsMapView)
        planeRoutesPlotter = DefaultPlaneRoutesPlotter(mapView: gmsMapView)
        geospatialPlotter = DefaultGeospatialPlotter(mapView: gmsMapView)

        plotReservationsIfNeeded()
        plotGeoAirspaces()
        plotGeoHeliports()
    }

    @IBAction func didTapLogoutButton(_ sender: UIButton) {
        delegate?.mapViewDidRequestLogout(self)
    }

    @IBAction func didTapShowReservations(_ sender: UIButton) {
        delegate?.mapViewDidRequestReservationsList(self)
    }

    @IBAction func didTapStartReservationButton(_ sender: UIButton) {
        delegate?.mapViewDidRequestStartingReservation(self)
    }

    func show(reservation: ReservationModel) {
        if let location = reservation.airspaceLocation {
            airspacesPlotter?.zoom(toLocation: location)
        } else if let points = reservation.airspacePoints {
            planeRoutesPlotter?.zoom(toRoute: PlaneRoute(wayPoints: points, margin: 0))
        } else if let route = reservation.planeRoute {
            planeRoutesPlotter?.zoom(toRoute: route)
        }
    }
}

extension MapView: GMSMapViewDelegate {

    public func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        plotReservationsIfNeeded()
        manageButtonsVisibility()
    }
}

private extension MapView {
    func arrangeView() {
        initGoogleMapView()
        manageButtonsVisibility()

        layoutIfNeeded()
    }

    func plotReservationsIfNeeded() {
        planeRoutesPlotter?.clear()
        airspacesPlotter?.clear()

        var airspaces = [AirspaceLocation]()
        var routes = [PlaneRoute]()
        for reservation in currentReservations {
            if let location = reservation.airspaceLocation {
                airspaces.append(location)
            } else if let points = reservation.airspacePoints {
                routes.append(PlaneRoute(wayPoints: points, margin: 0))
            } else if let route = reservation.planeRoute {
                routes.append(route)
            }
        }
        airspacesPlotter?.plot(airspaces: airspaces, color: .dsViolet)
        planeRoutesPlotter?.plot(routes: routes, color: .dsViolet)
    }

    func initGoogleMapView() {
        let location = userLocation ?? Coordinates.defaultLocation
        let camera = GMSCameraPosition.camera(withLatitude: location.latitude, longitude: location.longitude, zoom: ZoomLevel.airspaces.rawValue)

        gmsMapView = GMSMapView.map(withFrame: mapContainer.bounds, camera: camera)
        mapContainer.addSubview(self.gmsMapView)
        gmsMapView.delegate = self
        gmsMapView.isMyLocationEnabled = true
        if let styleURL = Bundle.main.url(forResource: "mapDarkStyle", withExtension: "json") {
            gmsMapView.mapStyle = try? GMSMapStyle(contentsOfFileURL: styleURL)
        }

        gmsMapView.snp.prepareConstraints {
            make in
            make.margins.equalToSuperview()
        }

        isMapInitialized = true
    }

    func zoomToUserLocation() {
        if let location = userLocation, isMapInitialized {
            let camera = GMSCameraPosition.camera(withLatitude: location.latitude, longitude: location.longitude, zoom: ZoomLevel.high.rawValue)
            gmsMapView.camera = camera
        }
    }

    func manageButtonsVisibility() {
        buttonReservations.isHidden = currentReservations.isEmpty
    }

    func plotGeoAirspaces() {
        if let geoAirspaces = geoAirspaces {
            geospatialPlotter?.plot(airspaces: geoAirspaces, color: .dsViolet)
        }
    }

    func plotGeoHeliports() {
        if let geoHeliports = geoHeliports {
            geospatialPlotter?.plot(heliports: geoHeliports, color: .red)
        }
    }
}

enum ZoomLevel: Float {
    case countries = 3.5
    case low = 5.5
    case airspaces = 7
    case medium = 11
    case high = 13
    case veryHigh = 15
    case buildings = 20
    case geoculturalDataMax = 23
}
