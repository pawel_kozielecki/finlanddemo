import Foundation
import UIKit

protocol MapViewControllerDelegate: class {
    func mapViewControllerDidFinish(_ viewController: MapViewController)
    func mapViewControllerDidRequestReservationsList(_ viewController: MapViewController)
    func mapViewController(_ viewController: MapViewController, didRequestStartingReservation type: ReservationType)
}

class MapViewController: UIViewController {

    let locationManager: LocationManager
    let loginManager: LoginManager
    let reservationManager: AirspaceReservationManager

    let geospatialManager: GeospatialManager

    weak var delegate: MapViewControllerDelegate?
    var alert: AcceptanceAlert?
    var actionSheet: ActionSheet?
    var dateProvider: DateProvider = SimpleDateProvider()

    private var selectedReservation: ReservationModel?

    init(locationManager: LocationManager, loginManager: LoginManager, reservationManager: AirspaceReservationManager) {
        self.locationManager = locationManager
        self.loginManager = loginManager
        self.reservationManager = reservationManager

        self.geospatialManager = DefaultGeospatialManager()

        super.init(nibName: nil, bundle: nil)

        alert = SimpleAcceptanceAlert(presentingViewController: self)
        actionSheet = DefaultActionSheet(presentingViewController: self)
        actionSheet?.delegate = self
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("Init(coder) not implemented")
    }

    override func loadView() {
        let view: MapView = Bundle.loadView(fromNib: "MapView")
        view.delegate = self
        self.view = view
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        mainView.currentReservations = reservationManager.getLocalReservations().onlyValidReservations(currentDate: dateProvider.currentDate())
        locationManager.requestForAuthorization()
//        locationManager.startUpdating()
        
        initGeoAirspaces()
        initGeoHeliports()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        locationManager.delegate = self
        mainView.userLocation = locationManager.currentLocation

        reservationManager.getReservations {
            [weak self] models, error in
            self?.mainView.currentReservations = models.onlyValidReservations(currentDate: self?.dateProvider.currentDate() ?? Date())
        }

        if let reservation = selectedReservation {
            mainView.show(reservation: reservation)
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        locationManager.delegate = nil
        selectedReservation = nil
    }

    func show(reservation: ReservationModel) {
        self.selectedReservation = reservation
    }
}

extension MapViewController: LocationProviderDelegate {

    func locationProvider(_ provider: LocationProvider, didUpdateLocation location: Coordinates, updatesCount: Int) {
        mainView.userLocation = location
    }
}

extension MapViewController: MapViewDelegate {

    func mapViewDidRequestLogout(_ view: MapView) {
        showLogoutAlert()
    }

    func mapViewDidRequestStartingReservation(_ view: MapView) {
        showReservationTypeActionSheet()
    }

    func mapViewDidRequestReservationsList(_ view: MapView) {
        delegate?.mapViewControllerDidRequestReservationsList(self)
    }
}

extension MapViewController: ActionSheetDelegate {

    func actionPicker(_ actionPicker: ActionSheet, didSelectAction action: ActionSheetOption) {
        guard let type = action.value as? ReservationType else {
            return
        }
        delegate?.mapViewController(self, didRequestStartingReservation: type)
    }
}

private extension MapViewController {

    var mainView: MapView {
        return view as! MapView
    }

    func showReservationTypeActionSheet() {
        let title = "RESERVATION_TYPE_TITLE".localized
        let stationaryAction = ActionSheetOption(title: "RESERVATION_TYPE_STATIONARY".localized, value: ReservationType.stationary)
        let droneAction = ActionSheetOption(title: "RESERVATION_TYPE_DRONE".localized, value: ReservationType.drone)
        let planeRouteAction = ActionSheetOption(title: "RESERVATION_TYPE_PLANE".localized, value: ReservationType.plane)
        actionSheet?.show(title: title, options: [stationaryAction, droneAction, planeRouteAction])
    }

    func showLogoutAlert() {
        let title = "LOGOUT_ALERT_TITLE".localized
        let message = "LOGOUT_ALERT_MESSAGE".localized
        alert?.show(title: title, message: message) {
            [weak self] answer in
            guard let strongSelf = self else {
                return
            }

            if answer == .yes {
                strongSelf.performLogout()
            }
        }
    }

    func performLogout() {
        loginManager.logOut(manual: true) {
            [weak self] result in
            guard let strongSelf = self else {
                return
            }

            strongSelf.delegate?.mapViewControllerDidFinish(strongSelf)
        }
    }

    func showReservation() {
        if let reservation = selectedReservation {
            mainView.show(reservation: reservation)
        }
    }

    func initGeoAirspaces() {
        if let geoAirspaces = geospatialManager.getAirspaces(upperLimit: 8000) {
            mainView.geoAirspaces = geoAirspaces
        }

    }

    func initGeoHeliports() {
        if let geoHeliports = geospatialManager.getHeliports() {
            mainView.geoHeliports = geoHeliports
        }
    }
}
