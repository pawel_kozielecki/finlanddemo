import Foundation
import UIKit

protocol HeightSelectionViewDelegate: class {
    func heightSelectionViewDidCancel(_ view: HeightSelectionView)
    func heightSelectionView(_ view: HeightSelectionView, didSelectHeight height: Int)
}

class HeightSelectionView: UIView {

    weak var delegate: HeightSelectionViewDelegate?

    override func willMove(toWindow newWindow: UIWindow?) {
        super.willMove(toWindow: newWindow)
        arrangeView()
    }

    @IBOutlet weak var buttonNext: DSRoundedButton!
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var labelHeight: UILabel!
    @IBOutlet weak var labelTitle: UILabel!


    @IBAction func didTapCancelButton(_ sender: Any) {
        delegate?.heightSelectionViewDidCancel(self)
    }

    @IBAction func didTapNextButton(_ sender: Any) {
        delegate?.heightSelectionView(self, didSelectHeight: currentHeight)
    }

    @IBAction func didSetHeight(_ sender: UISlider) {
        labelHeight.text = "\(currentHeight) m"
    }

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        arrangeView()
    }
}

private extension HeightSelectionView {

    func arrangeView() {
        labelTitle.text = "HEIGHT_SELECT_SCREEN_TITLE".localized
        labelDescription.text = "HEIGHT_SELECT_SCREEN_DESCRIPTION".localized

        layoutIfNeeded()
    }

    var currentHeight: Int {
        return Int(slider.value)
    }
}
