import Foundation
import UIKit

protocol HeightSelectionViewControllerDelegate: class {
    func heightSelectionViewControllerDidCancel(_ viewController: HeightSelectionViewController)
    func heightSelectionViewController(_ viewController: HeightSelectionViewController, didSelectHeight height: Int)
}

class HeightSelectionViewController: UIViewController {

    weak var delegate: HeightSelectionViewControllerDelegate?

    init() {
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("Init(coder) not implemented")
    }

    override func loadView() {
        let view: HeightSelectionView = Bundle.loadView(fromNib: "HeightSelectionView")
        view.delegate = self
        self.view = view
    }
}

extension HeightSelectionViewController: HeightSelectionViewDelegate {
    func heightSelectionViewDidCancel(_ view: HeightSelectionView) {
        delegate?.heightSelectionViewControllerDidCancel(self)
    }

    func heightSelectionView(_ view: HeightSelectionView, didSelectHeight height: Int) {
        delegate?.heightSelectionViewController(self, didSelectHeight: height)
    }
}
