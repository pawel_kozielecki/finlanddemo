import Foundation
import UIKit

protocol WeatherViewControllerDelegate: class {
    func weatherViewControllerDidBack(_ viewController: WeatherViewController)
    func weatherViewControllerDidNext(_ viewController: WeatherViewController)
}

class WeatherViewController: UIViewController {
    
    weak var delegate: WeatherViewControllerDelegate?
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("Init(coder) not implemented")
    }
    
    override func loadView() {
        let view: WeatherView = Bundle.loadView(fromNib: "WeatherView")
        view.delegate = self
        self.view = view
    }
}

extension WeatherViewController: WeatherViewDelegate {
    func weatherViewDidBack(_ view: WeatherView) {
        delegate?.weatherViewControllerDidBack(self)
    }
    func weatherViewDidNext(_ view: WeatherView) {
      delegate?.weatherViewControllerDidNext(self)
    }
}

