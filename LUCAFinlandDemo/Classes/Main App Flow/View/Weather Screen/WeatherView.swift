import Foundation
import UIKit

protocol WeatherViewDelegate: class {
    func weatherViewDidBack(_ view: WeatherView)
    func weatherViewDidNext(_ view: WeatherView)
}

class WeatherView: UIView {
    weak var delegate: WeatherViewDelegate?

    @IBAction func didTapBackButton(_ sender: Any) {
        delegate?.weatherViewDidBack(self)
    }
    
    @IBAction func didTapGotoNextButton(_ sender: Any) {
        delegate?.weatherViewDidNext(self)
    }

    override func draw(_ rect: CGRect) {
        arrangeView()
    }
}

private extension WeatherView {
    func arrangeView() {
    }
}
