import Foundation
import UIKit

protocol SelectRoleViewDelegate: class {
    func selectRoleViewDidCancel(_ view: SelectRoleView)
    func selectRoleView(_ view: SelectRoleView, didSelectFlightType flightType: FlightType)
}

class SelectRoleView: UIView {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var buttonNext: DSRoundedButton!
    @IBOutlet weak var buttonSwitch: UISwitch!
    @IBOutlet weak var labelFlightType: UILabel!

    weak var delegate: SelectRoleViewDelegate?

    private var selectedFlightType: FlightType = .recreational

    func setup(reservationType: ReservationType) {
        arrangeView(type: reservationType)
    }

    @IBAction func didTapGoBackButton(_ sender: UIButton) {
        delegate?.selectRoleViewDidCancel(self)
    }

    @IBAction func didTapNextButton(_ sender: UIButton) {
        delegate?.selectRoleView(self, didSelectFlightType: selectedFlightType)
    }

    @IBAction func didChangeFlightType(_ sender: Any) {
        selectedFlightType = buttonSwitch.isOn ? .commercial : .recreational
        manageFlightTypeLabel()
    }
}

private extension SelectRoleView {

    func arrangeView(type: ReservationType) {
        labelTitle.text = "ROLE_SELECT_SCREEN_TITLE".localized

        switch type {
        case .stationary:
            labelDescription.text = "ROLE_SELECT_SCREEN_MESSAGE_STATIONARY".localized
        case .drone:
            labelDescription.text = "ROLE_SELECT_SCREEN_MESSAGE_DRONE".localized
        case .plane:
            labelDescription.text = "ROLE_SELECT_SCREEN_MESSAGE_PLANE".localized
        }
        manageFlightTypeLabel()
        layoutIfNeeded()
    }

    func manageFlightTypeLabel() {
        labelFlightType.text = selectedFlightType == .commercial ? "ROLE_SELECT_SCREEN_COMMERCIAL".localized : "ROLE_SELECT_SCREEN_RECREATIONAL".localized
    }
}
