import Foundation
import UIKit

protocol SelectRoleViewControllerDelegate: class {
    func selectRoleViewControllerDidCancel(_ viewController: SelectRoleViewController)
    func selectRoleViewController(_ viewController: SelectRoleViewController, didSelectFlightType flightType: FlightType)
}

class SelectRoleViewController: UIViewController {

    let type: ReservationType
    weak var delegate: SelectRoleViewControllerDelegate?

    init(reservationType: ReservationType) {
        self.type = reservationType
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("Init(coder) not implemented")
    }

    override func loadView() {
        let view: SelectRoleView = Bundle.loadView(fromNib: "SelectRoleView")
        view.delegate = self
        self.view = view
        view.setup(reservationType: type)
    }
}

extension SelectRoleViewController: SelectRoleViewDelegate {
    func selectRoleViewDidCancel(_ view: SelectRoleView) {
        delegate?.selectRoleViewControllerDidCancel(self)
    }

    func selectRoleView(_ view: SelectRoleView, didSelectFlightType flightType: FlightType) {
        delegate?.selectRoleViewController(self, didSelectFlightType: flightType)
    }
}
