import Foundation

protocol AirspaceReservationStorage: class {

    func add(reservation: ReservationModel)
    func store(reservations: [ReservationModel])
    func invalidate()
    func getReservations() -> [ReservationModel]
    func remove(reservation: ReservationModel)
}

class DefaultAirspaceReservationStorage: AirspaceReservationStorage {

    let key = "reservations"
    var storage: LocalStorage = UserDefaultsStorage()

    func add(reservation: ReservationModel) {
        var reservations = getReservations()
        if !reservations.contains(reservation) {
            reservations.append(reservation)
            store(reservations: reservations)
        }
    }

    func store(reservations: [ReservationModel]) {
        let encoder = JSONEncoder()
        if let reservationsData = try? encoder.encode(reservations) {
            storage.write(key: key, value: reservationsData)
        }
    }

    func invalidate() {
        storage.invalidate(key: key)
    }

    func getReservations() -> [ReservationModel] {
        guard let reservationData = storage.readData(key: key) else {
            return []
        }

        let decoder = JSONDecoder()
        let reservations = try? decoder.decode([ReservationModel].self, from: reservationData)
        return reservations ?? []
    }

    func remove(reservation: ReservationModel) {
        var reservations = getReservations()
        if let index = reservations.firstIndex(of: reservation) {
            reservations.remove(at: index)
            store(reservations: reservations)
        }
    }
}
