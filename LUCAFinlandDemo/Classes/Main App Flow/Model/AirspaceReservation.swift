import Foundation

protocol AirspaceReservation {

}

struct CircularAirspaceReservation {
    let isCommercial: Bool
    let startDate: Date
    let endDate: Date
    let location: Coordinates
    let radius: Int
}

struct CustomSelectionAirspaceReservation {
    let isCommercial: Bool
    let startDate: Date
    let endDate: Date
    let points: [Coordinates]
}
