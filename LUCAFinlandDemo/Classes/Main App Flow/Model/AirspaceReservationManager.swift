import Foundation

protocol AirspaceReservationManager {
    func set(reservationType: ReservationType)
    func set(flightType: FlightType)
    func set(startDate: Date, endDate: Date)
    func set(airspaceLocation: AirspaceLocation)
    func set(customAirspaceOfPoints points: [Coordinates])
    func set(planeRoute: PlaneRoute)
    func set(height: Int)

    func makeReservation(completion: ((ReservationModel?, NSError?) -> Void)?)
    func getReservations(completion: (([ReservationModel], NSError?) -> Void)?)
    func getLocalReservations() -> [ReservationModel]
    func cancel(reservation: ReservationModel, completion: ((NSError?) -> Void)?)
    func clear()

    var reservationType: ReservationType? { get }
    var flightType: FlightType? { get }
    var startDate: Date? { get }
    var endDate: Date? { get }
    var airspaceLocation: AirspaceLocation? { get }
    var airspacePoints: [Coordinates]? { get }
    var height: Int? { get }
}

class DefaultAirspaceReservationManager: AirspaceReservationManager {

    let networkController: AirspaceReservationNetworkController
    let storage: AirspaceReservationStorage

    private (set) var reservationType: ReservationType?
    private (set) var flightType: FlightType?
    private (set) var startDate: Date?
    private (set) var endDate: Date?
    private (set) var airspaceLocation: AirspaceLocation?
    private (set) var airspacePoints: [Coordinates]?
    private (set) var planeRoute: PlaneRoute?
    private (set) var height: Int?

    init(networkController: AirspaceReservationNetworkController, storage: AirspaceReservationStorage) {
        self.networkController = networkController
        self.storage = storage
    }

    func getReservations(completion: (([ReservationModel], NSError?) -> Void)?) {
        networkController.getReservations() {
            [weak self] reservations, error in

            if let reservations = reservations {
                self?.storage.store(reservations: reservations)
                completion?(reservations, nil)
            } else {
                completion?([], error)
            }
        }
    }

    func getLocalReservations() -> [ReservationModel] {
        return storage.getReservations()
    }

    func makeReservation(completion: ((ReservationModel?, NSError?) -> Void)?) {
        guard let reservationType = reservationType else {
            completion?(nil, NSError.makeUnknownError())
            return
        }

        switch reservationType {
        case .plane:
            let details = PlaneRouteDetails(route: planeRoute!, startDate: startDate!, endDate: endDate!, height: height!, flightType: flightType!)
            networkController.reserve(planeRoute: details) {
                [weak self] reservation, error in
                self?.saveIfNeeded(reservation: reservation)
                completion?(reservation, error)
            }
        default:
            if let location = airspaceLocation {
                let details = CircularAirspaceDetails(location: location, startDate: startDate!, endDate: endDate!, height: height!, flightType: flightType!, reservationType: reservationType)
                networkController.reserve(circularAirspace: details) {
                    [weak self] reservation, error in
                    self?.saveIfNeeded(reservation: reservation)
                    completion?(reservation, error)
                }
            } else {
                let details = CustomAirspaceDetails(points: airspacePoints!, startDate: startDate!, endDate: endDate!, height: height!, flightType: flightType!, reservationType: reservationType)
                networkController.reserve(customAirspace: details) {
                    [weak self] reservation, error in
                    self?.saveIfNeeded(reservation: reservation)
                    completion?(reservation, error)
                }
            }
        }
    }

    func cancel(reservation: ReservationModel, completion: ((NSError?) -> Void)?) {
        networkController.cancel(reservation: reservation) {
            [weak self] error in
            if error == nil {
                self?.storage.remove(reservation: reservation)
            }
            completion?(error)
        }
    }

    func clear() {
        reservationType = nil
        flightType = nil
        startDate = nil
        endDate = nil
        airspaceLocation = nil
        airspacePoints = nil
        planeRoute = nil
        height = nil
    }

    func set(reservationType: ReservationType) {
        self.reservationType = reservationType
    }

    func set(flightType: FlightType) {
        self.flightType = flightType
    }

    func set(startDate: Date, endDate: Date) {
        self.startDate = startDate
        self.endDate = endDate
    }

    func set(airspaceLocation: AirspaceLocation) {
        self.airspaceLocation = airspaceLocation
    }

    func set(customAirspaceOfPoints points: [Coordinates]) {
        self.airspacePoints = points
    }

    func set(planeRoute: PlaneRoute) {
        self.planeRoute = planeRoute
    }

    func set(height: Int) {
        self.height = height
    }
}

private extension DefaultAirspaceReservationManager {

    func saveIfNeeded(reservation: ReservationModel?) {
        if let reservation = reservation {
            storage.add(reservation: reservation)
        }
    }
}

enum FlightType: String, Codable {
    case commercial
    case recreational
}

enum ReservationType: String, Codable {
    case stationary
    case drone
    case plane
}

struct AirspaceLocation: Codable, Equatable {
    let location: Coordinates
    let radius: Int

    private enum CodingKeys: String, CodingKey {
        case location = "center"
        case radius
    }
}

struct ReservationModel: Codable, Equatable {
    let id: String
    let reservationType: ReservationType
    let flightType: FlightType
    let startDate: Date
    let endDate: Date
    let height: Int
    let airspaceLocation: AirspaceLocation?
    let airspacePoints: [Coordinates]?
    let planeRoute: PlaneRoute?

    private enum CodingKeys: String, CodingKey {
        case id
        case reservationType = "reservation_type"
        case flightType = "flight_type"
        case startDate = "start_date"
        case endDate = "end_date"
        case height
        case airspaceLocation = "airspace_location"
        case airspacePoints = "airspace_points"
        case planeRoute = "plane_route"
    }
}

extension ReservationType {

    func toIndex() -> Int {
        switch self {
        case .stationary:
            return 0
        case .drone:
            return 1
        case .plane:
            return 2
        }
    }
}

extension AirspaceLocation {

    func updating(radius: Int) -> AirspaceLocation {
        return AirspaceLocation(location: location, radius: radius)
    }

    func toJSON() -> [String: AnyHashable] {
        return [
            "center": location.toJSON(),
            "radius": radius
        ]
    }
}

extension ReservationType {

    var localizedDescription: String {
        switch self {
        case .drone:
            return "SUMMARY_SCREEN_RESERVATION_TYPE_DRONE".localized
        case .plane:
            return "SUMMARY_SCREEN_RESERVATION_TYPE_PLANE".localized
        case .stationary:
            return "SUMMARY_SCREEN_RESERVATION_TYPE_STATIONARY".localized
        }
    }
}

extension FlightType {

    var localizedDescription: String {
        switch self {
        case .recreational:
            return "SUMMARY_SCREEN_FLIGHT_TYPE_RECREATIONAL".localized
        case .commercial:
            return "SUMMARY_SCREEN_FLIGHT_TYPE_COMMERCIAL".localized
        }
    }
}

extension ReservationModel {

    func isValid(date: Date) -> Bool {
//        return date < endDate && date > startDate
        return date < endDate
    }
}

extension Array where Element == ReservationModel {

    func onlyValidReservations(currentDate: Date) -> [ReservationModel] {
        return self.filter({ $0.isValid(date: currentDate) })
    }
}
