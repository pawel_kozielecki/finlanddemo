import Foundation
import UserNotifications
import UIKit

enum RemoteNotificationsRegistrationStatus {
    case undetermined
    case denied
    case allowed
}

protocol RemoteNotificationsRegistrationManager {

    func getRemoteNotificationsAuthorizationStatus(completion: @escaping (RemoteNotificationsRegistrationStatus) -> Void)
    func registerForRemoteNotifications(completion: @escaping (Bool) -> Void)
}

extension UNAuthorizationStatus {
    func toRemoteNotificationsRegistrationStatus() -> RemoteNotificationsRegistrationStatus {
        switch self {
        case .notDetermined:
            return RemoteNotificationsRegistrationStatus.undetermined
        case .denied:
            return RemoteNotificationsRegistrationStatus.denied
        default:
            return RemoteNotificationsRegistrationStatus.allowed
        }
    }
}

protocol NotificationSettings {
    var authorizationStatus: UNAuthorizationStatus { get }
}

extension UNNotificationSettings: NotificationSettings {

}

protocol RemoteNotificationsCenter {
    func requestAuthorization(options: UNAuthorizationOptions, completionHandler: @escaping (Bool, Error?) -> Void)
    func getNotificationSettings(completionHandler: @escaping (NotificationSettings) -> Swift.Void)
}

protocol DefaultRemoteNotificationsRegistrator {
    func registerForRemoteNotifications()
}

class DefaultRemoteNotificationsRegistrationManager: RemoteNotificationsRegistrationManager {

    var notificationCenter: RemoteNotificationsCenter = UNUserNotificationCenter.current()
    var registrator: DefaultRemoteNotificationsRegistrator = UIApplication.shared
    var operationsExecutor: AsynchronousOperationsExecutor = MainQueueOperationsExecutor()

    func getRemoteNotificationsAuthorizationStatus(completion: @escaping (RemoteNotificationsRegistrationStatus) -> Void) {
        notificationCenter.getNotificationSettings {
            settings in
            completion(settings.authorizationStatus.toRemoteNotificationsRegistrationStatus())
        }
    }

    func registerForRemoteNotifications(completion: @escaping (Bool) -> Void) {
        notificationCenter.requestAuthorization(options: [.badge, .alert, .sound]) {
            [weak self] completed, error in
            //
            //  We must assume the callback is executed on background thread,
            //  ...and the registration must be executed on main thread,
            //  ...so we have to introduce main thread executor to prevent crashes
            //
            self?.operationsExecutor.execute {
                [weak self] in
                self?.registrator.registerForRemoteNotifications()
                completion(completed)
            }
        }
    }
}

extension UNUserNotificationCenter: RemoteNotificationsCenter {
    func getNotificationSettings(completionHandler: @escaping (NotificationSettings) -> Swift.Void) {
        getNotificationSettings { (settings: UNNotificationSettings) in
            completionHandler(settings)
        }
    }
}

extension UIApplication: DefaultRemoteNotificationsRegistrator {
}
