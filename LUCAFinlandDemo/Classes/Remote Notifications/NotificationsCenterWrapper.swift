import Foundation
import UserNotifications
import UIKit

protocol NotificationsCenterWrapperDelegate: class {
    func notificationsCenterWrapper(_ wrapper: NotificationsCenterWrapper, didReactToNotification notification: [AnyHashable: Any], requestID: String, responseID: String)
}

protocol NotificationsCenterWrapper: class {
    var localNotificationsDelegate: NotificationsCenterWrapperDelegate? { get set }
    var remoteNotificationsDelegate: NotificationsCenterWrapperDelegate? { get set }
}

protocol NotificationsCenter: class {
    var delegate: UNUserNotificationCenterDelegate? { get set }
    func add(_ request: UNNotificationRequest, withCompletionHandler completionHandler: ((Error?) -> Swift.Void)?)
}

/*
 *  Once UNNotificationCenter delegate is set, ALL notifications go through it if the app is running
 *  ...this would omit Remote Notifications flow entirely
 *  This wrapper would receive incoming notifications, detect if they are local or remote, and notify appropriate delegate
 *
 *  This is only used in iOS 10+ as lower OS do not have UNNotification center
 */

class DefaultNotificationsCenterWrapper: NSObject, NotificationsCenterWrapper {
    let notificationCenter: NotificationsCenter

    weak var localNotificationsDelegate: NotificationsCenterWrapperDelegate?
    weak var remoteNotificationsDelegate: NotificationsCenterWrapperDelegate?

    required init(notificationCenter: NotificationsCenter) {
        self.notificationCenter = notificationCenter
        super.init()
        self.notificationCenter.delegate = self
    }
}

extension DefaultNotificationsCenterWrapper: UNUserNotificationCenterDelegate {

    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let notificationBody = notification.request.content.userInfo
        self.remoteNotificationsDelegate?.notificationsCenterWrapper(self, didReactToNotification: notificationBody, requestID: notification.request.identifier, responseID: "")
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let notification = response.notification
        let notificationBody = notification.request.content.userInfo
        remoteNotificationsDelegate?.notificationsCenterWrapper(self,
                didReactToNotification: notificationBody,
                requestID: notification.request.identifier,
                responseID: response.actionIdentifier)
    }
}

extension UNUserNotificationCenter: NotificationsCenter {
}
