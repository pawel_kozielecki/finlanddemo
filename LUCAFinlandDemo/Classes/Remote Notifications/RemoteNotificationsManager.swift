import Foundation
import UIKit
import FirebaseCore
import FirebaseMessaging

protocol RemoteNotificationsManager: class {
    var lastAuthorizationStatus: RemoteNotificationsRegistrationStatus { get }
    var deviceToken: Data? { get set }
    var notificationHandlers: [RemoteNotificationHandler] { get }

    func registerForRemoteNotifications(completion: ((Bool) -> Void)?)
    func getRemoteNotificationsRegistrationStatus(completion: ((RemoteNotificationsRegistrationStatus) -> Void)?)
    func refreshPushNotificationStatus(completion: ((RemoteNotificationsRegistrationStatus) -> Void)?)
    func handle(notification: [AnyHashable: Any], forState state: UIApplication.State)
    func set(handlers: [RemoteNotificationHandler])
    func invalidateDeviceToken()
}

protocol ApplicationStateProvider {
    var applicationState: UIApplication.State { get }
}

protocol FirebaseMessagingWrapper {
    var apnsToken: Data? { get set }
    var fcmToken: String? { get }
    var shouldEstablishDirectChannel: Bool { get set }
    var delegate: MessagingDelegate? { get set }

    func subscribe(toTopic topic: String)
    func unsubscribe(fromTopic topic: String)
}

protocol RemoteNotificationHandler {
    func canHandle(notification: [AnyHashable: Any], inApplicationState applicationState: UIApplication.State) -> Bool
    func handle(notification: [AnyHashable: Any], inApplicationState applicationState: UIApplication.State)
}

class DefaultRemoteNotificationsManager: NSObject, RemoteNotificationsManager {

    let pushNotificationsTokenKey = "PUSH_NOTIFICATIONS_TOKEN"
    let firebaseTopics = "DRONE_AREA_CREATIONS"

    let registrationManager: RemoteNotificationsRegistrationManager
    let notificationWrapper: NotificationsCenterWrapper
    var mainQueueExecutor: AsynchronousOperationsExecutor = MainQueueOperationsExecutor()
    var localStorage: LocalStorage = UserDefaultsStorage()
    var firebaseTokenManager: FirebaseMessagingWrapper = Messaging.messaging()
    var applicationStateProvider: ApplicationStateProvider = UIApplication.shared
    var deviceToken: Data? {
        set {
            storeDeviceToken(newToken: newValue)
            firebaseTokenManager.apnsToken = newValue
            if newValue != nil {
                firebaseTokenManager.subscribe(toTopic: firebaseTopics)
                firebaseTokenManager.shouldEstablishDirectChannel = true
                firebaseTokenManager.delegate = self
            } else {
                firebaseTokenManager.unsubscribe(fromTopic: firebaseTopics)
                firebaseTokenManager.shouldEstablishDirectChannel = false
                firebaseTokenManager.delegate = nil
            }
        }
        get {
            return retrieveDeviceToken()
        }
    }

    private (set) var lastAuthorizationStatus: RemoteNotificationsRegistrationStatus
    private (set) var notificationHandlers: [RemoteNotificationHandler] = []

    required init(registrationManager: RemoteNotificationsRegistrationManager, notificationCenterWrapper: NotificationsCenterWrapper) {
        self.registrationManager = registrationManager
        self.lastAuthorizationStatus = .undetermined
        self.notificationWrapper = notificationCenterWrapper

        super.init()

        notificationWrapper.remoteNotificationsDelegate = self
    }

    // Trigger the procedure to register for Pushes and obtain devide token
    func registerForRemoteNotifications(completion: ((Bool) -> Void)?) {
        registrationManager.registerForRemoteNotifications {
            [weak self] completed in
            self?.lastAuthorizationStatus = completed ? .allowed : .denied
            self?.mainQueueExecutor.execute({
                completion?(completed)
            })
        }
    }

    // If we already have a consent, renew push notification registration
    // needed e.g. when device token changed since last application run (nearly impossible to happen, but still...)
    func refreshPushNotificationStatus(completion: ((RemoteNotificationsRegistrationStatus) -> Void)?) {
        registrationManager.getRemoteNotificationsAuthorizationStatus {
            [weak self] status in
            self?.lastAuthorizationStatus = status
            if status == .allowed {
                self?.registerForRemoteNotifications(completion: nil)
            }
            if let completion = completion {
                self?.mainQueueExecutor.execute({
                    completion(status)
                })
            }
        }
    }

    // Query for current notification status
    func getRemoteNotificationsRegistrationStatus(completion: ((RemoteNotificationsRegistrationStatus) -> Void)?) {
        registrationManager.getRemoteNotificationsAuthorizationStatus {
            [weak self] status in
            self?.lastAuthorizationStatus = status
            self?.mainQueueExecutor.execute({
                completion?(status)
            })
        }
    }

    // Handle oncoming remote notifications
    func handle(notification: [AnyHashable: Any], forState state: UIApplication.State) {
        for handler in notificationHandlers {
            if handler.canHandle(notification: notification, inApplicationState: state) {
                handler.handle(notification: notification, inApplicationState: state)
                return
            }
        }
    }

    func set(handlers: [RemoteNotificationHandler]) {
        self.notificationHandlers = handlers
    }

    func invalidateDeviceToken() {
        deviceToken = nil
    }
}

extension DefaultRemoteNotificationsManager: NotificationsCenterWrapperDelegate {

    func notificationsCenterWrapper(_ wrapper: NotificationsCenterWrapper, didReactToNotification notification: [AnyHashable: Any], requestID: String, responseID: String) {
        let state = applicationStateProvider.applicationState
        handle(notification: notification, forState: state)
    }
}

private extension DefaultRemoteNotificationsManager {

    func retrieveDeviceToken() -> Data? {
        return localStorage.readData(key: pushNotificationsTokenKey)
    }

    func storeDeviceToken(newToken: Data?) {
        if let newToken = newToken {
            localStorage.write(key: pushNotificationsTokenKey, value: newToken)
        } else {
            localStorage.invalidate(key: pushNotificationsTokenKey)
        }
    }
}

extension DefaultRemoteNotificationsManager: MessagingDelegate {

    @available(iOS 10.0, *)
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        handle(notification: remoteMessage.appData, forState: .active)
    }
}

extension Messaging: FirebaseMessagingWrapper {
}

extension UIApplication: ApplicationStateProvider {
}
