import Foundation

protocol UserDataStorage: class {

    func store(userData user: User?)
    func invalidate()
}

protocol UserDataProvider: class {
    func getUserData() -> User?
}

protocol UserDataManager: UserDataStorage, UserDataProvider {
}

class DefaultUserDataManager: UserDataManager {

    let key = "user"

    var storage: LocalStorage = UserDefaultsStorage()

    func store(userData user: User?) {
        let encoder = JSONEncoder()
        if let user = user, let userData = try? encoder.encode(user) {
            storage.write(key: key, value: userData)
        } else {
            invalidate()
        }
    }

    func invalidate() {
        storage.invalidate(key: key)
    }

    func getUserData() -> User? {
        guard let userData = storage.readData(key: key) else {
            return nil
        }

        let decoder = JSONDecoder()
        return try? decoder.decode(User.self, from: userData)
    }
}
