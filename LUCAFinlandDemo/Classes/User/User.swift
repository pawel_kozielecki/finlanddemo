import Foundation

struct User: Codable, Equatable {
    let id: String
    let firstName: String
    let lastName: String
    let email: String
}

extension User {

    var fullName: String {
        if !firstName.isEmpty {
            if !lastName.isEmpty {
                return "\(firstName) \(lastName)".capitalized
            } else {
                return firstName.capitalized
            }
        }
        return ""
    }
}
