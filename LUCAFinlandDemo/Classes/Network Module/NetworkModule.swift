import Foundation

typealias NetworkModuleCompletion = (_ response: NetworkResponse, _ error: NSError?) -> Swift.Void

struct NetworkResponse {

    let data: Data?
    let networkResponse: URLResponse?
}

protocol CancellationToken {

    var originalRequest: URLRequest? { get }
    func cancel()
}

protocol NetworkModule {

    @discardableResult func execute(request: NetworkRequest, callback: @escaping NetworkModuleCompletion) -> CancellationToken?
    func cancelRequest(withToken token: CancellationToken)
}

extension URLSessionTask: CancellationToken {

}

extension NetworkResponse {

    var result: Any? {
        let json = data?.toJSON()
        return json
    }

    var statusCode: Int {
        return (self.networkResponse as? HTTPURLResponse)?.statusCode ?? -1
    }
}
