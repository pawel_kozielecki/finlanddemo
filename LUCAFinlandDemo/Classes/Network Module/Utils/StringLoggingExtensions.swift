import Foundation

extension String {

    var lucaLog: String {
        return "[LUCA]\(self)"
    }

    //
    //  NETWORK LOGGING:
    //

    var networkLog: String {
        return "[NETWORK]\(self)".lucaLog
    }

    var networkRequestLog: String {
        return "[REQUEST]\(self)".networkLog
    }

    var networkResponseLog: String {
        return "[RESPONSE]\(self)".networkLog
    }

    var networkErrorLog: String {
        return "[RESPONSE][ERROR]\(self)".networkLog
    }

    var networkResponseWarningLog: String {
        return "[RESPONSE][WARNING]\(self)".networkLog
    }

    var networkWarningLog: String {
        return "[WARNING]\(self)".networkLog
    }

    //
    //  PARSING LOGGING:
    //

    var parseLog: String {
        return "[PARSE]\(self)".lucaLog
    }

    var parseErrorLog: String {
        return "[PARSE][ERROR]\(self)".lucaLog
    }

    //
    //  ANALYTICS LOGGING:
    //

    var analyticsLog: String {
        return "[ANALYTICS]\(self)".lucaLog
    }

    var analyticsWarningLog: String {
        return "[ANALYTICS][WARNING]\(self)".lucaLog
    }
}

extension String: LogParametersProvider {
    var logParameters: String {
        return self
    }
}
