import Foundation

extension URL {

    var isAbsolute: Bool {
        return host != nil
    }
}
