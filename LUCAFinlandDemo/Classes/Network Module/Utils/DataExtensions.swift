import Foundation

extension Data {

    func toJSON() -> Any? {
        do {
            let result = try JSONSerialization.jsonObject(
                    with: self,
                    options: JSONSerialization.ReadingOptions.allowFragments
            )
            return result
        } catch {
            return nil
        }
    }

    func toDeviceTokenString() -> String {
        return self.reduce("", { $0 + String(format: "%02X", $1) })
    }
}
