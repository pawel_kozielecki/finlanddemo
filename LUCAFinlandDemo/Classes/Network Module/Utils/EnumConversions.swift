import Foundation

//  This is an optional initializer where a default value is passed
//  ... when creating an Enum from a given rawValue fails, the default value will be used
//  For this solution to work, init(from decoder: Decoder) must be implemented in an Enum

extension RawRepresentable where RawValue: Decodable {
    init(from decoder: Decoder, default: Self) throws {
        let container = try decoder.singleValueContainer()
        let rawValue = try container.decode(RawValue.self)
        self = Self(rawValue: rawValue) ?? `default`
    }
}

