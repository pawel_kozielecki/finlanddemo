import Foundation

protocol RequestBuilder {

    func build(fromRequest: NetworkRequest) -> URLRequest?
}

class DefaultRequestBuilder: RequestBuilder {

    let urlBase: URL

    required init(urlBase: URL) {
        self.urlBase = urlBase
    }

    func build(fromRequest request: NetworkRequest) -> URLRequest? {
        guard let url = makeUrl(fromRequest: request) else {
            return nil
        }

        var urlRequest = URLRequest(url: url)
        urlRequest.cachePolicy = .reloadIgnoringLocalCacheData
        urlRequest.httpMethod = request.method.rawValue
        if let body = request.body {
            do {
                urlRequest.httpBody = try JSONSerialization.data(withJSONObject: body, options: .prettyPrinted)
            } catch {
                logError("DefaultRequestBuilder:build:\(error.localizedDescription)".parseErrorLog)
            }
        }

        return urlRequest
    }
}

extension DefaultRequestBuilder {

    func makeUrl(fromRequest request: NetworkRequest) -> URL? {
        var components: URLComponents?
        if let url = URL(string: request.path), url.isAbsolute {
            // for absolute paths
            components = URLComponents(string: request.path)
        } else {
            // for relative paths
            let urlBaseString = request.urlBase?.absoluteString ?? urlBase.absoluteString
            components = URLComponents(string: urlBaseString)
            let initialPath = components?.path ?? ""
            components?.path = initialPath + request.path
        }

        if let parameters = request.params {
            components?.queryItems = makeQueryItems(fromParameters: parameters)
        }

        return components?.url
    }

    func makeQueryItems(fromParameters parameters: [String: Any]) -> [URLQueryItem] {
        var queryItems = [URLQueryItem]()

        for (key, value) in parameters {
            let valueString = "\(value)"
            queryItems.append(URLQueryItem(name: key, value: valueString))
        }

        return queryItems
    }
}

