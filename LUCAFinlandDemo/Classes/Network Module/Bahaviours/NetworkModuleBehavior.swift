import Foundation

protocol NetworkModuleBehavior {

    func beforeNetworkCall(request: NetworkRequest, urlRequest: inout URLRequest)
    func afterNetworkCall(request: NetworkRequest, response: NetworkResponse)
}

extension NetworkModuleBehavior {

    func beforeNetworkCall(request: NetworkRequest, urlRequest: inout URLRequest) {
    }

    func afterNetworkCall(request: NetworkRequest, response: NetworkResponse) {
    }
}
