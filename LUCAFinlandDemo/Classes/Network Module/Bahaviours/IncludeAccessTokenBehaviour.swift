import Foundation

class IncludeAccessTokenBehaviour: NetworkModuleBehavior {

    let accessTokenProvider: AccessTokenProvider

    init(accessTokenProvider: AccessTokenProvider) {
        self.accessTokenProvider = accessTokenProvider
    }

    func beforeNetworkCall(request: NetworkRequest, urlRequest: inout URLRequest) {
        guard request.includeAuthenticationToken == true else {
            return
        }

        let token = accessTokenProvider.getToken() ?? ""
        urlRequest.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
    }
}
