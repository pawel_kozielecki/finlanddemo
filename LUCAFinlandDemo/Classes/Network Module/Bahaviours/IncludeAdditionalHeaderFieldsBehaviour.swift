import Foundation

class IncludeAdditionalHeaderFieldsBehaviour: NetworkModuleBehavior {


    func beforeNetworkCall(request: NetworkRequest, urlRequest: inout URLRequest) {
        guard let fields = request.additionalHeaderFields else {
            return
        }

        for (key, value) in fields {
            urlRequest.setValue(value, forHTTPHeaderField: key)
        }
    }
}
