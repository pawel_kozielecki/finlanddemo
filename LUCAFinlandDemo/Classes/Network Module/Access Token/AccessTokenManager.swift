import Foundation

protocol AccessTokenStorage: class {

    func store(token: String?)
    func invalidate()
}

protocol AccessTokenProvider: class {

    func getToken() -> String?
}

protocol AccessTokenManager: AccessTokenStorage, AccessTokenProvider {
}

class DefaultAccessTokenManager: AccessTokenManager {

    let accessTokenKey = "access_token"

    var storage: LocalStorage = UserDefaultsStorage()

    func getToken() -> String? {
        return storage.readString(key: accessTokenKey)
    }

    func store(token: String?) {
        if let token = token {
            storage.write(key: accessTokenKey, value: token)
        } else {
            invalidate()
        }
    }

    func invalidate() {
        storage.invalidate(key: accessTokenKey)
    }
}
