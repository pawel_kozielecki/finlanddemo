import Foundation

class DefaultNetworkModule: NetworkModule {

    let firstHttpErrorStatusCode = 400

    let requestBuilder: RequestBuilder
    let urlSession: URLSession
    let networkBehaviors: [NetworkModuleBehavior]
    let completionExecutor: AsynchronousOperationsExecutor
    let accessTokenProvider: AccessTokenProvider

    init(requestBuilder: RequestBuilder,
         urlSession: URLSession,
         networkBehaviors: [NetworkModuleBehavior],
         accessTokenProvider: AccessTokenProvider,
         completionExecutor: AsynchronousOperationsExecutor = MainQueueOperationsExecutor()) {

        self.requestBuilder = requestBuilder
        self.urlSession = urlSession
        self.networkBehaviors = networkBehaviors
        self.accessTokenProvider = accessTokenProvider
        self.completionExecutor = completionExecutor
    }


    @discardableResult func execute(request: NetworkRequest, callback: @escaping NetworkModuleCompletion) -> CancellationToken? {
        guard request.includeAuthenticationToken == false || accessTokenProvider.getToken() != nil else {
            return nil
        }

        guard var urlRequest = requestBuilder.build(fromRequest: request) else {
            let error = NSError.makeNetworkRequestCreationError()
            executeOnMainThread(
                    completionCallback: callback,
                    response: NetworkResponse(data: nil, networkResponse: nil),
                    error: error)
            return nil
        }

        runBehavioursBeforeNetworkCall(request: request, urlRequest: &urlRequest)

        let task = urlSession.dataTask(with: urlRequest, completionHandler: {
            [weak self] data, response, error in
            if let error = error as NSError? {
                if error.code == NetworkErrorCode.cancelled.rawValue {
                    logWarning("NetworkModule:execute:request cancelled \(error.localizedDescription)".networkResponseWarningLog)
                    return
                }

                self?.executeOnMainThread(
                        completionCallback: callback,
                        response: NetworkResponse(data: nil, networkResponse: response),
                        error: error as NSError)
            } else if let response = response as? HTTPURLResponse {
                var httpError: NSError?

                if self?.requestSucceed(response: response) == true {
                    self?.runBehavioursAfterNetworkCall(
                            request: request,
                            response: NetworkResponse(data: data, networkResponse: response))
                } else {
                    httpError = self?.parseErrorResponse(response: response)
                }

                self?.executeOnMainThread(
                        completionCallback: callback,
                        response: NetworkResponse(data: data, networkResponse: response),
                        error: httpError)
            } else {
                let error = NSError.makeNetworkError(
                        code: NetworkErrorCode.unknown.rawValue,
                        localizedMessage: NetworkErrorCode.unknown.toLocalizedDescription()
                )

                self?.executeOnMainThread(
                        completionCallback: callback,
                        response: NetworkResponse(data: nil, networkResponse: response),
                        error: error)
            }
        })
        task.resume()
        return task
    }

    func cancelRequest(withToken token: CancellationToken) {
        logWarning("NetworkModule:cancelRequest:\(token.originalRequest?.debugDescription ?? "")".networkWarningLog)
        token.cancel()
    }
}

private extension DefaultNetworkModule {

    func executeOnMainThread(completionCallback callback: @escaping NetworkModuleCompletion, response: NetworkResponse, error: NSError?) {
        completionExecutor.execute {
            callback(response, error)
        }
    }

    func runBehavioursBeforeNetworkCall(request: NetworkRequest, urlRequest: inout URLRequest) {
        for behavior in networkBehaviors {
            behavior.beforeNetworkCall(request: request, urlRequest: &urlRequest)
        }
    }

    func runBehavioursAfterNetworkCall(request: NetworkRequest, response: NetworkResponse) {
        for behavior in networkBehaviors {
            behavior.afterNetworkCall(request: request, response: response)
        }
    }

    func requestSucceed(response: HTTPURLResponse) -> Bool {
        return response.statusCode < self.firstHttpErrorStatusCode
    }

    func parseErrorResponse(response: HTTPURLResponse) -> NSError {
        let message = HTTPURLResponse.localizedString(forStatusCode: response.statusCode)
        return NSError.makeNetworkError(code: response.statusCode, localizedMessage: message)
    }
}

extension DefaultNetworkModule {

    static func makeGenericNetworkModule(
            configuration: AppConfiguration,
            accessTokenProvider: AccessTokenProvider,
            completionExecutor: AsynchronousOperationsExecutor = MainQueueOperationsExecutor()) -> NetworkModule {

        let builder = DefaultRequestBuilder(urlBase: configuration.baseURL)
        let session = createUrlSession(timeout: configuration.requestTimeout)
        let behaviours: [NetworkModuleBehavior] = [
            IncludeAdditionalHeaderFieldsBehaviour(),
            IncludeAccessTokenBehaviour(accessTokenProvider: accessTokenProvider)
        ]
        return DefaultNetworkModule(
                requestBuilder: builder, urlSession: session,
                networkBehaviors: behaviours,
                accessTokenProvider: accessTokenProvider,
                completionExecutor: completionExecutor)
    }

    fileprivate static func createUrlSession(timeout: Int) -> URLSession {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = Double(timeout)
        return URLSession(configuration: configuration)
    }
}
