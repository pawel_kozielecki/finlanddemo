import UIKit

class AppDelegate: UIResponder, UIApplicationDelegate {

    var dependencyProvider: DependencyProvider
    var windowController: WindowController
    var appConfigurationFactory = AppConfigurationFactory.sharedInstance

    override init() {
        let configuration = appConfigurationFactory.currentConfiguration
        self.dependencyProvider = DefaultDependencyProvider(appConfiguration: configuration)
        self.windowController = WindowController(dependencyProvider: dependencyProvider)
    }

    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        dependencyProvider.setup(windowController: windowController)
        return true
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        windowController.makeAndPresentInitialViewController()

        if dependencyProvider.loginManager.isLoggedIn {
            dependencyProvider.remoteNotificationsManager.refreshPushNotificationStatus(completion: nil)
        }

        if let remoteNotification = launchOptions?[.remoteNotification] as? [AnyHashable: Any] {
            handle(notification: remoteNotification, inApplicationState: application.applicationState)
        }

        return true
    }
}

extension AppDelegate {

    //
    // MARK: Push Notifications
    //

    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        //todo: handle error
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        dependencyProvider.remoteNotificationsManager.deviceToken = deviceToken
    }
}

private extension AppDelegate {

    func handle(notification: [AnyHashable: Any], inApplicationState applicationState: UIApplication.State) {
        dependencyProvider.remoteNotificationsManager.handle(notification: notification, forState: applicationState)
    }
}
