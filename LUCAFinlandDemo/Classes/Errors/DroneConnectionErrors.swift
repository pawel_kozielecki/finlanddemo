import Foundation

extension NSError {

    class func makeDroneConnectionTimeoutError() -> NSError {
        let domain = ErrorDomain.droneConnection.rawValue
        let code = DroneConnectionErrorCode.timeout.rawValue
        let info = [NSLocalizedDescriptionKey: DroneConnectionErrorCode.timeout.toLocalizedDescription()]
        return NSError(domain: domain, code: code, userInfo: info)
    }
}
