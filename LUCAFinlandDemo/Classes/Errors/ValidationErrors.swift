import Foundation

extension NSError {

    class func makeValidationError(code: Int, message: String) -> NSError {
        let domain = ErrorDomain.validation.rawValue
        let info = [NSLocalizedDescriptionKey: message]
        return NSError(domain: domain, code: code, userInfo: info)
    }

    class func makeInvalidTextLengthValidationError() -> NSError {
        let domain = ErrorDomain.validation.rawValue
        let code = ValidationErrorCode.invalidTextLength.rawValue
        let info = [NSLocalizedDescriptionKey: ValidationErrorCode.invalidTextLength.toLocalizedDescription()]
        return NSError(domain: domain, code: code, userInfo: info)
    }

    class func makeInvalidNumberValidationError() -> NSError {
        let domain = ErrorDomain.validation.rawValue
        let code = ValidationErrorCode.invalidNumber.rawValue
        let info = [NSLocalizedDescriptionKey: ValidationErrorCode.invalidNumber.toLocalizedDescription()]
        return NSError(domain: domain, code: code, userInfo: info)
    }

    class func makeZeroNumberValidationError() -> NSError {
        let domain = ErrorDomain.validation.rawValue
        let code = ValidationErrorCode.invalidZeroNumber.rawValue
        let info = [NSLocalizedDescriptionKey: ValidationErrorCode.invalidZeroNumber.toLocalizedDescription()]
        return NSError(domain: domain, code: code, userInfo: info)
    }

    class func makeInvalidEmailValidationError() -> NSError {
        let domain = ErrorDomain.validation.rawValue
        let code = ValidationErrorCode.invalidEmail.rawValue
        let info = [NSLocalizedDescriptionKey: ValidationErrorCode.invalidEmail.toLocalizedDescription()]
        return NSError(domain: domain, code: code, userInfo: info)
    }

    static func makeInvalidNameError() -> NSError {
        let domain = ErrorDomain.validation.rawValue
        let code = ValidationErrorCode.invalidName.rawValue
        let info = [NSLocalizedDescriptionKey: ValidationErrorCode.invalidName.toLocalizedDescription()]
        return NSError(domain: domain, code: code, userInfo: info)
    }

    static func makeInvalidRestrictedTextError() -> NSError {
        let domain = ErrorDomain.validation.rawValue
        let code = ValidationErrorCode.invalidRestrictedText.rawValue
        let info = [NSLocalizedDescriptionKey: ValidationErrorCode.invalidRestrictedText.toLocalizedDescription()]
        return NSError(domain: domain, code: code, userInfo: info)
    }

    class func makeInvalidPhoneValidationError() -> NSError {
        let domain = ErrorDomain.validation.rawValue
        let code = ValidationErrorCode.invalidPhone.rawValue
        let info = [NSLocalizedDescriptionKey: ValidationErrorCode.invalidPhone.toLocalizedDescription()]
        return NSError(domain: domain, code: code, userInfo: info)
    }

    class func makeInvalidPasswordValidationError() -> NSError {
        let domain = ErrorDomain.validation.rawValue
        let code = ValidationErrorCode.invalidPassword.rawValue
        let info = [NSLocalizedDescriptionKey: ValidationErrorCode.invalidPassword.toLocalizedDescription()]
        return NSError(domain: domain, code: code, userInfo: info)
    }

    class func makePasswordsDoNotMatchValidationError() -> NSError {
        let domain = ErrorDomain.validation.rawValue
        let code = ValidationErrorCode.passwordsDoNotMatch.rawValue
        let info = [NSLocalizedDescriptionKey: ValidationErrorCode.passwordsDoNotMatch.toLocalizedDescription()]
        return NSError(domain: domain, code: code, userInfo: info)
    }

    class func makeUserAlreadyExistsValidationError() -> NSError {
        let domain = ErrorDomain.validation.rawValue
        let code = ValidationErrorCode.userAlreadyExists.rawValue
        let info = [NSLocalizedDescriptionKey: ValidationErrorCode.userAlreadyExists.toLocalizedDescription()]
        return NSError(domain: domain, code: code, userInfo: info)
    }



}
