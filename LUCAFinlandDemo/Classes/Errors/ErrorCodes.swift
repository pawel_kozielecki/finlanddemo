import Foundation

protocol LocalizableDescriptionErrorCode {

    var rawValue: Int { get }
    func toLocalizedDescription() -> String
}

extension LocalizableDescriptionErrorCode {

    func toLocalizedDescription() -> String {
        return "UNKNOWN".localized
    }
}

enum NetworkErrorCode: Int, LocalizableDescriptionErrorCode {
    case unknown = 1000
    case unreachable = 1001
    case requestCreationFailure = 1002
    case cancelled = -999
    case unauthorized = 401
    case forbidden = 403

    func toLocalizedDescription() -> String {
        switch self {
        case .unknown:
            return "UNKNOWN_NETWORK_ERROR".localized
        case .unreachable:
            return "NO_INTERNET_CONNECTION".localized
        case .requestCreationFailure:
            return "REQUEST_CREATION_FAILED".localized
        case .cancelled:
            return "REQUEST_CANCELLED".localized
        case .unauthorized:
            return "UNAUTHORIZED_REQUEST_ERROR".localized
        case .forbidden:
            return "FORBIDDEN_REQUEST_ERROR".localized
        }
    }
}

enum ValidationErrorCode: Int, LocalizableDescriptionErrorCode {
    case invalidTextLength = 2000
    case invalidEmail = 2001
    case invalidPassword = 2002
    case passwordsDoNotMatch = 2003
    case userAlreadyExists = 2004
    case invalidPhone = 2005
    case invalidName = 2006
    case invalidRestrictedText = 2007
    case invalidNumber = 2008
    case invalidZeroNumber = 2009

    func toLocalizedDescription() -> String {
        switch self {
        case .invalidTextLength:
            return "INVALID_TEXT_LENGTH".localized
        case .invalidEmail:
            return "INVALID_EMAIL".localized
        case .invalidPassword:
            return "INVALID_PASSWORD".localized
        case .passwordsDoNotMatch:
            return "PASSWORDS_DO_NOT_MATCH".localized
        case .userAlreadyExists:
            return "REGISTER_ERROR_MESSAGE".localized
        case .invalidPhone:
            return "INVALID_PHONE".localized
        case .invalidName:
            return "INVALID_TEXT".localized
        case .invalidRestrictedText:
            return "INVALID_TEXT".localized
        case .invalidNumber:
            return "INVALID_NUMBER".localized
        case .invalidZeroNumber:
            return "INVALID_ZERO_NUMBER".localized
        }
    }
}

enum DroneConnectionErrorCode: Int, LocalizableDescriptionErrorCode {
    case timeout = 3000

    func toLocalizedDescription() -> String {
        switch self {
        case .timeout:
            return "DRONE_CONNECTION_TIMEOUT_ERROR".localized
        }
    }
}
