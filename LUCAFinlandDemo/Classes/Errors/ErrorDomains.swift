import Foundation

enum ErrorDomain: String {
    case network = "pl.lhsystems.luca.network"
    case validation = "pl.lhsystems.luca.validation"
    case droneConnection = "pl.lhsystems.luca.droneConnection"
}
