import Foundation

enum AsynchronousExecutorType {
    case main
    case background
}

protocol AsynchronousOperationsExecutor {
    func execute(_ block: @escaping () -> Void)
}

class MainQueueOperationsExecutor: AsynchronousOperationsExecutor {
    func execute(_ block: @escaping () -> Void) {
        DispatchQueue.main.async {
            block()
        }
    }
}

class BackgroundQueueOperationsExecutor: AsynchronousOperationsExecutor {
    func execute(_ block: @escaping () -> Void) {
        DispatchQueue(label: "BackgroundQueueOperationsExecutor", attributes: .concurrent).async {
            block()
        }
    }
}
