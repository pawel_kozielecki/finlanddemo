import Foundation

protocol DelayedAsynchronousBlocksExecutor {

    var queue: DispatchQueue { get }
    var type: AsynchronousExecutorType { get }

    func executeDelayed(by delay: Double, block: @escaping () -> Void)
}

extension DelayedAsynchronousBlocksExecutor {

    func executeDelayed(by delay: Double, block: @escaping () -> Void) {
        queue.asyncAfter(deadline: DispatchTime.now() + delay) {
            block()
        }
    }
}

class MainThreadDelayedAsynchronousBlocksExecutor: DelayedAsynchronousBlocksExecutor {

    let queue = DispatchQueue.main
    let type: AsynchronousExecutorType = .main
}
