import Foundation

protocol DateProvider {
    func currentDate() -> Date
}

class SimpleDateProvider: DateProvider {

    func currentDate() -> Date {
        return Date()
    }
}
