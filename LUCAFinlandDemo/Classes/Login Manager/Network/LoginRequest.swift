import Foundation

struct LoginRequest: NetworkRequest {

    let method = RequestMethod.post
    let path = "/login"
    let includeAuthenticationToken = false
    let params: [String: Any]? = nil
    let isAuthenticationRequest: Bool = true
    let additionalHeaderFields: [String: String]? = nil

    let body: [String: Any]?

    init(credentials: LoginCredentials) {
        body = [
            "login": credentials.login,
            "password": credentials.password
        ]
    }
}
