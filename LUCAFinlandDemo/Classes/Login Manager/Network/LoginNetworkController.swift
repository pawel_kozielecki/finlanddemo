import Foundation

struct LoginResult: Codable {
    let user: User
    let token: String
}

class LoginNetworkController {

    let networkModule: NetworkModule

    required init(networkModule: NetworkModule) {
        self.networkModule = networkModule
    }

    func login(withCredentials credentials: LoginCredentials, completion: ((LoginResult?, NSError?) -> Void)?) {
        let request = LoginRequest(credentials: credentials)
        let decoder = JSONDecoder()
        networkModule.execute(request: request) {
            response, error in
            if let error = error {
                completion?(nil, error)
            } else if let resultData = response.data {
                do {
                    let loginResult = try decoder.decode(LoginResult.self, from: resultData)
                    completion?(loginResult, nil)
                } catch {
                    completion?(nil, error as NSError)
                }
            }
        }
    }

    func logout(completion: ((NSError?) -> Void)?) {
        let request = LogoutRequest()
        networkModule.execute(request: request) {
            response, error in
            completion?(error)
        }
    }
}
