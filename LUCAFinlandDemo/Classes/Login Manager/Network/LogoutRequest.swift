import Foundation

struct LogoutRequest: NetworkRequest {

    let method = RequestMethod.post
    let includeAuthenticationToken = true
    let path = "/logout"
    let params: [String: Any]? = nil
    let body: [String: Any]? = nil
    let isAuthenticationRequest: Bool = true
}
