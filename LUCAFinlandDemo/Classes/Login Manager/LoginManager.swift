import Foundation
import UIKit
import FirebaseInstanceID

struct LoginCredentials {
    let login: String
    let password: String
}

protocol LoginManager {
    var isLoggedIn: Bool { get }
    func logIn(credentials: LoginCredentials, completion: ((User?, NSError?) -> Void)?)
    func logOut(manual: Bool, completion: ((Bool) -> Void)?)
}

class DefaultLoginManager: LoginManager {

    let networkController: LoginNetworkController
    unowned var accessTokenManager: AccessTokenManager
    unowned var userDataManager: UserDataManager
    unowned var remoteNotificationsManager: RemoteNotificationsManager


    var firebaseTokenRemover: FirebaseTokenRemover = InstanceID.instanceID()

    private (set) var isBeingLoggedOut: Bool = false


    init(accessTokenManager: AccessTokenManager, userDataManager: UserDataManager, remoteNotificationsManager: RemoteNotificationsManager, networkController: LoginNetworkController) {
        self.accessTokenManager = accessTokenManager
        self.userDataManager = userDataManager
        self.remoteNotificationsManager = remoteNotificationsManager
        self.networkController = networkController
    }

    var isLoggedIn: Bool {
        return accessTokenManager.getToken() != nil
    }

    func logIn(credentials: LoginCredentials, completion: ((User?, NSError?) -> Void)?) {
        performLogIn(credentials: credentials, completion: completion)
    }

    func logOut(manual: Bool, completion: ((Bool) -> Void)?) {
        performLogout(manual: manual, completion: completion)
    }
}

private extension DefaultLoginManager {

    func performLogIn(credentials: LoginCredentials, completion: ((User?, NSError?) -> Void)?) {
        guard isLoggedIn == false else {
            completion?(userDataManager.getUserData(), nil)
            return
        }

        networkController.login(withCredentials: credentials) {
            [weak self] loginResult, error in
            if let loginResult = loginResult {
                self?.accessTokenManager.store(token: loginResult.token)
                self?.userDataManager.store(userData: loginResult.user)
                self?.remoteNotificationsManager.registerForRemoteNotifications(completion: nil)
                completion?(loginResult.user, nil)
            } else {
                completion?(nil, error)
            }
        }
    }

    func performLogout(manual: Bool, completion: ((Bool) -> Void)?) {
        guard isLoggedIn == true && isBeingLoggedOut == false else {
            return
        }

        isBeingLoggedOut = true
        networkController.logout {
            [weak self] error in
            self?.isBeingLoggedOut = false
            self?.accessTokenManager.invalidate()
            self?.remoteNotificationsManager.invalidateDeviceToken()
            self?.userDataManager.invalidate()
            self?.removeFirebaseToken()
            completion?(true)
        }
    }

    func removeFirebaseToken() {
        firebaseTokenRemover.deleteID {
            error in
            if let error = error {
                logWarning("LoginManager:removeFirebaseToken:\(error.localizedDescription)".lucaLog)
            }
        }
    }
}

protocol FirebaseTokenRemover {
    func deleteID(handler: @escaping (Error?) -> Swift.Void)
}

extension InstanceID: FirebaseTokenRemover {
}
