import Foundation

class UserDefaultsStorage: LocalStorage {

    var defaults: UserDefaults = UserDefaults.standard

    func readString(key: String) -> String? {
        return defaults.string(forKey: key)
    }

    func readData(key: String) -> Data? {
        return defaults.data(forKey: key)
    }

    func readDate(key: String) -> Date? {
        return defaults.object(forKey: key) as? Date
    }

    func write(key: String, value: String) {
        defaults.set(value, forKey: key)
        defaults.synchronize()
    }

    func write(key: String, value: Data) {
        defaults.set(value, forKey: key)
        defaults.synchronize()
    }

    func write(key: String, value: Date) {
        defaults.set(value, forKey: key)
        defaults.synchronize()
    }

    func invalidate(key: String) {
        defaults.removeObject(forKey: key)
        defaults.synchronize()
    }
}
