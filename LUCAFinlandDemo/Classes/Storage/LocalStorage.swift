import Foundation

protocol LocalStorage {

    func readString(key: String) -> String?
    func readData(key: String) -> Data?
    func readDate(key: String) -> Date?
    func write(key: String, value: String)
    func write(key: String, value: Data)
    func write(key: String, value: Date)
    func invalidate(key: String)
}

extension LocalStorage {

    func invalidate(key: String) {
    }
}
