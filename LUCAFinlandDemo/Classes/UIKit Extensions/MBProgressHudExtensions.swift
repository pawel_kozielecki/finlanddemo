import Foundation
import UIKit
import MBProgressHUD

extension MBProgressHUD {

    class func showPreloaderView(addedTo view: UIView, animated: Bool = true) -> MBProgressHUD {
        let preloaderHud = MBProgressHUD.showProgressView(addedTo: view, animated: animated)
        preloaderHud.backgroundView.style = .solidColor
        preloaderHud.backgroundView.color = UIColor.black.withAlphaComponent(0.3)
        return preloaderHud
    }

    class func showProgressView(addedTo view: UIView, animated: Bool = true, loaderTint: UIColor = .white) -> MBProgressHUD {
        let image = UIImage(named: "icon-preloader")!
        let loader = RotatingLoaderView(image: image.withRenderingMode(.alwaysTemplate))
        loader.loaderImageView.tintColor = loaderTint
        let progressHud = MBProgressHUD.showCustomHUDAddedToView(view, animated: animated, customView: loader)
        return progressHud
    }

    class func showCustomHUDAddedToView(_ view: UIView, animated: Bool, customView: UIView) -> MBProgressHUD {
        let progressHUD = MBProgressHUD.showAdded(to: view, animated: animated)
        progressHUD.mode = .customView
        progressHUD.bezelView.color = .clear
        progressHUD.bezelView.style = .solidColor
        progressHUD.customView = customView
        progressHUD.margin = 10
        return progressHUD
    }
}


