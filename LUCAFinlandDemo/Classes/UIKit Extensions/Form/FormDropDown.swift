import UIKit

@IBDesignable
class FormDropDown: DropDown {
    
    @IBInspectable
    var obligatory: Bool = false
    
    @IBInspectable
    var message: String? = nil
}
