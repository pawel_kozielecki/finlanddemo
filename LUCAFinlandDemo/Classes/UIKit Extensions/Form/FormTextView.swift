import UIKit

@IBDesignable
class FormTextView: UITextView {
    
    @IBInspectable
    var obligatory: Bool = true
    
    @IBInspectable
    var message: String? = nil
}
