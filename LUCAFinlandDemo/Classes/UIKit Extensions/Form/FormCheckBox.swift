import UIKit

@IBDesignable
class FormCheckBox: UIButton {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        arrangeView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        arrangeView()
    }
    
    @IBInspectable var borderColor: UIColor = UIColor.lightGray {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 2.0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 5.0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var padding: CGFloat = CGFloat(15)
    
    @IBInspectable var selectedBackground: UIColor = UIColor.clear
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        let newBound = CGRect(
            x: self.bounds.origin.x - padding,
            y: self.bounds.origin.y - padding,
            width: self.bounds.width + 2 * padding,
            height: self.bounds.width + 2 * padding
        )
        
        return newBound.contains(point)
    }
}

private extension FormCheckBox {
    func arrangeView() {
        addTarget(self, action: #selector(FormCheckBox.checkboxTapped), for: .touchUpInside)
        setTitle(nil, for: UIControl.State())
        
        clipsToBounds = true
        
        setCheckBoxImage()
    }
    
    func setCheckBoxImage() {
        let image = UIImage(named: "checkbox-black")
        imageView?.contentMode = .scaleAspectFill
        
        setImage(nil, for: UIControl.State())
        setImage(image, for: .selected)
        setImage(image, for:  .highlighted)
    }
    
    @objc func checkboxTapped(_ sender: FormCheckBox) {
        isSelected = !isSelected
        backgroundColor = isSelected ? selectedBackground : .clear
    }
}
