import UIKit

@IBDesignable
class FormTextField: UITextField {
    
    @IBInspectable
    var isObligatory: Bool = true
    
    @IBInspectable
    var message: String? = nil
    
    var isObligatoryAndEmpty: Bool {
        return isObligatory && text?.isEmpty ?? true
    }
}

