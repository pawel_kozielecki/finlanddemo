import Foundation
import UIKit

@IBDesignable
class DSRoundedButton: UIButton {

    @IBInspectable
    var enabledColor: UIColor = .clear {
        didSet {
            let round: CGFloat = 5
            let enabledImage = UIImage.circularImage(withColor: enabledColor, diameter: round * 2)
            apply(scalableImage: enabledImage, inset: round, forState: .normal)
        }
    }

    @IBInspectable
    var disabledColor: UIColor = .clear {
        didSet {
            let round: CGFloat = 5
            let disabledImage = UIImage.circularImage(withColor: disabledColor, diameter: round * 2)
            apply(scalableImage: disabledImage, inset: round, forState: .disabled)
        }
    }
}

private extension DSRoundedButton {
    func apply(scalableImage: UIImage?, inset: CGFloat, forState state: UIControl.State) {
        let insets = UIEdgeInsets(top: inset, left: inset, bottom: inset, right: inset)
        let image = scalableImage?.resizableImage(withCapInsets: insets)
        setBackgroundImage(image, for: state)
    }
}
