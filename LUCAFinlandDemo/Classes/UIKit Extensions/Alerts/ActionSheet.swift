import Foundation
import UIKit

struct ActionSheetOption {
    let title: String
    let style: UIAlertAction.Style
    let value: Any?
}

protocol ActionSheetDelegate: class {
    func actionPicker(_ actionPicker: ActionSheet, didSelectAction action: ActionSheetOption)
    func actionPickerDidCancel(_ actionPicker: ActionSheet)
}

protocol ActionSheet {
    var delegate: ActionSheetDelegate? { get set }
    func show(title: String, options: [ActionSheetOption])
}

class DefaultActionSheet: NSObject, ActionSheet {

    weak var presentingViewController: UIViewController?

    var delegate: ActionSheetDelegate?

    init(presentingViewController: UIViewController) {
        self.presentingViewController = presentingViewController
    }

    func show(title: String, options: [ActionSheetOption]) {
        let alertController = UIAlertController(title: title, message: nil, preferredStyle: .actionSheet)
        alertController.view.tintColor = .dsViolet

        for option in options {
            let action = UIAlertAction(title: option.title, style: option.style) {
                [weak self] _ in
                guard let strongSelf = self else {
                    return
                }
                strongSelf.delegate?.actionPicker(strongSelf, didSelectAction: option)
            }
            alertController.addAction(action)
        }

        let cancelAction = UIAlertAction(title: "CANCEL".localized, style: .cancel)
        alertController.addAction(cancelAction)

        presentingViewController?.present(alertController, animated: true)
    }
}

extension ActionSheetOption {

    init(title: String, value: Any? = nil) {
        self.init(title: title, style: .default, value: value)
    }
}

extension ActionSheetDelegate {
    func actionPickerDidCancel(_ actionPicker: ActionSheet) {
    }
}
