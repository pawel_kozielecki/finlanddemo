import Foundation
import UIKit

protocol InfoAlert: class {

    func show(title: String?, message: String?, completion: (() -> ())?)
    func dismiss(animated: Bool)
}

class SimpleInfoAlert: InfoAlert {

    let aboveAlertWindowLevel = CGFloat(2001)

    var alert: UIAlertController?
    weak var presentingViewController: UIViewController?

    init(presentingViewController: UIViewController) {
        self.presentingViewController = presentingViewController
    }

    func show(title: String?, message: String?, completion: (() -> Void)?) {
        let okButtonLabel = "OK".localized
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.view.tintColor = .dsViolet

        alert.addAction(UIAlertAction(title: okButtonLabel, style: .default, handler: {
            [weak self] alert in
            completion?()
            self?.alert = nil
        }))
        presentingViewController?.present(alert, animated: true, completion: nil)
        self.alert = alert
    }

    func dismiss(animated: Bool) {
        alert?.dismiss(animated: animated, completion: nil)
        self.alert = nil
    }
}
