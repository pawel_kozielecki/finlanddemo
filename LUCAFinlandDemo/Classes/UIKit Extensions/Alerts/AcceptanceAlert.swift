import Foundation
import UIKit

enum AcceptanceAlertAnswer: Int {
    case no = 0
    case yes = 1
    case none = 3

    func toBool() -> Bool {
        return self == .yes
    }
}

protocol AcceptanceAlert: class {

    var additionalData: Any? { get set }

    func show(title: String?, message: String?, completion: ((AcceptanceAlertAnswer) -> Void)?)

    func dismiss(animated: Bool)
}

class SimpleAcceptanceAlert: AcceptanceAlert {

    var alert: UIAlertController?

    var additionalData: Any?

    weak private var presentingViewController: UIViewController?            // defined as optional to prevent retain cycles
    weak var visibleViewControllerProvider: VisibleViewControllerProvider?  // as above
    private var completion: ((AcceptanceAlertAnswer) -> Void)?

    init(presentingViewController: UIViewController) {
        self.presentingViewController = presentingViewController
    }

    init(visibleViewControllerProvider: VisibleViewControllerProvider) {
        self.visibleViewControllerProvider = visibleViewControllerProvider
    }

    func show(title: String?, message: String?, completion: ((AcceptanceAlertAnswer) -> Void)?) {
        self.completion = completion
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.view.tintColor = .dsViolet
        alert.addAction(UIAlertAction(title: "NO".localized, style: .default, handler: {
            [weak self] alert in
            completion?(AcceptanceAlertAnswer.no)
            self?.completion = nil
            self?.alert = nil
        }))
        alert.addAction(UIAlertAction(title: "YES".localized, style: .default, handler: {
            [weak self] alert in
            completion?(AcceptanceAlertAnswer.yes)
            self?.completion = nil
            self?.alert = nil
        }))

        currentPresentingViewController?.present(alert, animated: true, completion: nil)
        self.alert = alert
    }

    func dismiss(animated: Bool) {
        alert?.dismiss(animated: animated)
        alert = nil
        completion?(AcceptanceAlertAnswer.none)
        completion = nil
    }
}

private extension SimpleAcceptanceAlert {

    var currentPresentingViewController: UIViewController? {
        return presentingViewController ?? visibleViewControllerProvider?.visibleViewController()
    }
}
