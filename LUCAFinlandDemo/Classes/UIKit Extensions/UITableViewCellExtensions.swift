import Foundation
import UIKit

extension UITableViewCell {

    func animateBackground(isSelected: Bool, color: UIColor = .dsDarkGrey) {
        UIView.animate(withDuration: 0.1) {
            [weak self] in
            let lighterColor = color.lighter(by: 0.05)
            self?.backgroundColor = isSelected ? lighterColor : color
        }
    }
}
