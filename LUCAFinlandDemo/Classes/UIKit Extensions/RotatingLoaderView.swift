import Foundation
import UIKit
import QuartzCore
import SnapKit

class RotatingLoaderView: UIView {

    let loaderImageView: UIImageView

    init(image: UIImage) {
        self.loaderImageView = UIImageView(image: image)
        super.init(frame: CGRect.zero)
        addSubview(loaderImageView)
        loaderImageView.snp.makeConstraints {
            make in
            make.edges.equalToSuperview()
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("Not implemented")
    }

    override func didMoveToWindow() {
        super.didMoveToWindow()
        rotate()
    }
}

private extension RotatingLoaderView {

    func rotate() {
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.fromValue = 0
        rotateAnimation.toValue = CGFloat((Double.pi * 360) / 180)
        rotateAnimation.duration = 1
        rotateAnimation.repeatCount = 1000
        layer.add(rotateAnimation, forKey: "360")
    }
}

