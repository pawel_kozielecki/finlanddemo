import Foundation

class PasswordTextfieldValidator: TextfieldValidator {

    func validate(value: String?) -> NSError? {
        // No point in setting upper limit of password length
        // All LucaTextfields have an upper limit of 100 characters and would not allow more

        if let value = value, value.count >= 8, containsNumber(value: value) {
            return nil
        }
        return NSError.makeInvalidPasswordValidationError()
    }
}

private extension PasswordTextfieldValidator {

    func containsNumber(value: String) -> Bool {
        let decimalCharacters = CharacterSet.decimalDigits
        let decimalRange = value.rangeOfCharacter(from: decimalCharacters)
        return decimalRange != nil
    }
}
