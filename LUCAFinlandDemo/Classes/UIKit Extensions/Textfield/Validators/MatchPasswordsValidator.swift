import Foundation

class MatchPasswordsValidator: TextfieldValidator {

    var firstPasswordValue: String?

    func validate(value: String?) -> NSError? {
        let passwordsDoNotMatchError = NSError.makePasswordsDoNotMatchValidationError()
        
        if let value = value, let firstPasswordValue = firstPasswordValue {
            if value == firstPasswordValue {
                return nil
            }
        }
        return passwordsDoNotMatchError
    }
}
