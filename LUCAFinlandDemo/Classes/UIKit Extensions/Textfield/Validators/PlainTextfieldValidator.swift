import Foundation

class PlainTextfieldValidator: TextfieldValidator {

    let lowerCharacterLimit: Int
    let upperCharacterLimit: Int?

    init(lowerCharacterLimit: Int = 1, upperCharacterLimit: Int? = nil) {
        self.lowerCharacterLimit = lowerCharacterLimit
        self.upperCharacterLimit = upperCharacterLimit
    }

    func validate(value: String?) -> NSError? {
        let lengthValidationError = NSError.makeInvalidTextLengthValidationError()
        if let value = value, value.count >= lowerCharacterLimit {
            if let maxCharactersNum = upperCharacterLimit {
                return value.count <= maxCharactersNum ? nil : lengthValidationError
            } else {
                return nil
            }   
        }
        return lengthValidationError
    }
}
