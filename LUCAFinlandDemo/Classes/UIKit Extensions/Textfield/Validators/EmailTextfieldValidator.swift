import Foundation

class EmailTextfieldValidator: TextfieldValidator {

    let emailRegEx = "^[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-+]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z‌​]{2,})$"

    func validate(value: String?) -> NSError? {
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        if emailTest.evaluate(with: value) {
            return nil
        } else {
            return NSError.makeInvalidEmailValidationError()
        }
    }
}
