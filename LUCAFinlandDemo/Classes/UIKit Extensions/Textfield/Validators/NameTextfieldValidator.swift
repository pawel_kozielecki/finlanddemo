import Foundation

class NameTextfieldValidator: TextfieldValidator {
    let alphaNumericsRegEx = "^[\\w \\-]{2,30}$"    // Only letters, spaces and hyphen
    let noDigitsRegEx = "^[\\D]{2,30}$"             // No digits

    func validate(value: String?) -> NSError? {
        guard let value = value else {
            return NSError.makeInvalidNameError()
        }

        if value.range(of: alphaNumericsRegEx, options: .regularExpression) != nil
                   && value.range(of: noDigitsRegEx, options: .regularExpression) != nil
                   && value != "" {
            return nil
        } else {
            return NSError.makeInvalidNameError()
        }
    }
}
