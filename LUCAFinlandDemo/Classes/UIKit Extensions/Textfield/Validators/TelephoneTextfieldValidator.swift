import Foundation

class TelephoneTextfieldValidator: TextfieldValidator {
    let phoneRegEx = "^(\\+){0,1}[0-9]{8,20}$"

    func validate(value: String?) -> NSError? {
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegEx)
        if phoneTest.evaluate(with: value?.split(separator: " ").joined()) {
            return nil
        } else {
            return NSError.makeInvalidPhoneValidationError()
        }
    }
}
