import Foundation

class NotEmptyFieldValidator: TextfieldValidator {
    func validate(value: String?) -> NSError? {
        guard let value = value else {
            return NSError.makeInvalidNameError()
        }

        if value.trimmingCharacters(in: .whitespaces).isEmpty {
            return NSError.makeInvalidNameError()
        } else {
            return nil
        }
    }
}
