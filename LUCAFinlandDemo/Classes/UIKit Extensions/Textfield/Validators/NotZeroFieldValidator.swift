import Foundation

class NotZeroFieldValidator: TextfieldValidator {
    func validate(value: String?) -> NSError? {
        guard let value = value else {
            return NSError.makeZeroNumberValidationError()
        }

        let floatValue = Float.fromString(stringValue: value)
        if floatValue != 0 {
            return nil
        } else {
            return NSError.makeZeroNumberValidationError()
        }
    }
}
