import Foundation

class NumberFieldValidator: TextfieldValidator {
    let numberRegEx = "\\d+(\\.\\d*)?|\\.\\d+"

    func validate(value: String?) -> NSError? {
        guard let value = value else {
            return NSError.makeInvalidNumberValidationError()
        }

        let floatValue = Float.fromString(stringValue: value)
        let numberTest = NSPredicate(format: "SELF MATCHES %@", numberRegEx)
        if numberTest.evaluate(with: value) && floatValue != nil {
            return nil
        } else {
            return NSError.makeInvalidNumberValidationError()
        }
    }
}
