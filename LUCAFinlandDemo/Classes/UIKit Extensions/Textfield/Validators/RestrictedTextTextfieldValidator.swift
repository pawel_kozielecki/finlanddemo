import Foundation

class RestrictedTextTextfieldValidator: TextfieldValidator {
    let alphaNumericsRegEx = "^[\\w \\-]{2,30}$"    // Only letters, numbers, spaces and hyphen

    func validate(value: String?) -> NSError? {
        guard let value = value else {
            return NSError.makeInvalidRestrictedTextError()
        }

        if value.range(of: alphaNumericsRegEx, options: .regularExpression) != nil && value != "" {
            return nil
        } else {
            return NSError.makeInvalidRestrictedTextError()
        }
    }
}
