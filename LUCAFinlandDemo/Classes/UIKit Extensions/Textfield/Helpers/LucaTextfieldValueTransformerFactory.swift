import Foundation

protocol LucaTextfieldValueTransformerFactory {

    func makeValueTransformer(forTextfieldType type: LucaTextfieldType) -> LucaTextfieldValueTransformer
}

class DefaultLucaTextfieldValueTransformerFactory: LucaTextfieldValueTransformerFactory {

    func makeValueTransformer(forTextfieldType type: LucaTextfieldType) -> LucaTextfieldValueTransformer {
        switch type {
        case .date:
            return LucaDatePickerValueTransformer()
        case .dateNoRange:
            return LucaDatePickerValueTransformer()
        case .futureDate:
            return LucaDateTimePickerValueTransformer()
        default:
            return UnmodyfyingLucaTextfieldValueTransformer()
        }
    }
}
