import Foundation
import UIKit

extension UITextField {

    // to avoid nasty trailing space after switching to / from secure text entry
    func luca_fixTrailingSpaceAfterSecureTextEntryModeToggle() {
        let text = self.text
        self.text = nil
        self.text = text
    }
}
