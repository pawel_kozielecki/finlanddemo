import Foundation
import UIKit

class LucaFromDateToDateUpdater {
    static func updateDateRange(inputFromDate: DSTextfield, inputToDate: DSTextfield) {
        if let fromDate = inputFromDate.valueTransformer?.transform(value: inputFromDate.value) as? Date,
           let toDatePicker = inputToDate.textField.inputView as? UIDatePicker {
            toDatePicker.minimumDate = fromDate
        }

        if let toDate = inputToDate.valueTransformer?.transform(value: inputToDate.value) as? Date,
           let fromDatePicker = inputFromDate.textField.inputView as? UIDatePicker {
            fromDatePicker.maximumDate = toDate
        }
    }
}
