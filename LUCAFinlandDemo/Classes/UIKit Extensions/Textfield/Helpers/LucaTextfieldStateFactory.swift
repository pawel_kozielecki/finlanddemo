import Foundation
import UIKit

enum LucaTextfieldType: Int {
    case text = 0
    case email = 1
    case password = 2
    case date = 3
    case phone = 4
    case dateNoRange = 5
    case numericValue = 6
    case futureDate = 7
}

struct LucaTextfieldState {
    let placeholderLabelColor: UIColor?
    let lineColor: UIColor
    let errorMessage: String?
    let showOkIcon: Bool
    let showRevealIcon: Bool
    let keyboardType: UIKeyboardType
}

struct LucaTextfieldStateFactoryConfiguration {
    let errorColor: UIColor
    let inputColor: UIColor
    let placeholderColor: UIColor
}

protocol LucaTextfieldStateFactory {

    func setup(configuration: LucaTextfieldStateFactoryConfiguration)
    func calculateTextfieldState(forTextfieldType type: LucaTextfieldType, isTyping: Bool, currentValue value: String?, validationError: NSError?) -> LucaTextfieldState
}

class DefaultLucaTextfieldStateFactory: LucaTextfieldStateFactory {

    var configuration: LucaTextfieldStateFactoryConfiguration?

    func setup(configuration: LucaTextfieldStateFactoryConfiguration) {
        self.configuration = configuration
    }

    func calculateTextfieldState(forTextfieldType type: LucaTextfieldType, isTyping: Bool, currentValue value: String?, validationError: NSError?) -> LucaTextfieldState {
        guard let configuration = configuration else {
            return LucaTextfieldState(
                    placeholderLabelColor: nil,
                    lineColor: .lightGray,
                    errorMessage: nil,
                    showOkIcon: false,
                    showRevealIcon: false,
                    keyboardType: .default)
        }

        let keyboardType = getKeyboardType(forTextfieldType: type)
        let showRevealIcon = type == .password
        let hasValue = value?.isEmpty ?? false
        if !hasValue {
            if isTyping {
                // user is still typing
                return LucaTextfieldState(
                        placeholderLabelColor: configuration.inputColor,
                        lineColor: configuration.inputColor,
                        errorMessage: nil,
                        showOkIcon: false,
                        showRevealIcon: showRevealIcon,
                        keyboardType: keyboardType)
            } else if let validationError = validationError {
                // invalid input
                return LucaTextfieldState(
                        placeholderLabelColor: configuration.errorColor,
                        lineColor: configuration.errorColor,
                        errorMessage: validationError.localizedDescription,
                        showOkIcon: false,
                        showRevealIcon: showRevealIcon,
                        keyboardType: keyboardType)
            } else {
                // inout is correct
                return LucaTextfieldState(
                        placeholderLabelColor: configuration.placeholderColor,
                        lineColor: configuration.placeholderColor,
                        errorMessage: nil,
                        showOkIcon: true,
                        showRevealIcon: showRevealIcon,
                        keyboardType: keyboardType)
            }
        } else {
            if isTyping {
                return LucaTextfieldState(
                        placeholderLabelColor: configuration.inputColor,
                        lineColor: configuration.inputColor,
                        errorMessage: nil,
                        showOkIcon: false,
                        showRevealIcon: showRevealIcon,
                        keyboardType: keyboardType)
            } else {
                // initial state or no value:
                return LucaTextfieldState(
                        placeholderLabelColor: nil,
                        lineColor: configuration.placeholderColor,
                        errorMessage: nil,
                        showOkIcon: false,
                        showRevealIcon: showRevealIcon,
                        keyboardType: keyboardType)
            }
        }
    }

    func getKeyboardType(forTextfieldType textfieldType: LucaTextfieldType) -> UIKeyboardType {
        switch textfieldType {
        case .email:
            return .emailAddress
        case .phone:
            return .phonePad
        case .numericValue:
            return .decimalPad
        default:
            return .default
        }
    }
}
