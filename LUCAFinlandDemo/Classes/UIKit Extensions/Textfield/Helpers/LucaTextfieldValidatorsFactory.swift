import Foundation

protocol TextfieldValidator {

    func validate(value: String?) -> NSError?
}

protocol LucaTextfieldValidatorsFactory {

    func makeValidators(forTextfieldType type: LucaTextfieldType) -> [TextfieldValidator]
}

class DefaultLucaTextfieldValidatorsFactory: LucaTextfieldValidatorsFactory {

    func makeValidators(forTextfieldType type: LucaTextfieldType) -> [TextfieldValidator] {
        switch type {
        case .text:
            return [PlainTextfieldValidator()]
        case .email:
            return [EmailTextfieldValidator()]
        case .password:
            return [PasswordTextfieldValidator()]
        case .date:
            return [PlainTextfieldValidator(lowerCharacterLimit: 10, upperCharacterLimit: 10)]  // date is in YYYY-MM-DD format, so 10 characters
        case .dateNoRange:
            return [PlainTextfieldValidator(lowerCharacterLimit: 10, upperCharacterLimit: 10)]  // date is in YYYY-MM-DD format, so 10 characters
        case .futureDate:
            return []
        case .phone:
            return [TelephoneTextfieldValidator()]
        case .numericValue:
            return [NumberFieldValidator()]
        }
    }
}
