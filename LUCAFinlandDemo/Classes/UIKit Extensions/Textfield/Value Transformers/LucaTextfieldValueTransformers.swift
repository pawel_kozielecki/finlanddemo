import Foundation

protocol LucaTextfieldValueTransformer {

    func transform(value: String?) -> Any?
    func reverse(transformedValue: Any?) -> String?
}

class LucaDatePickerValueTransformer: LucaTextfieldValueTransformer {

    let dateFormatter = DateFormatter.makeDatePickerFormatter()

    func transform(value: String?) -> Any? {
        guard let value = value else {
            return nil
        }

        return dateFormatter.date(from: value)
    }

    func reverse(transformedValue: Any?) -> String? {
        guard let date = transformedValue as? Date else {
            return nil
        }

        return dateFormatter.string(from: date)
    }
}

class LucaDateTimePickerValueTransformer: LucaTextfieldValueTransformer {

    let dateFormatter = DateFormatter.makeSimpleDateAndTimeFormatter()

    func transform(value: String?) -> Any? {
        guard let value = value else {
            return nil
        }

        return dateFormatter.date(from: value)
    }

    func reverse(transformedValue: Any?) -> String? {
        guard let date = transformedValue as? Date else {
            return nil
        }

        return dateFormatter.string(from: date)
    }
}

class UnmodyfyingLucaTextfieldValueTransformer: LucaTextfieldValueTransformer {
    func transform(value: String?) -> Any? {
        return value
    }

    func reverse(transformedValue: Any?) -> String? {
        return transformedValue as? String
    }
}
