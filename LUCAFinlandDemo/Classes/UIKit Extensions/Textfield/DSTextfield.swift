import Foundation
import UIKit
import SnapKit

protocol Validatable {

    func validate() -> NSError?
}

protocol DSTextFieldDelegate: class {

    func textField(_ field: DSTextfield, didChangeValue value: String?)
    func textFieldDidReturn(_ field: DSTextfield)
}

protocol Focusable {

    func releaseFocus()
    func setFocus()
}

@IBDesignable
class DSTextfield: UIView, Focusable {

    let animationDuration = 0.3

    let iconHolder: UIStackView = DSTextfield.makeIconsHolder()
    let revealPasswordButton: UIButton = DSTextfield.makeRevealPasswordIcon()
    let tickIcon: UIImageView = UIImageView(image: UIImage(named: "icon-textfield-correct"))
    let placeholderLabel: UILabel = DSTextfield.makePlaceholderLabel()
    let line: UIView = UIView()
    let textField: UITextField = DSTextfield.makeTextfield()
    let errorLabel: UILabel = DSTextfield.makeErrorLabel()

    weak var delegate: DSTextFieldDelegate?
    var stateFactory: LucaTextfieldStateFactory = DefaultLucaTextfieldStateFactory()
    var validatorsFactory: LucaTextfieldValidatorsFactory = DefaultLucaTextfieldValidatorsFactory()
    var validators: [TextfieldValidator]?       // set these if you want to replace validators generated automatically for specific textfield type
    var valueTransformerFactory: LucaTextfieldValueTransformerFactory = DefaultLucaTextfieldValueTransformerFactory()
    var valueTransformer: LucaTextfieldValueTransformer?

    private var isTyping: Bool = false

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        arrangeSubviews()
        applyConstraints()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        arrangeSubviews()
        applyConstraints()
    }

    var value: String? {
        set {
            fillInputViewIfNeeded(value: newValue)
            textField.text = newValue
            let validationError = validate()
            redraw(error: validationError)
        }
        get {
            return textField.text
        }
    }

    @IBInspectable
    var type: Int = LucaTextfieldType.text.rawValue {
        didSet {
            if type == LucaTextfieldType.date.rawValue {
                textField.inputView = makeDatePicker(minimumDate: nil, maximumDate: Date())
            } else if type == LucaTextfieldType.dateNoRange.rawValue {
                textField.inputView = makeDatePicker(minimumDate: nil, maximumDate: nil)
            } else if type == LucaTextfieldType.futureDate.rawValue {
                textField.inputView = makeDatePicker(minimumDate: Date(), maximumDate: nil, pickerMode: .dateAndTime)
            } else {
                textField.inputView = nil
            }
            let isPasswordField = type == LucaTextfieldType.password.rawValue
            textField.isSecureTextEntry = isPasswordField && revealPasswordButton.isSelected == false
            if isPasswordField {
                iconHolder.insertArrangedSubview(revealPasswordButton, at: 0)
            }
            valueTransformer = valueTransformerFactory.makeValueTransformer(forTextfieldType: LucaTextfieldType(rawValue: type)!)
            redraw(animated: false)
        }
    }

    @IBInspectable
    var characterLimit: Int = 100

    @IBInspectable
    var placeholderTextKey: String = "FAKE_PLACEHOLDER" {
        didSet {
            redraw(animated: false)
        }
    }

    @IBInspectable
    var placeholderColor: UIColor = UIColor.dsWarmGrey {
        didSet {
            updateFactorySettings()
            redraw(animated: false)
        }
    }

    @IBInspectable
    var inputColor: UIColor = UIColor.dsViolet {
        didSet {
            updateFactorySettings()
            redraw(animated: false)
        }
    }

    @IBInspectable
    var textColor: UIColor = UIColor.white {
        didSet {
            redraw(animated: false)
        }
    }

    @IBInspectable
    var errorColor: UIColor = UIColor.dsViolet {
        didSet {
            updateFactorySettings()
            redraw(animated: false)
        }
    }

    @objc func didTapRevealPasswordButton(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        textField.isSecureTextEntry = !sender.isSelected
        textField.luca_fixTrailingSpaceAfterSecureTextEntryModeToggle()
    }

    @objc func textfieldDidChangeValue(_ textfield: UITextField) {
        delegate?.textField(self, didChangeValue: value)
    }

    // Mark: Focusable

    func releaseFocus() {
        textField.resignFirstResponder()
    }

    func setFocus() {
        textField.becomeFirstResponder()
    }
}

extension DSTextfield: UITextFieldDelegate {

    func textFieldDidBeginEditing(_ textField: UITextField) {
        isTyping = true
        redraw()
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        retrieveInputViewValueIfNeeded()
        isTyping = false
        let error = validate()
        redraw(error: error)
        delegate?.textField(self, didChangeValue: value)
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        delegate?.textFieldDidReturn(self)
        return true
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = textField.text ?? ""
        let newLength = text.count + string.count - range.length
        return newLength <= characterLimit
    }
}

extension DSTextfield: Validatable {

    func validate() -> NSError? {
        let textfieldType = LucaTextfieldType(rawValue: type) ?? LucaTextfieldType.text
        let currentValidators = validators ?? validatorsFactory.makeValidators(forTextfieldType: textfieldType)

        for validator in currentValidators {
            if let error = validator.validate(value: value) {
                return error
            }
        }
        return nil
    }
}

private extension DSTextfield {

    func redraw(animated: Bool = true, error: NSError? = nil) {
        let type = LucaTextfieldType(rawValue: self.type) ?? LucaTextfieldType.text
        let state = stateFactory.calculateTextfieldState(forTextfieldType: type, isTyping: isTyping, currentValue: value, validationError: error)
        textField.keyboardType = state.keyboardType
        textField.attributedPlaceholder = NSAttributedString(string: placeholderTextKey.localized, attributes: [NSAttributedString.Key.foregroundColor: placeholderColor])
        textField.textColor = textColor
        errorLabel.textColor = errorColor
        placeholderLabel.text = placeholderTextKey.localized
        if animated == true {
            UIView.animate(withDuration: self.animationDuration) {
                [weak self] in
                self?.apply(textfieldState: state)
            }
        } else {
            apply(textfieldState: state)
        }
    }

    func fillInputViewIfNeeded(value: String?) {
        if let datePicker = textField.inputView as? UIDatePicker,
           let date = valueTransformer?.transform(value: value) as? Date {
            datePicker.date = date
        }
    }

    func retrieveInputViewValueIfNeeded() {
        if let inputView = textField.inputView as? UIDatePicker {
            let stringValue = valueTransformer?.reverse(transformedValue: inputView.date)
            textField.text = stringValue
        }
    }

    func updateFactorySettings() {
        let configuration = LucaTextfieldStateFactoryConfiguration(errorColor: errorColor, inputColor: inputColor, placeholderColor: placeholderColor)
        stateFactory.setup(configuration: configuration)
    }

    func makeDatePicker(minimumDate: Date?, maximumDate: Date?, pickerMode: UIDatePicker.Mode = .date) -> UIDatePicker {
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = pickerMode

        if let minimumDate = minimumDate {
            datePicker.minimumDate = minimumDate
        }
        if let maximumDate = maximumDate {
            datePicker.maximumDate = maximumDate
        }

        return datePicker
    }

    func apply(textfieldState state: LucaTextfieldState) {
        placeholderLabel.textColor = state.placeholderLabelColor
        placeholderLabel.alpha = state.placeholderLabelColor == nil ? 0 : 1
        line.backgroundColor = state.lineColor
        errorLabel.text = state.errorMessage
        let isEmpty = value?.isEmpty ?? true
        let hasError = state.errorMessage != nil
        errorLabel.alpha = hasError ? 1 : 0
        if hasError || isTyping || isEmpty {
            tickIcon.removeFromSuperview()
            tickIcon.alpha = 0
        } else {
            iconHolder.insertArrangedSubview(tickIcon, at: iconHolder.arrangedSubviews.count)
            tickIcon.alpha = 1
        }
    }

    func applyConstraints() {
        placeholderLabel.snp.makeConstraints {
            make in
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.top.equalToSuperview()
            make.height.equalTo(20)
        }
        textField.snp.makeConstraints {
            make in
            make.edges.equalToSuperview()
        }
        line.snp.makeConstraints {
            make in
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.centerY.equalToSuperview().offset(17)
            make.height.equalTo(1)
        }
        errorLabel.snp.makeConstraints {
            make in
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.bottom.equalToSuperview()
            make.height.equalTo(20)
        }
        iconHolder.snp.makeConstraints {
            make in
            make.trailing.equalToSuperview()
            make.centerY.equalToSuperview()
        }
        revealPasswordButton.snp.makeConstraints {
            make in
            make.height.equalTo(16)
            make.width.equalTo(20)
        }
    }

    func arrangeSubviews() {
        addSubview(textField)
        addSubview(placeholderLabel)
        addSubview(line)
        addSubview(errorLabel)
        addSubview(iconHolder)

        textField.delegate = self
        textField.addTarget(self, action: #selector(DSTextfield.textfieldDidChangeValue(_:)), for: .editingChanged)
        revealPasswordButton.addTarget(self, action: #selector(DSTextfield.didTapRevealPasswordButton(_:)), for: .touchUpInside)
    }

    static func makePlaceholderLabel() -> UILabel {
        let label = UILabel()
        label.alpha = 0
        label.font = .dsTextfieldPromptSmall
        label.textColor = .dsViolet
        return label
    }

    static func makeTextfield() -> UITextField {
        let textField = UITextField()
        textField.font = .dsTextfieldText
        textField.autocorrectionType = .no
        textField.autocapitalizationType = .none
        return textField
    }

    static func makeErrorLabel() -> UILabel {
        let label = UILabel()
        label.alpha = 0
        label.font = .dsTextfieldPromptSmall
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.5
        label.textColor = .dsViolet
        return label
    }

    static func makeIconsHolder() -> UIStackView {
        let stackView = UIStackView()
        stackView.distribution = .equalCentering
        stackView.alignment = .trailing
        stackView.spacing = 5
        stackView.axis = .horizontal
        return stackView
    }

    static func makeRevealPasswordIcon() -> UIButton {
        let button = UIButton(type: .custom)
        let normalImage = UIImage(named: "icon-reveal-password")
        let selectedImage = UIImage(named: "icon-hide-password")
        button.setImage(normalImage, for: .normal)
        button.setImage(selectedImage, for: .selected)
        return button
    }
}
