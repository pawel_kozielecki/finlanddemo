import Foundation
import UIKit

func readStringFile(forResource: String, ofType: String) -> String {
    let path = Bundle.main.path(forResource: forResource, ofType: ofType)!
    let data = try! String(contentsOfFile: path, encoding: .utf8)
    return data;
}

func readDataAsset(assetName: String) -> Data {
    let asset = NSDataAsset(name: assetName)!
    return asset.data
}

func readStringDataAsset(assetName: String) -> String {
    let data = readDataAsset(assetName: assetName)
    return String(data: data, encoding: .utf8)!
}

func fileToUrl(forResource: String, withExtension: String) -> URL? {
    return Bundle.main.url(forResource: forResource, withExtension: withExtension)
}

func fileToUrl(forResource: String, ofType: String) -> URL? {
    if let path = Bundle.main.path(forResource: forResource, ofType: ofType) {
        return URL(fileURLWithPath: path)
    }
    return nil
}
