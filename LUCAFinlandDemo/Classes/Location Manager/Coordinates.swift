import Foundation
import GoogleMaps

struct Coordinates: Codable, Equatable {
    let latitude: Double
    let longitude: Double

    private enum CodingKeys: String, CodingKey {
        case latitude = "lat"
        case longitude = "lng"
    }
}

struct CoordinateBounds: Codable {
    let northEast: Coordinates
    let southWest: Coordinates

    private enum CodingKeys: String, CodingKey {
        case northEast = "northeast"
        case southWest = "southwest"
    }
}

extension Coordinates {

    static var defaultLocation: Coordinates {
        return Coordinates(latitude: 60.192059, longitude: 24.945831) // Helsinki
    }

    func toCLCoordinates() -> CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }

    func toJSON() -> [String: AnyHashable] {
        return [
            "lat": self.latitude,
            "lng": self.longitude
        ]
    }
}

extension CLLocationCoordinate2D {

    func toCoordinates() -> Coordinates {
        return Coordinates(latitude: latitude, longitude: longitude)
    }
}

extension CoordinateBounds: Equatable {
    public static func ==(lhs: CoordinateBounds, rhs: CoordinateBounds) -> Bool {
        return lhs.northEast == rhs.northEast && lhs.southWest == rhs.southWest
    }
}

extension GMSCoordinateBounds {
    func toCoordinateBounds() -> CoordinateBounds {
        return CoordinateBounds(
                northEast: northEast.toCoordinates(),
                southWest: southWest.toCoordinates())
    }
}

extension CoordinateBounds {
    func toGMSCoordinateBounds() -> GMSCoordinateBounds {
        return GMSCoordinateBounds(coordinate: northEast.toCLCoordinates(), coordinate: southWest.toCLCoordinates())
    }
}
