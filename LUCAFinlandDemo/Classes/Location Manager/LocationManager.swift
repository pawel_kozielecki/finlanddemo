import Foundation
import GoogleMaps
import CoreLocation

protocol LocationProviderDelegate: class {
    func locationProvider(_ provider: LocationProvider, didUpdateLocation location: Coordinates, updatesCount: Int)
    func locationProviderDidFailWithInsufficientPermissions(_ provider: LocationProvider)
}

protocol LocationProvider: class {
    var currentLocation: Coordinates { get }
    var delegate: LocationProviderDelegate? { get set }
    func startUpdating()
    func stopUpdating()
}

protocol LocationPermissionsHandler {
    var locationPermissionsStatus: LocationPermissionsStatus { get }
    func requestForAuthorization()
}

protocol LocationManager: LocationProvider, LocationPermissionsHandler {
}

enum LocationPermissionsStatus {
    case undetermined
    case granted
    case denied
}

protocol CLLocationManagerWrapper {
    var delegate: CLLocationManagerDelegate? { get set }
    func startUpdatingLocation()
    func stopUpdatingLocation()
    func requestAlwaysAuthorization()
}

class DefaultLocationManager: NSObject, LocationProvider {

    let defaultLocation = Coordinates.defaultLocation

    var locationManager: CLLocationManagerWrapper
    weak var delegate: LocationProviderDelegate?

    private var lastLocationUpdate: Coordinates?
    private var updatesCount: Int = 0

    init(distanceFilter: Double = 5) {
        locationManager = DefaultLocationManager.makeInternalLocationManager(distanceFilter: distanceFilter)

        super.init()

        locationManager.delegate = self
    }

    func startUpdating() {
        locationManager.startUpdatingLocation()
        dispatchCurrentLocationIfNeeded()
    }

    func stopUpdating() {
        locationManager.stopUpdatingLocation()
    }

    var currentLocation: Coordinates {
        return lastLocationUpdate ?? defaultLocation
    }
}

extension DefaultLocationManager: LocationPermissionsHandler {

    var locationPermissionsStatus: LocationPermissionsStatus {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined:
                return .undetermined
            case .restricted, .denied:
                return .denied
            case .authorizedAlways, .authorizedWhenInUse:
                return .granted
            @unknown default:
                return .denied
            }
        } else {
            return .denied
        }
    }

    func requestForAuthorization() {
        locationManager.requestAlwaysAuthorization()
    }
}

extension DefaultLocationManager: CLLocationManagerDelegate {

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            updatesCount += 1
            let coordinates = location.coordinate.toCoordinates()
            lastLocationUpdate = coordinates
            delegate?.locationProvider(self, didUpdateLocation: coordinates, updatesCount: updatesCount)
        }
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        let nsError = error as NSError
        if nsError.code == CLError.denied.rawValue {
            delegate?.locationProviderDidFailWithInsufficientPermissions(self)
        }
    }
}

private extension DefaultLocationManager {

    func dispatchCurrentLocationIfNeeded() {
        // if user location is already established (eg. user logged out and back in), immediately notify delegate to update Map View
        // ... deliberately set updatesCount to 0, as we want to ensure that Map View gets updated
        if updatesCount > 0 {
            delegate?.locationProvider(self, didUpdateLocation: lastLocationUpdate!, updatesCount: 0)
        }
    }

    static func makeInternalLocationManager(distanceFilter: Double) -> CLLocationManagerWrapper {
        let locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        locationManager.distanceFilter = distanceFilter
        return locationManager
    }
}

extension DefaultLocationManager: LocationManager {
}

extension CLLocationManager: CLLocationManagerWrapper {
}

extension LocationProviderDelegate {

    func locationProviderDidFailWithInsufficientPermissions(_ provider: LocationProvider) {
    }
}
