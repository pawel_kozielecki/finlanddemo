import Foundation
import CocoaLumberjack

enum LogKind: String {
    case generic = "DEBUG"
    case warning = "WARNING"
    case error = "ERROR"
}

protocol LogParametersProvider {
    var logParameters: String { get }
}

class AppLogger {

    static let sharedInstance = AppLogger()

    private init() {
        DDLog.add(DDOSLogger.sharedInstance)    // Apple os_log facility
    }

    func log(kind: LogKind, message: String) {
        switch kind {
        case .generic:
            DDLogDebug(message)
        case .warning:
            DDLogWarn(message)
        case .error:
            DDLogError(message)
        }
    }
}

/*
 * Implemented as a global functions - to enable convenient logging:
 * log("Something to log")
 * logWarning("Something to log")
 * logError("Something to log")
 * log(kind: .warning, withMessage: "Something to log")
 */

func log(kind: LogKind = .generic, message: String) {
    AppLogger.sharedInstance.log(kind: kind, message: message)
}

func log(_ message: String) {
    AppLogger.sharedInstance.log(kind: .generic, message: message)
}

func logWarning(_ message: String) {
    AppLogger.sharedInstance.log(kind: .warning, message: message)
}

func logError(_ message: String) {
    AppLogger.sharedInstance.log(kind: .error, message: message)
}
