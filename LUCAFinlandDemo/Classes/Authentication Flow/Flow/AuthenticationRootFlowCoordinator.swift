import Foundation
import UIKit

class AuthenticationRootFlowCoordinator: RootFlowCoordinator {

    let type: RootFlowCoordinatorType = .authentication
    let dependencyProvider: DependencyProvider

    var navigationController: UINavigationController
    var rootFlowCoordinatorDelegate: RootFlowCoordinatorDelegate?
    var authenticationFlowCoordinator: AuthenticationFlowCoordinator
    var rootViewController: UIViewController {
        return navigationController
    }

    init(dependencyProvider: DependencyProvider) {
        self.dependencyProvider = dependencyProvider
        self.navigationController = AuthenticationRootFlowCoordinator.makeNavigationController()
        self.authenticationFlowCoordinator = AuthenticationFlowCoordinator(dependencyProvider: dependencyProvider, presentingNavigationController: navigationController)
        self.authenticationFlowCoordinator.flowCoordinatorDelegate = self
    }

    func start() {
        authenticationFlowCoordinator.present(animated: true)
    }
}

extension AuthenticationRootFlowCoordinator: FlowCoordinatorDelegate {

    func flowCoordinatorDidFinish(_ flowCoordinator: FlowCoordinator) {
        rootFlowCoordinatorDelegate?.rootFlowCoordinatorDidFinish(self)
    }
}

private extension AuthenticationRootFlowCoordinator {

    class func makeNavigationController() -> UINavigationController {
        let navigationController = UINavigationController()
        navigationController.isNavigationBarHidden = true
        return navigationController
    }
}

