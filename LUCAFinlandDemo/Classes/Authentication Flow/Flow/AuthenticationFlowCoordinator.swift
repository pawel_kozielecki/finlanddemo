import Foundation
import UIKit

class AuthenticationFlowCoordinator: FlowCoordinator {

    let dependencyProvider: DependencyProvider

    weak var flowCoordinatorDelegate: FlowCoordinatorDelegate?
    weak var presentingNavigationController: UINavigationController?

    init(dependencyProvider: DependencyProvider, presentingNavigationController: UINavigationController) {
        self.dependencyProvider = dependencyProvider
        self.presentingNavigationController = presentingNavigationController
    }

    func present(animated: Bool) {
        let rootViewController = LoginViewController(loginManager: dependencyProvider.loginManager)
        rootViewController.delegate = self
        presentingNavigationController?.pushViewController(rootViewController, animated: animated)
    }
}

extension AuthenticationFlowCoordinator: LoginViewControllerDelegate {

    func loginViewControllerDidFinish(_ viewController: LoginViewController) {
        flowCoordinatorDelegate?.flowCoordinatorDidFinish(self)
    }
}

private extension AuthenticationFlowCoordinator {

    var mapViewController: MapViewController {
        return presentingNavigationController?.viewControllers.first as! MapViewController
    }


}
