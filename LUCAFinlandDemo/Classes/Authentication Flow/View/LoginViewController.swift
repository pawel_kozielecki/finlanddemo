import Foundation
import UIKit

protocol LoginViewControllerDelegate: class {

    func loginViewControllerDidFinish(_ viewController: LoginViewController)
}

class LoginViewController: UIViewController {

    let loginManager: LoginManager
    weak var delegate: LoginViewControllerDelegate?

    var progressHud: ProgressHud?
    var alert: InfoAlert?

    init(loginManager: LoginManager) {
        self.loginManager = loginManager

        super.init(nibName: nil, bundle: nil)

        progressHud = DefaultProgressHud(view: view)
        alert = SimpleInfoAlert(presentingViewController: self)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("Init(coder) not implemented")
    }

    override func loadView() {
        let view: LoginView = Bundle.loadView(fromNib: "LoginView")
        view.delegate = self
        self.view = view
    }
}

extension LoginViewController: LoginViewDelegate {

    func loginView(_ view: LoginView, didLoginWithCredentials credentials: LoginCredentials) {
        login(credentials: credentials)
    }
}

private extension LoginViewController {

    func login(credentials: LoginCredentials) {
        progressHud?.show()
        loginManager.logIn(credentials: credentials) {
            [weak self] user, error in
            guard let strongSelf = self else {
                return
            }

            if let error = error {
                let title = "LOGIN_ERROR_TITLE".localized
                let message = "LOGIN_ERROR_MESSAGE".localized
                strongSelf.alert?.show(title: title, message: message, completion: nil)
                strongSelf.progressHud?.hide()
            } else {
                strongSelf.delegate?.loginViewControllerDidFinish(strongSelf)
            }
        }
    }
}
