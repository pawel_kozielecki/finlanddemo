import Foundation
import UIKit

protocol LoginViewDelegate: class {
    func loginView(_ view: LoginView, didLoginWithCredentials credentials: LoginCredentials)
}

class LoginView: UIView {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var inputLogin: DSTextfield!
    @IBOutlet weak var inputPassword: DSTextfield!
    @IBOutlet weak var buttonOk: DSRoundedButton!

    weak var delegate: LoginViewDelegate?
    var validatableFields: [Validatable] = []

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        arrangeView()
    }

    @IBAction func didTapOkButton(_ sender: UIButton) {
        guard let email = inputLogin.value, let password = inputPassword.value else {
            return
        }

        inputLogin.releaseFocus()
        inputPassword.releaseFocus()

        let credentials = LoginCredentials(login: email, password: password)
        delegate?.loginView(self, didLoginWithCredentials: credentials)
    }
}

extension LoginView: DSTextFieldDelegate {

    func textField(_ field: DSTextfield, didChangeValue value: String?) {
        unlockLoginButtonIfNeeded()
    }

    func textFieldDidReturn(_ field: DSTextfield) {
        if field == inputLogin {
            inputPassword.setFocus()
        } else if buttonOk.isEnabled {
            didTapOkButton(buttonOk)
        }
    }
}

private extension LoginView {

    func arrangeView() {
        labelTitle.text = "LOGIN_SCREEN_TITLE".localized

        inputPassword.validators = [PlainTextfieldValidator(lowerCharacterLimit: 1)]
        validatableFields = [inputLogin, inputPassword]
        inputLogin.delegate = self
        inputPassword.delegate = self
        unlockLoginButtonIfNeeded()

        layoutIfNeeded()
    }

    func unlockLoginButtonIfNeeded() {
        buttonOk.isEnabled = getCurrentValidationError() == nil
    }

    func getCurrentValidationError() -> NSError? {
        for field in validatableFields {
            if let error = field.validate() {
                return error
            }
        }
        return nil
    }
}
