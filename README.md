# Installation guide

1. Install `homebrew` if you haven't done so yet (instructions [here](http://brew.sh))
2. Install Ruby 2.3.0 via `rvm` or `rbenv`
3. Install bundler by running `gem install bundler`
4. Install gems using bundler by running `bundle install`
5. Install pods by running `pod install`
6. You're all set!